package north.Overthink.config;

import north.Overthink.api.service.serviceImpl.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailService)
                .passwordEncoder(encoder());
    }

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors();

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers( "/register").permitAll()
                .antMatchers("/authenticate").permitAll()
                .antMatchers(HttpMethod.PUT, "/job_field/**").permitAll()
                .antMatchers(HttpMethod.GET, "/job_field/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/job/**").permitAll()
                .antMatchers(HttpMethod.GET, "/job/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/offer/**").permitAll()
                .antMatchers(HttpMethod.GET, "/offer/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/course/**").permitAll()
                .antMatchers(HttpMethod.GET, "/course/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/order/**").permitAll()
                .antMatchers(HttpMethod.GET, "/order/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/request/**").permitAll()
                .antMatchers(HttpMethod.GET, "/request/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/event/**").permitAll()
                .antMatchers(HttpMethod.GET, "/event    /**").permitAll()
                .anyRequest().authenticated().and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
