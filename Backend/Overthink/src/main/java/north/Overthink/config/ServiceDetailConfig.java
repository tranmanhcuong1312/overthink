package north.Overthink.config;

import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ServiceDetailConfig {

    private List<StandardServiceDetail> serviceDetailList;

    @Autowired
    public void setServiceDetailList(List<StandardServiceDetail> serviceDetailList) {
        this.serviceDetailList = serviceDetailList;
    }

    public List<StandardServiceDetail> getServiceDetailList() {
        return serviceDetailList;
    }

    @Primary
    @Bean(name = "StandardServiceDetailMap")
    public Map<String, StandardServiceDetail> standardServiceDetailMap(){
        Map<String, StandardServiceDetail> standardServiceDetailMap = new HashMap<>();
        for(StandardServiceDetail standardServiceDetail: getServiceDetailList()){
            standardServiceDetailMap.put(standardServiceDetail.getClass().getName(), standardServiceDetail);
        }
        return standardServiceDetailMap;
    }
}
