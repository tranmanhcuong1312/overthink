package north.Overthink.config;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import north.Overthink.api.handler.Context;
import north.Overthink.api.handler.utils.JwtTokenUtil;
import north.Overthink.api.service.serviceImpl.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        final String requestTokenHeader = request.getHeader("Authorization");

        String username = null;
        String jwtToken = null;

//        HttpSession session = request.getSession(false);

//        if(session == null) {
//            try {
//                session = ((HttpServletRequest) RequestCycle.get().getRequest().getContainerRequest()).getSession();
//            }catch (NullPointerException e){
//
//            }
//        }

        if(requestTokenHeader != null){
            jwtToken = requestTokenHeader;
            try{
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            }catch (SignatureException e){
                throw new SignatureException("JWT Token validate");
            }catch (MalformedJwtException e) {
                throw new MalformedJwtException("JWT Token validate");
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        } else {
            logger.warn("Where is token?");
        }

        if(username != null
                && SecurityContextHolder.getContext().getAuthentication() == null
                && Objects.equals(jwtToken, Context.getUserAuthenticated().getToken())
        ){
//            if(session == null){
//                throw new SessionException("Session out date");
//            }

//            String jwtTokenCheck = ((UserInfoAuthentication)
//                    session.getAttribute(
//                            FinalKeyWords.USER_INFO_AUTHENTICATION)).getToken();

//            if(!jwtToken.equals(jwtTokenCheck)){
//                throw new SignatureException("JWT Token validate");
//            }
            UserDetails userDetails = this.userDetailService.loadUserByUsername(username);

            if(jwtTokenUtil.validateToken(jwtToken, userDetails)){
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(
                                userDetails, null, userDetails.getAuthorities()
                        );

                usernamePasswordAuthenticationToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }else{
                Context.clearThreadLocal();
            }
        }

        filterChain.doFilter(request, response);
    }
}
