package north.Overthink.annotation;

import north.Overthink.model.StandardModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ReferenceCreate {

    public Class<? extends StandardModel> value();
}
