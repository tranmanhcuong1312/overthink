package north.Overthink.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NoSuchThingForUpdateException extends Exception{

    private static final HttpStatus BAD_REQUEST_STATUS = HttpStatus.BAD_REQUEST;
    private static final String BAD_REQUEST = "No value need to be update";

    public NoSuchThingForUpdateException() {
        super();
    }

    public ResponseStatusException badRequest(){
        return new ResponseStatusException(BAD_REQUEST_STATUS, BAD_REQUEST);
    }

    public ResponseStatusException standardBadRequest(String message){
        return new ResponseStatusException(BAD_REQUEST_STATUS, message);
    }
}
