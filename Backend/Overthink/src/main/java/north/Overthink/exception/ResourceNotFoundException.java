package north.Overthink.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ResourceNotFoundException extends Exception{

    private static final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
    private static final String RESOURCE_NOT_FOUND = "Resource not found";
    private static final String NOT_FOUND_RESOURCE_SATISFY = "No resource found satisfy the conditions";

    public ResourceNotFoundException(){
        super();
    }

    public ResponseStatusException resourceNotFound(){
        return new ResponseStatusException(httpStatus, RESOURCE_NOT_FOUND);
    }

    public ResponseStatusException notFoundResourceSatisfy(){
        return new ResponseStatusException(httpStatus, NOT_FOUND_RESOURCE_SATISFY);
    }

    public ResponseStatusException standardResourceNotFound(String message){
        return new ResponseStatusException(httpStatus, message);
    }
}
