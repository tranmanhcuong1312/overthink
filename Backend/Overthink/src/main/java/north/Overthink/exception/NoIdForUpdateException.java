package north.Overthink.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NoIdForUpdateException extends Exception{

    private static final HttpStatus httpStatus = HttpStatus.NOT_FOUND;
    private static final String NOT_ID_FOUND = "No id for update";

    public NoIdForUpdateException() {
        super();
    }

    public ResponseStatusException noIdFound(){
        return new ResponseStatusException(httpStatus, NOT_ID_FOUND);
    }

    public ResponseStatusException standardNoIdForUpdate(String message){
        return new ResponseStatusException(httpStatus, message);
    }
}
