package north.Overthink.model.templatemodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import north.Overthink.model.submodel.User;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoAuthentication implements Serializable {

    private User user;

    private String token;
}
