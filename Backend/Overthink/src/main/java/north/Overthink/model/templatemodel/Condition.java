package north.Overthink.model.templatemodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Condition {

    private String field;

    private ConditionKey conditionKey;

    private Object value;
}
