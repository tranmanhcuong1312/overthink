package north.Overthink.model.templatemodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomPageRequest {

    private int page;

    private int size;

    private Sort.Direction direction;

    private String sortBy;

    public Pageable getPageable(){
        return PageRequest.of(getPage(), getSize(), getDirection(), getSortBy());
    }
}
