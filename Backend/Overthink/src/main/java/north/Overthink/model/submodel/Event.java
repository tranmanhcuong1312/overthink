package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "event")
@Data
public class Event extends StandardModel<Event> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @Column(name = "start_date")
//    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date startDate;

    @Column(name = "end_date")
//    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date endDate;

    public Event(){

    }

    public Event(Event event){
        this.name = event.getName();
        this.content = event.getContent();
        this.startDate = event.getStartDate();
        this.endDate = event.getEndDate();
    }

    public Event(String name, String content, Date startDate, Date endDate) {
        this.name = name;
        this.content = content;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Event(Integer id, String name, String content, Date startDate, Date endDate) {
        super(id);
        this.name = name;
        this.content = content;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Event(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Event oldVersion, Event newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String content, Date startDate, Date endDate) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.content = content;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
