package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "person_name")
@Data
public class PersonName extends StandardModel<PersonName> {

    @ManyToOne
    @JoinColumn(name = "person")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Person.class)
    @StandardJsonIgnoreProperties
    private Person person;

    @Column(name = "given_name")
    @FieldCanHaveCondition
    private String givenName;

    @Column(name = "middle_name")
    @FieldCanHaveCondition
    private String middleName;

    @Column(name = "family_name_1")
    @FieldCanHaveCondition
    private String familyName1;

    @Column(name = "family_name_2")
    @FieldCanHaveCondition
    private String familyName2;

    public PersonName(){

    }

    public PersonName(PersonName personName){
        this.person = personName.getPerson();
        this.givenName = personName.getGivenName();
        this.middleName = personName.getMiddleName();
        this.familyName1 = personName.getFamilyName1();
        this.familyName2 = personName.getFamilyName2();
    }

    public PersonName(String givenName, String middleName, String familyName1, String familyName2) {
        super();
        this.person = person;
        this.givenName = givenName;
        this.middleName = middleName;
        this.familyName1 = familyName1;
        this.familyName2 = familyName2;
    }

    public PersonName(Integer id, String givenName, String middleName, String familyName1, String familyName2) {
        super(id);
        this.person = person;
        this.givenName = givenName;
        this.middleName = middleName;
        this.familyName1 = familyName1;
        this.familyName2 = familyName2;
    }

    public PersonName(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, PersonName oldVersion, PersonName newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Person person, String givenName, String middleName, String familyName1, String familyName2) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.person = person;
        this.givenName = givenName;
        this.middleName = middleName;
        this.familyName1 = familyName1;
        this.familyName2 = familyName2;
    }
}
