package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "offer_pckage")
@Data
public class OfferPackage extends StandardModel<OfferPackage> {

    @ManyToOne
    @JoinColumn(name = "offer")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Offer offer;

    @ManyToOne
    @JoinColumn(name = "package")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Package aPackage;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @Column(name = "price")
    @FieldCanHaveCondition
    private double price;

    @Column(name = "delivery_time")
    @FieldCanHaveCondition
    private LocalTime deliveryTime;

    @Column(name = "order")
    @FieldCanHaveCondition
    private int order;

    public OfferPackage(){

    }

    public OfferPackage(OfferPackage offerPackage){
        this.offer = offerPackage.getOffer();
        this.aPackage = offerPackage.getAPackage();
        this.name = offerPackage.getName();
        this.content = offerPackage.getContent();
        this.price = offerPackage.getPrice();
        this.deliveryTime = offerPackage.getDeliveryTime();
        this.order = offerPackage.getOrder();
    }

    public OfferPackage(Offer offer, Package aPackage, String name, String content, double price, LocalTime deliveryTime, int order) {
        this.offer = offer;
        this.aPackage = aPackage;
        this.name = name;
        this.content = content;
        this.price = price;
        this.deliveryTime = deliveryTime;
        this.order = order;
    }

    public OfferPackage(Integer id, Offer offer, Package aPackage, String name, String content, double price, LocalTime deliveryTime, int order) {
        super(id);
        this.offer = offer;
        this.aPackage = aPackage;
        this.name = name;
        this.content = content;
        this.price = price;
        this.deliveryTime = deliveryTime;
        this.order = order;
    }

    public OfferPackage(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, OfferPackage oldVersion, OfferPackage newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Offer offer, Package aPackage, String name, String content, double price, LocalTime deliveryTime, int order) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.offer = offer;
        this.aPackage = aPackage;
        this.name = name;
        this.content = content;
        this.price = price;
        this.deliveryTime = deliveryTime;
        this.order = order;
    }
}
