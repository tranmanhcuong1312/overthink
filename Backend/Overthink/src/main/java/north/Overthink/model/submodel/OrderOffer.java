package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "order_offer")
@Data
public class OrderOffer extends StandardModel<OrderOffer> {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Order order;

    @ManyToOne
    @JoinColumn(name = "offer")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Offer offer;

    public OrderOffer(){

    }

    public OrderOffer(OrderOffer orderOffer){
        this.order = orderOffer.getOrder();
        this.offer = orderOffer.getOffer();
    }

    public OrderOffer(Order order, Offer offer) {
        this.order = order;
        this.offer = offer;
    }

    public OrderOffer(Integer id, Order order, Offer offer) {
        super(id);
        this.order = order;
        this.offer = offer;
    }

    public OrderOffer(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, OrderOffer oldVersion, OrderOffer newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Order order, Offer offer) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.order = order;
        this.offer = offer;
    }
}
