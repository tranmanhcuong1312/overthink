package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.aspectj.weaver.ast.Not;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notification")
@Data
public class Notification extends StandardModel<Notification> {

    @ManyToOne
    @JoinColumn(name = "notification_type")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private NotificationType notificationType;

    @ManyToOne
    @JoinColumn(name = "receiver")
    @JsonIgnoreProperties(value = {"person", "role"})
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User receiver;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @Column(name = "is_read")
    private boolean isRead;

    public Notification(){

    }

    public Notification(Notification notification){
        this.notificationType = notification.getNotificationType();
        this.receiver = notification.getReceiver();
        this.content = notification.getContent();
        this.isRead = notification.isRead();
    }

    public Notification(NotificationType notificationType, User receiver, String content, boolean isRead) {
        this.notificationType = notificationType;
        this.receiver = receiver;
        this.content = content;
        this.isRead = isRead;
    }

    public Notification(Integer id, NotificationType notificationType, User receiver, String content, boolean isRead) {
        super(id);
        this.notificationType = notificationType;
        this.receiver = receiver;
        this.content = content;
        this.isRead = isRead;
    }

    public Notification(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Notification oldVersion, Notification newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, NotificationType notificationType, User receiver, String content, boolean isRead) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.notificationType = notificationType;
        this.receiver = receiver;
        this.content = content;
        this.isRead = isRead;
    }
}
