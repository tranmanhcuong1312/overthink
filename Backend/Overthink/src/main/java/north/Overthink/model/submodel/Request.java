package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "request")
@Data
public class Request extends StandardModel<Request> {

    @Column(name = "active")
    private boolean active;

    @ManyToOne
    @JoinColumn(name = "user")
    @JsonIgnoreProperties(value = {"person", "role"})
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User user;

    @ManyToOne
    @JoinColumn(name = "job")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Job job;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "lowest_price")
    @FieldCanHaveCondition
    private double lowestPrice;

    @Column(name = "highest_price")
    @FieldCanHaveCondition
    private double highestPrice;

    @Column(name = "expect_time")
    @FieldCanHaveCondition
    private LocalTime expectTime;

    public Request(){

    }

    public Request(Request request){
        this.active = request.isActive();
        this.user = request.getUser();
        this.job = request.getJob();
        this.name = request.getName();
        this.description = request.getDescription();
        this.lowestPrice = request.getLowestPrice();
        this.highestPrice = request.getHighestPrice();
        this.expectTime = request.getExpectTime();
    }

    public Request(boolean active, User user, Job job, String name, String description, double lowestPrice, double highestPrice, LocalTime expectTime) {
        this.active = active;
        this.user = user;
        this.job = job;
        this.name = name;
        this.description = description;
        this.lowestPrice = lowestPrice;
        this.highestPrice = highestPrice;
        this.expectTime = expectTime;
    }

    public Request(Integer id, boolean active, User user, Job job, String name, String description, double lowestPrice, double highestPrice, LocalTime expectTime) {
        super(id);
        this.active = active;
        this.user = user;
        this.job = job;
        this.name = name;
        this.description = description;
        this.lowestPrice = lowestPrice;
        this.highestPrice = highestPrice;
        this.expectTime = expectTime;
    }

    public Request(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Request oldVersion, Request newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, boolean active, User user, Job job, String name, String description, double lowestPrice, double highestPrice, LocalTime expectTime) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.active = active;
        this.user = user;
        this.job = job;
        this.name = name;
        this.description = description;
        this.lowestPrice = lowestPrice;
        this.highestPrice = highestPrice;
        this.expectTime = expectTime;
    }
}
