package north.Overthink.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import north.Overthink.model.submodel.User;
import org.springframework.data.annotation.CreatedDate;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VoidedStandardModel {

    private int id;

    private boolean voided;

    private String voidedCause;
}
