package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "offer")
@Data
public class Offer extends StandardModel<Offer> {

    @Column(name = "active")
    @FieldCanHaveCondition
    private boolean active;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @ManyToOne
    @JoinColumn(name = "job")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @FieldCanHaveCondition
    @StandardJsonIgnoreProperties
    @JsonIgnoreProperties(value = {"jobField"})
    private Job job;

    @ManyToOne
    @JoinColumn(name = "product_level")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @FieldCanHaveCondition
    @StandardJsonIgnoreProperties
    private ProductLevel productLevel;

    @Column(name = "price")
    @FieldCanHaveCondition
    private int price;

    @Column(name = "order_count")
    @FieldCanHaveCondition
    private int orderCount;

    @Column(name = "evaluation_rate")
    @FieldCanHaveCondition
    private int evaluationRate;

    public Offer(){

    }

    public Offer(Offer offer){
        this.active = offer.isActive();
        this.job = offer.getJob();
        this.name = offer.getName();
        this.content = offer.getContent();
        this.productLevel = offer.getProductLevel();
        this.price = offer.getPrice();
        this.orderCount = offer.getOrderCount();
        this.evaluationRate = offer.getEvaluationRate();
    }

    public Offer(boolean active, String name, String content, Job job, ProductLevel productLevel, int price, int orderCount, int evaluationRate) {
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.productLevel = productLevel;
        this.price = price;
        this.orderCount = orderCount;
        this.evaluationRate = evaluationRate;
    }

    public Offer(Integer id, boolean active, String name, String content, Job job, ProductLevel productLevel, int price, int orderCount, int evaluationRate) {
        super(id);
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.productLevel = productLevel;
        this.price = price;
        this.orderCount = orderCount;
        this.evaluationRate = evaluationRate;
    }

    public Offer(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Offer oldVersion, Offer newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, boolean active, String name, String content, Job job, ProductLevel productLevel, int price, int orderCount, int evaluationRate) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.productLevel = productLevel;
        this.price = price;
        this.orderCount = orderCount;
        this.evaluationRate = evaluationRate;
    }
}
