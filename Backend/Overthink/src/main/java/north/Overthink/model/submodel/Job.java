package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "job")
@Data
public class Job extends StandardModel<Job> {

    @ManyToOne
    @JoinColumn(name = "job_field")
    @StandardJsonIgnoreProperties
    @JsonIgnoreProperties("jobs")
    @FieldCanHaveCondition
    private JobField jobField;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public Job(){

    }

    public Job(Job job){
        this.jobField = job.getJobField();
        this.name = job.getName();
        this.description = job.getDescription();
    }

    public Job(JobField jobField, String name, String description) {
        this.jobField = jobField;
        this.name = name;
        this.description = description;
    }

    public Job(Integer id, JobField jobField, String name, String description) {
        super(id);
        this.jobField = jobField;
        this.name = name;
        this.description = description;
    }

    public Job(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Job oldVersion, Job newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, JobField jobField, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.jobField = jobField;
        this.name = name;
        this.description = description;
    }
}
