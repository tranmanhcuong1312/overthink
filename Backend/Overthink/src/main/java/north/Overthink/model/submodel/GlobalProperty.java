package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "global_properties")
@Data
public class GlobalProperty extends StandardModel<GlobalProperty> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @ManyToOne
    @JoinColumn(name = "datatype")
    @StandardJsonIgnoreProperties
    private Datatype datatype;

    @Column(name = "value")
    @FieldCanHaveCondition
    private String value;

    @Column(name = "description")
    private String description;

    public GlobalProperty(){

    }

    public GlobalProperty(GlobalProperty globalProperty){
        this.name = globalProperty.getName();
        this.datatype = globalProperty.getDatatype();
        this.value = globalProperty.getValue();
        this.description = globalProperty.getDescription();
    }

    public GlobalProperty(String name, Datatype datatype, String value, String description) {
        super();
        this.name = name;
        this.datatype = datatype;
        this.value = value;
        this.description = description;
    }

    public GlobalProperty(Integer id, String name, Datatype datatype, String value, String description) {
        super(id);
        this.name = name;
        this.datatype = datatype;
        this.value = value;
        this.description = description;
    }

    public GlobalProperty(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, GlobalProperty oldVersion, GlobalProperty newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, Datatype datatype, String value, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.datatype = datatype;
        this.value = value;
        this.description = description;
    }
}
