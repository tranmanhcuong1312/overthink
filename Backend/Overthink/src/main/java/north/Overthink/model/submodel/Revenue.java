package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.time.YearMonth;
import java.util.Date;

@Entity
@Table(name = "revenue")
@Data
public class Revenue extends StandardModel<Revenue> {

    @ManyToOne
    @JoinColumn(name = "user_revenue")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private UserRevenue userRevenue;

    @Column(name = "month")
    @FieldCanHaveCondition
    private String month;

    @Column(name = "income")
    @FieldCanHaveCondition
    private double income;

    public Revenue(){

    }

    public Revenue(Revenue revenue){
        this.userRevenue = revenue.getUserRevenue();
        this.month = revenue.getMonth();
        this.income = revenue.getIncome();
    }

    public Revenue(UserRevenue userRevenue, String month, double income) {
        this.userRevenue = userRevenue;
        this.month = month;
        this.income = income;
    }

    public Revenue(Integer id, UserRevenue userRevenue, String month, double income) {
        super(id);
        this.userRevenue = userRevenue;
        this.month = month;
        this.income = income;
    }

    public Revenue(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Revenue oldVersion, Revenue newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, UserRevenue userRevenue, String month, double income) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.userRevenue = userRevenue;
        this.month = month;
        this.income = income;
    }
}
