package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "task_type")
@Data
public class TaskType extends StandardModel<TaskType> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public TaskType(){

    }

    public TaskType(TaskType taskType){
        this.name = taskType.getName();
        this.description = taskType.getDescription();
    }

    public TaskType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TaskType(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public TaskType(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, TaskType oldVersion, TaskType newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
