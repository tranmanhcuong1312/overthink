package north.Overthink.model.templatemodel;

public enum ConditionKey {

    CONTAIN("contain"),
    GREATER("greater"),
    SMALLER("smaller"),
    EQUAL("equal"),
    GREATER_AND_EQUAL("greater and equal"),
    SMALLER_AND_EQUAL("smaller and equal");

    private String value;

    ConditionKey(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}