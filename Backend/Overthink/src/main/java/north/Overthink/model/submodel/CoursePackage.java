package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Table(name = "course_package")
@Data
public class CoursePackage extends StandardModel<CoursePackage> {

    @ManyToOne
    @JoinColumn(name = "course")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Course course;

    @ManyToOne
    @JoinColumn(name = "package")
    @StandardJsonIgnoreProperties
    private Package aPackage;

    @Column(name = "content")
    private String content;

    @Column(name = "price")
    @FieldCanHaveCondition
    private double price;

    @Column(name = "delivery_time")
    @FieldCanHaveCondition
    private LocalTime time;

    @Column(name = "order")
    private int order;

    public CoursePackage(){

    }

    public CoursePackage(CoursePackage coursePackage){
        this.course = coursePackage.getCourse();
        this.aPackage = coursePackage.getAPackage();
        this.content = coursePackage.getContent();
        this.price = coursePackage.getPrice();
        this.time = coursePackage.getTime();
        this.order = coursePackage.getOrder();
    }

    public CoursePackage(Course course, Package aPackage, String content, double price, LocalTime time, int order) {
        this.course = course;
        this.aPackage = aPackage;
        this.content = content;
        this.price = price;
        this.time = time;
        this.order = order;
    }

    public CoursePackage(Integer id, Course course, Package aPackage, String content, double price, LocalTime time, int order) {
        super(id);
        this.course = course;
        this.aPackage = aPackage;
        this.content = content;
        this.price = price;
        this.time = time;
        this.order = order;
    }

    public CoursePackage(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, CoursePackage oldVersion, CoursePackage newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Course course, Package aPackage, String content, double price, LocalTime time, int order) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.course = course;
        this.aPackage = aPackage;
        this.content = content;
        this.price = price;
        this.time = time;
        this.order = order;
    }
}
