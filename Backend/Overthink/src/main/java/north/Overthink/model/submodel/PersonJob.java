package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "person_job")
@Data
public class PersonJob extends StandardModel<PersonJob> {

    @ManyToOne
    @JoinColumn(name = "person")
    @StandardJsonIgnoreProperties
    private Person person;

    @ManyToOne
    @JoinColumn(name = "job_field", referencedColumnName = "id")
    @FieldCanHaveCondition
    private JobField jobField;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "level")
    private String level;

    @Column(name = "company")
    @FieldCanHaveCondition
    private String company;

    @Column(name ="description")
    private String description;

    public PersonJob(){

    }

    public PersonJob(PersonJob personJob){
        this.person = personJob.getPerson();
        this.jobField = personJob.getJobField();
        this.name = personJob.getName();
        this.startDate = personJob.getStartDate();
        this.endDate = personJob.getEndDate();
        this.level = personJob.getLevel();
        this.company = personJob.getCompany();
        this.description = personJob.getDescription();
    }

    public PersonJob(Person person, JobField jobField, String name, Date startDate, Date endDate, String level, String company, String description) {
        this.person = person;
        this.jobField = jobField;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.level = level;
        this.company = company;
        this.description = description;
    }

    public PersonJob(Integer id, Person person, JobField jobField, String name, Date startDate, Date endDate, String level, String company, String description) {
        super(id);
        this.person = person;
        this.jobField = jobField;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.level = level;
        this.company = company;
        this.description = description;
    }

    public PersonJob(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, PersonJob oldVersion, PersonJob newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Person person, JobField jobField, String name, Date startDate, Date endDate, String level, String company, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.person = person;
        this.jobField = jobField;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.level = level;
        this.company = company;
        this.description = description;
    }
}
