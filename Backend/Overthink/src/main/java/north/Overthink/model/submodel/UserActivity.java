package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_activity")
@Data
public class UserActivity extends StandardModel<UserActivity> {

    @ManyToOne
    @JoinColumn(name = "user_profile")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UserProfile.class)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private UserProfile userProfile;

    @ManyToOne
    @JoinColumn(name = "activity_type")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private ActivityType activityType;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    public UserActivity(){

    }

    public UserActivity(UserActivity userActivity){
        this.userProfile = userActivity.getUserProfile();
        this.activityType = userActivity.getActivityType();
        this.content = userActivity.getContent();
    }

    public UserActivity(UserProfile userProfile, ActivityType activityType, String content) {
        this.userProfile = userProfile;
        this.activityType = activityType;
        this.content = content;
    }

    public UserActivity(Integer id, UserProfile userProfile, ActivityType activityType, String content) {
        super(id);
        this.userProfile = userProfile;
        this.activityType = activityType;
        this.content = content;
    }

    public UserActivity(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, UserActivity oldVersion, UserActivity newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, UserProfile userProfile, ActivityType activityType, String content) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.userProfile = userProfile;
        this.activityType = activityType;
        this.content = content;
    }
}
