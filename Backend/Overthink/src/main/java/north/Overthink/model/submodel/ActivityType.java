package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import north.Overthink.model.StandardModelVersion;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "activity_type")
@Data
public class ActivityType extends StandardModel<ActivityType> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public ActivityType(){

    }

    public ActivityType(ActivityType activityType){
        this.name = activityType.getName();
        this.description = activityType.getDescription();
    }

    public ActivityType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public ActivityType(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public ActivityType(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, ActivityType oldVersion, ActivityType newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }

    //    public ActivityType(Integer id, User creator, Date dateCreated, boolean voided, User voidedBy, Date voidedDate, String voidedCause, User changedBy, Date dateChanged, ActivityType oldVersion, ActivityType newVersion, String name, String description) {
//        super(id, creator, dateCreated, voided, voidedBy, voidedDate, voidedCause, changedBy, dateChanged, oldVersion, newVersion);
//        this.name = name;
//        this.description = description;
//    }
}
