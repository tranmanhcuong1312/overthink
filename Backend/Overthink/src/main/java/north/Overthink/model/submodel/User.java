package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user")
@Data
public class User extends StandardModel<User> {

    @ManyToOne
    @JoinColumn(name = "person")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @FieldCanHaveCondition
    private Person person;

    @ManyToOne
    @JoinColumn(name = "role")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Role role;

    @Column(name = "registered_email")
    @FieldCanHaveCondition
    private String registeredEmail;

    @Column(name = "username")
    @FieldCanHaveCondition
    private String username;

    @Column(name = "password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "secret_question")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String secretQuestion;

    @Column(name = "secret_answer")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String secretAnswer;

    public User(){

    }

    public User (User user){
        this.person = user.getPerson();
        this.role = user.getRole();
        this.registeredEmail = user.getRegisteredEmail();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.secretQuestion = user.getSecretQuestion();
        this.secretAnswer = user.getSecretAnswer();
    }

    public User(Person person, Role role, String registeredEmail, String username, String password, String secretQuestion, String secretAnswer) {
        this.person = person;
        this.role = role;
        this.registeredEmail = registeredEmail;
        this.username = username;
        this.password = password;
        this.secretQuestion = secretQuestion;
        this.secretAnswer = secretAnswer;
    }

    public User(Integer id, Person person, Role role, String registeredEmail, String username, String password, String secretQuestion, String secretAnswer) {
        super(id);
        this.person = person;
        this.role = role;
        this.registeredEmail = registeredEmail;
        this.username = username;
        this.password = password;
        this.secretQuestion = secretQuestion;
        this.secretAnswer = secretAnswer;
    }

    public User(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, User oldVersion, User newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Person person, Role role, String registeredEmail, String username, String password, String secretQuestion, String secretAnswer) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.person = person;
        this.role = role;
        this.registeredEmail = registeredEmail;
        this.username = username;
        this.password = password;
        this.secretQuestion = secretQuestion;
        this.secretAnswer = secretAnswer;
    }
}
