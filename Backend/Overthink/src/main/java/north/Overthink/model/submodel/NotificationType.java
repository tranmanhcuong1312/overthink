package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "notification_type")
@Data
public class NotificationType extends StandardModel<NotificationType> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public NotificationType(){

    }

    public NotificationType(NotificationType notificationType){
        this.name = notificationType.getName();
        this.description = notificationType.getDescription();
    }

    public NotificationType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public NotificationType(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public NotificationType(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, NotificationType oldVersion, NotificationType newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
