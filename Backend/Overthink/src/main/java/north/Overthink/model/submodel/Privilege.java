package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "privilege")
@Data
public class Privilege extends StandardModel<Privilege> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public Privilege(){

    }

    public Privilege(Privilege privilege){
        this.name = privilege.getName();
        this.description = privilege.getDescription();
    }

    public Privilege(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Privilege(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public Privilege(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Privilege oldVersion, Privilege newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
