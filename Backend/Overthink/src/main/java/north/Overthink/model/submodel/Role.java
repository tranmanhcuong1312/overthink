package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "role")
@Data
public class Role extends StandardModel<Role> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    @FieldCanHaveCondition
    private String description;

    public Role() {

    }

    public Role(Role role){
        this.name = role.getName();
        this.description = role.getDescription();
    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Role(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public Role(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Role oldVersion, Role newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
