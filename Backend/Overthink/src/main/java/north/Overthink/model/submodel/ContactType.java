package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import north.Overthink.model.StandardModelVersion;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "contact_type")
@Data
public class ContactType extends StandardModel<ContactType> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @ManyToOne
    @JoinColumn(name = "datatype")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Datatype.class)
    @StandardJsonIgnoreProperties
    private Datatype datatype;

    public ContactType(){

    }

    public ContactType(ContactType contactType){
        this.name = contactType.getName();
        this.datatype = contactType.getDatatype();
    }

    public ContactType(String name, Datatype datatype) {
        this.name = name;
        this.datatype = datatype;
    }

    public ContactType(Integer id, String name, Datatype datatype) {
        super(id);
        this.name = name;
        this.datatype = datatype;
    }

    public ContactType(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, ContactType oldVersion, ContactType newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, Datatype datatype) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.datatype = datatype;
    }

    //    public ContactType(Integer id, String name, Datatype datatype) {
//        super(id);
//        this.name = name;
//        this.datatype = datatype;
//    }
//
//    public ContactType(Integer id, User creator, Date dateCreated, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, Datatype datatype) {
//        super(id, creator, dateCreated, voided, voidedBy, voidedDate, voidedCause);
//        this.name = name;
//        this.datatype = datatype;
//    }
//
//    public ContactType(Integer id, User creator, Date dateCreated, boolean voided, User voidedBy, Date voidedDate, String voidedCause, User changedBy, Date dateChanged, ContactType oldVersion, ContactType newVersion, String name, Datatype datatype) {
//        super(id, creator, dateCreated, voided, voidedBy, voidedDate, voidedCause, changedBy, dateChanged, oldVersion, newVersion);
//        this.name = name;
//        this.datatype = datatype;
//    }
}
