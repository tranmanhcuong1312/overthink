package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import north.Overthink.model.templatemodel.State;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaction")
@Data
public class Transaction extends StandardModel<Transaction> {

    @ManyToOne
    @JoinColumn(name = "transaction_type")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private TransactionType transactionType;

    @Column(name = "value")
    @FieldCanHaveCondition
    private double value;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    @FieldCanHaveCondition
    private State state;

    public Transaction(){

    }

    public Transaction(Transaction transaction){
        this.transactionType = transaction.getTransactionType();
        this.value = transaction.getValue();
        this.content = transaction.getContent();
        this.description = transaction.getDescription();
        this.state = transaction.getState();
    }

    public Transaction(TransactionType transactionType, double value, String content, String description, State state) {
        this.transactionType = transactionType;
        this.value = value;
        this.content = content;
        this.description = description;
        this.state = state;
    }

    public Transaction(Integer id, TransactionType transactionType, double value, String content, String description, State state) {
        super(id);
        this.transactionType = transactionType;
        this.value = value;
        this.content = content;
        this.description = description;
        this.state = state;
    }

    public Transaction(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Transaction oldVersion, Transaction newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, TransactionType transactionType, double value, String content, String description, State state) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.transactionType = transactionType;
        this.value = value;
        this.content = content;
        this.description = description;
        this.state = state;
    }
}
