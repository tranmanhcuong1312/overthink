package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_contact")
@Data
public class UserContact extends StandardModel<UserContact> {

    @ManyToOne
    @JoinColumn(name = "user_profile")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UserProfile.class)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private UserProfile userProfile;

    @ManyToOne
    @JoinColumn(name = "contact_type")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private ContactType contactType;

    @Column(name = "value")
    @FieldCanHaveCondition
    private String value;

    public UserContact(){

    }

    public UserContact(UserContact userContact){
        this.userProfile = userContact.getUserProfile();
        this.contactType = userContact.getContactType();
        this.value = userContact.getValue();
    }

    public UserContact(UserProfile userProfile, ContactType contactType, String value) {
        this.userProfile = userProfile;
        this.contactType = contactType;
        this.value = value;
    }

    public UserContact(Integer id, UserProfile userProfile, ContactType contactType, String value) {
        super(id);
        this.userProfile = userProfile;
        this.contactType = contactType;
        this.value = value;
    }

    public UserContact(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, UserContact oldVersion, UserContact newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, UserProfile userProfile, ContactType contactType, String value) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.userProfile = userProfile;
        this.contactType = contactType;
        this.value = value;
    }
}
