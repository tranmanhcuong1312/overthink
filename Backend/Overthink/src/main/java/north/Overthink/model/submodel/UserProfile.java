package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_profile")
@Data
public class UserProfile extends StandardModel<UserProfile> {

    @OneToOne
    @JoinColumn(name = "user")
    @JsonIgnoreProperties(value = {"person", "role"})
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User user;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "profile_link")
    private String profileLink;

//    @OneToMany
//    @JoinColumn(name = "user_profile")
//    private List<UserActive> userActives;
//
//    @OneToMany
//    @JoinColumn(name = "user_profile")
//    private List<UserContact> userContacts;

//    @OneToOne
//    @JoinColumn(name = "user_profile")
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UserRevenue.class)
//    @JsonIgnoreProperties(value = {"creator", "changedBy", "voidedBy"})
//    private UserRevenue userRevenue;

    public UserProfile(){

    }

    public UserProfile(UserProfile userProfile){
        this.user = userProfile.getUser();
        this.name = userProfile.getName();
        this.profileLink = userProfile.getProfileLink();
    }

    public UserProfile(User user, String name, String profileLink) {
        this.user = user;
        this.name = name;
        this.profileLink = profileLink;
//        this.userRevenue = userRevenue;
    }

    public UserProfile(Integer id, User user, String name, String profileLink) {
        super(id);
        this.user = user;
        this.name = name;
        this.profileLink = profileLink;
//        this.userRevenue = userRevenue;
    }

    public UserProfile(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, UserProfile oldVersion, UserProfile newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, User user, String name, String profileLink) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.user = user;
        this.name = name;
        this.profileLink = profileLink;
    }
}
