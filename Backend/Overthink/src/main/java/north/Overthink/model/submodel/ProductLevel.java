package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "product_level")
@Data
public class ProductLevel extends StandardModel<ProductLevel> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "order_condition")
    @FieldCanHaveCondition
    private int orderCondition;

    @Column(name = "rate_condition")
    @FieldCanHaveCondition
    private double rateCondition;

    @Column(name = "description")
    private String description;

    public ProductLevel(){

    }

    public ProductLevel(ProductLevel productLevel){
        this.name = productLevel.getName();
        this.orderCondition = productLevel.getOrderCondition();
        this.rateCondition = productLevel.getRateCondition();
        this.description = productLevel.getDescription();
    }

    public ProductLevel(String name, int orderCondition, double rateCondition, String description) {
        this.name = name;
        this.orderCondition = orderCondition;
        this.rateCondition = rateCondition;
        this.description = description;
    }

    public ProductLevel(Integer id, String name, int orderCondition, double rateCondition, String description) {
        super(id);
        this.name = name;
        this.orderCondition = orderCondition;
        this.rateCondition = rateCondition;
        this.description = description;
    }

    public ProductLevel(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, ProductLevel oldVersion, ProductLevel newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, int orderCondition, double rateCondition, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.orderCondition = orderCondition;
        this.rateCondition = rateCondition;
        this.description = description;
    }
}
