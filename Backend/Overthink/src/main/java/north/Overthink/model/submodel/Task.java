package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "task")
@Data
public class Task extends StandardModel<Task> {

    @ManyToOne
    @JoinColumn(name = "task_type")
    @FieldCanHaveCondition
    @StandardJsonIgnoreProperties
    private TaskType taskType;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @Column(name = "priority")
    @FieldCanHaveCondition
    private String priority;

    public Task(){

    }

    public Task(Task task){
        this.taskType = task.getTaskType();
        this.name = task.getName();
        this.content = task.getContent();
        this.priority = task.priority;
    }

    public Task(TaskType taskType, String name, String content, String priority) {
        this.taskType = taskType;
        this.name = name;
        this.content = content;
        this.priority = priority;
    }

    public Task(Integer id, TaskType taskType, String name, String content, String priority) {
        super(id);
        this.taskType = taskType;
        this.name = name;
        this.content = content;
        this.priority = priority;
    }

    public Task(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Task oldVersion, Task newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, TaskType taskType, String name, String content, String priority) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.taskType = taskType;
        this.name = name;
        this.content = content;
        this.priority = priority;
    }
}
