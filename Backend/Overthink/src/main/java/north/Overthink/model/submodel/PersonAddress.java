package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "person_address")
@Data
public class PersonAddress extends StandardModel<PersonAddress> {

    @ManyToOne
    @JoinColumn(name = "person")
    @StandardJsonIgnoreProperties
    private Person person;

    @Column(name = "address")
    @FieldCanHaveCondition
    private String address;

    @Column(name = "city_village")
    @FieldCanHaveCondition
    private String cityVillage;

    @Column(name = "state_province")
    @FieldCanHaveCondition
    private String stateProvince;

    @Column(name = "country")
    @FieldCanHaveCondition
    private String country;

    @Column(name = "postal_code")
    @FieldCanHaveCondition
    private String postalCode;

    public PersonAddress(){

    }

    public PersonAddress(PersonAddress personAddress){
        this.person = personAddress.getPerson();
        this.address = personAddress.getAddress();
        this.cityVillage = personAddress.getCityVillage();
        this.stateProvince = personAddress.getStateProvince();
        this.country = personAddress.getCountry();
        this.postalCode = personAddress.getPostalCode();
    }

    public PersonAddress(String address, String cityVillage, String stateProvince, String country, String postalCode) {
        super();
        this.person = person;
        this.address = address;
        this.cityVillage = cityVillage;
        this.stateProvince = stateProvince;
        this.country = country;
        this.postalCode = postalCode;
    }

    public PersonAddress(Integer id, String address, String cityVillage, String stateProvince, String country, String postalCode) {
        super(id);
        this.person = person;
        this.address = address;
        this.cityVillage = cityVillage;
        this.stateProvince = stateProvince;
        this.country = country;
        this.postalCode = postalCode;
    }

    public PersonAddress(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, PersonAddress oldVersion, PersonAddress newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Person person, String address, String cityVillage, String stateProvince, String country, String postalCode) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.person = person;
        this.address = address;
        this.cityVillage = cityVillage;
        this.stateProvince = stateProvince;
        this.country = country;
        this.postalCode = postalCode;
    }
}
