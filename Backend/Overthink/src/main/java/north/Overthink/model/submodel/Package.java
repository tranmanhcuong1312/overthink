package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "package")
@Data
public class Package extends StandardModel<Package> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public Package(){

    }

    public Package(Package aPackage){
        this.name = aPackage.getName();
        this.description = aPackage.getDescription();
    }

    public Package(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Package(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public Package(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Package oldVersion, Package newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
