package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "order_course")
@Data
public class OrderCourse extends StandardModel<OrderCourse> {

    @OneToOne
    @JoinColumn(name = "order")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Order order;

    @ManyToOne
    @JoinColumn(name = "course")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Course course;

    public OrderCourse(){

    }

    public OrderCourse(OrderCourse orderCourse){
        this.order = orderCourse.getOrder();
        this.course = orderCourse.getCourse();
    }

    public OrderCourse(Order order, Course course) {
        this.order = order;
        this.course = course;
    }

    public OrderCourse(Integer id, Order order, Course course) {
        super(id);
        this.order = order;
        this.course = course;
    }

    public OrderCourse(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, OrderCourse oldVersion, OrderCourse newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Order order, Course course) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.order = order;
        this.course = course;
    }
}
