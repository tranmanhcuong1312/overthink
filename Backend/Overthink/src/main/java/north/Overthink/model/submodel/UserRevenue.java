package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user_revenue")
@Data
public class UserRevenue extends StandardModel<UserRevenue> {

    @Column(name = "total_income")
    @FieldCanHaveCondition
    private double totalIncome;

    @Column(name = "income_per_month")
    @FieldCanHaveCondition
    private double incomePerMonth;

    @OneToOne
    @JoinColumn(name = "user_profile")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UserProfile.class)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private UserProfile userProfile;

    @OneToMany(mappedBy = "userRevenue")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Revenue.class)
    @StandardJsonIgnoreProperties
    private List<Revenue> revenues;

    public UserRevenue(){

    }

    public UserRevenue(UserRevenue userRevenue){
        this.totalIncome = userRevenue.getTotalIncome();
        this.incomePerMonth = userRevenue.getIncomePerMonth();
        this.userProfile = userRevenue.getUserProfile();
        this.revenues = userRevenue.getRevenues();
    }

    public UserRevenue(double totalIncome, double incomePerMonth, UserProfile userProfile, List<Revenue> revenues) {
        this.totalIncome = totalIncome;
        this.incomePerMonth = incomePerMonth;
        this.userProfile = userProfile;
        this.revenues = revenues;
    }

    public UserRevenue(Integer id, double totalIncome, double incomePerMonth, UserProfile userProfile, List<Revenue> revenues) {
        super(id);
        this.totalIncome = totalIncome;
        this.incomePerMonth = incomePerMonth;
        this.userProfile = userProfile;
        this.revenues = revenues;
    }

    public UserRevenue(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, UserRevenue oldVersion, UserRevenue newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, double totalIncome, double incomePerMonth, UserProfile userProfile, List<Revenue> revenues) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.totalIncome = totalIncome;
        this.incomePerMonth = incomePerMonth;
        this.userProfile = userProfile;
        this.revenues = revenues;
    }
}
