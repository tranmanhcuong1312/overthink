package north.Overthink.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.submodel.User;

import javax.persistence.*;
import java.util.Date;

//@MappedSuperclass
public abstract class StandardModelVersion<T extends StandardModelVersion> extends StandardModel {

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "changed_by", nullable = true)
//    @StandardJsonIgnoreProperties
//    @FieldCanHaveCondition
//    private User changedBy;
//
//    @Column(name = "date_changed")
//    private Date dateChanged;
//
//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "old_version", nullable = true)
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//    @StandardJsonIgnoreProperties
//    private T oldVersion;
//
//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "new_version", nullable = true)
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//    @StandardJsonIgnoreProperties
//    private T newVersion;
//
//    public StandardModelVersion(){
//
//    }
//
//    public StandardModelVersion(User changedBy, Date dateChanged, T oldVersion, T newVersion) {
//        this.changedBy = changedBy;
//        this.dateChanged = dateChanged;
//        this.oldVersion = oldVersion;
//        this.newVersion = newVersion;
//    }
//
//    public StandardModelVersion(Integer id){
//        super(id);
//    }
//
//    public StandardModelVersion(Integer id, User creator, Date dateCreated, boolean voided, User voidedBy, Date voidedDate, String voidedCause) {
//        super(id, creator, dateCreated, voided, voidedBy, voidedDate, voidedCause);
//    }
//
//    public StandardModelVersion(Integer id, User changedBy, Date dateChanged, T oldVersion, T newVersion) {
//        super(id);
//        this.changedBy = changedBy;
//        this.dateChanged = dateChanged;
//        this.oldVersion = oldVersion;
//        this.newVersion = newVersion;
//    }
//
//    public StandardModelVersion(Integer id, User creator, Date dateCreated, boolean voided, User voidedBy, Date voidedDate, String voidedCause, User changedBy, Date dateChanged, T oldVersion, T newVersion) {
//        super(id, creator, dateCreated, voided, voidedBy, voidedDate, voidedCause);
//        this.changedBy = changedBy;
//        this.dateChanged = dateChanged;
//        this.oldVersion = oldVersion;
//        this.newVersion = newVersion;
//    }
}
