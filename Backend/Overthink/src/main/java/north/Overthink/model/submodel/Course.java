package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "course")
@Data
public class Course extends StandardModel<Course> {

    @Column(name = "active")
    private Boolean active;

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "content")
    @FieldCanHaveCondition
    private String content;

    @ManyToOne
    @JoinColumn(name = "job")
    @StandardJsonIgnoreProperties
    private Job job;

    @Column(name = "price")
    @FieldCanHaveCondition
    private int price;

    @Column(name = "lessons")
    private int lessons;

    @Column(name = "evaluation_rate")
    private int evaluationRate;

    public Course(){

    }

    public Course(Course course){
        this.active = course.getActive();
        this.name = course.getName();
        this.content = course.getContent();
        this.job = course.getJob();
        this.price = course.getPrice();
        this.lessons = course.getLessons();
        this.evaluationRate = course.getEvaluationRate();
    }

    public Course(Boolean active, String name, String content, Job job, int price, int lessons, int evaluationRate) {
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.price = price;
        this.lessons = lessons;
        this.evaluationRate = evaluationRate;
    }

    public Course(Integer id, Boolean active, String name, String content, Job job, int price, int lessons, int evaluationRate) {
        super(id);
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.price = price;
        this.lessons = lessons;
        this.evaluationRate = evaluationRate;
    }

    public Course(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Course oldVersion, Course newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Boolean active, String name, String content, Job job, int price, int lessons, int evaluationRate) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.active = active;
        this.name = name;
        this.content = content;
        this.job = job;
        this.price = price;
        this.lessons = lessons;
        this.evaluationRate = evaluationRate;
    }
}
