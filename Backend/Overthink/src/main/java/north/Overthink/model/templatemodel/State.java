package north.Overthink.model.templatemodel;

public enum State {

    WAITING("waiting"),
    DELAY("delay"),
    DONE("done");

    private String value;
    State(String value){
        this.value = value;
    }
}
