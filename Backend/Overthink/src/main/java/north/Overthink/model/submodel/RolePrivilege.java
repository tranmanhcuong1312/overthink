package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "role_privilege")
@Data
public class RolePrivilege extends StandardModel<RolePrivilege> {

    @ManyToOne
    @JoinColumn(name = "role")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Role role;

    @ManyToOne
    @JoinColumn(name = "privilege")
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private Privilege privilege;

    public RolePrivilege(){};

    public RolePrivilege(RolePrivilege rolePrivilege){
        this.role = rolePrivilege.getRole();
        this.privilege = rolePrivilege.getPrivilege();
    }

    public RolePrivilege(Role role, Privilege privilege) {
        this.role = role;
        this.privilege = privilege;
    }

    public RolePrivilege(Integer id, Role role, Privilege privilege) {
        super(id);
        this.role = role;
        this.privilege = privilege;
    }

    public RolePrivilege(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, RolePrivilege oldVersion, RolePrivilege newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Role role, Privilege privilege) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.role = role;
        this.privilege = privilege;
    }
}
