package north.Overthink.model;

import com.fasterxml.jackson.annotation.*;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.submodel.User;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class StandardModel<T extends StandardModel> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @FieldCanHaveCondition
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator", nullable = true)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User creator;

    @Column(name = "date_created")
    @CreatedDate
    @Generated(GenerationTime.ALWAYS)
    private Date dateCreated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "changed_by", nullable = true)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User changedBy;

    @Column(name = "date_changed")
    private Date dateChanged;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "old_version", nullable = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @StandardJsonIgnoreProperties
    private T oldVersion;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "new_version", nullable = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @StandardJsonIgnoreProperties
    private T newVersion;

    @Column(name = "voided")
    @FieldCanHaveCondition
    private boolean voided;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voided_by", nullable = true)
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User voidedBy;

    @Column(name = "voided_date")
    private Date voidedDate;

    @Column(name = "voided_cause")
    private String voidedCause;


    public StandardModel() {

    }

    @JsonCreator
    public StandardModel(@JsonProperty("id") Integer id){
        this.id = id;
    }

    public StandardModel(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, T oldVersion, T newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause) {
        this.id = id;
        this.creator = creator;
        this.dateCreated = dateCreated;
        this.changedBy = changedBy;
        this.dateChanged = dateChanged;
        this.oldVersion = oldVersion;
        this.newVersion = newVersion;
        this.voided = voided;
        this.voidedBy = voidedBy;
        this.voidedDate = voidedDate;
        this.voidedCause = voidedCause;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isVoided() {
        return voided;
    }

    public void setVoided(boolean voided) {
        this.voided = voided;
    }

    public User getVoidedBy() {
        return voidedBy;
    }

    public void setVoidedBy(User voidedBy) {
        this.voidedBy = voidedBy;
    }

    public Date getVoidedDate() {
        return voidedDate;
    }

    public void setVoidedDate(Date voidedDate) {
        this.voidedDate = voidedDate;
    }

    public String getVoidedCause() {
        return voidedCause;
    }

    public void setVoidedCause(String voidedCause) {
        this.voidedCause = voidedCause;
    }

    public User getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(User changedBy) {
        this.changedBy = changedBy;
    }

    public Date getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }

    public T getOldVersion() {
        return oldVersion;
    }

    public void setOldVersion(T oldVersion) {
        this.oldVersion = oldVersion;
    }

    public T getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(T newVersion) {
        this.newVersion = newVersion;
    }
}
