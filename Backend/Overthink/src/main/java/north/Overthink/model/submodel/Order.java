package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import north.Overthink.model.templatemodel.State;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "order")
@Data
public class Order extends StandardModel<Order> {

    @ManyToOne
    @JoinColumn(name = "buyer")
    @JsonIgnoreProperties(value = {"person", "role"})
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User buyer;

    @ManyToOne
    @JoinColumn(name = "seller")
    @JsonIgnoreProperties(value = {"person", "role"})
    @StandardJsonIgnoreProperties
    @FieldCanHaveCondition
    private User seller;

    @Column(name = "dealine")
    @FieldCanHaveCondition
    private String deadline;

    @OneToOne
    @JoinColumn(name = "transaction")
    @FieldCanHaveCondition
    @StandardJsonIgnoreProperties
    private Transaction transaction;

    public Order(){

    }

    public Order(Order order){
        this.buyer = order.getBuyer();
        this.seller = order.getSeller();
        this.deadline = order.getDeadline();
        this.transaction = order.getTransaction();
    }

    public Order(User buyer, User seller, String deadline, Transaction transaction) {
        this.buyer = buyer;
        this.seller = seller;
        this.deadline = deadline;
        this.transaction = transaction;
    }

    public Order(Integer id, User buyer, User seller, String deadline, Transaction transaction) {
        super(id);
        this.buyer = buyer;
        this.seller = seller;
        this.deadline = deadline;
        this.transaction = transaction;
    }

    public Order(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Order oldVersion, Order newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, User buyer, User seller, String deadline, Transaction transaction) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.buyer = buyer;
        this.seller = seller;
        this.deadline = deadline;
        this.transaction = transaction;
    }
}
