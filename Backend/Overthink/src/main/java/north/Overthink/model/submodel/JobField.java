package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "job_field")
@Data
public class JobField extends StandardModel<JobField> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "jobField", cascade = CascadeType.ALL)
    @JsonProperty( access = JsonProperty.Access.READ_WRITE)
    @StandardJsonIgnoreProperties
    private List<Job> jobs;

    public JobField(){

    }

    public JobField(JobField jobField){
        this.name = jobField.getName();
        this.description = jobField.getDescription();
        this.jobs = jobField.getJobs();
    }

    public JobField(String name, String description, List<Job> jobs) {
        this.name = name;
        this.description = description;
        this.jobs = jobs;
    }

    public JobField(Integer id, String name, String description, List<Job> jobs) {
        super(id);
        this.name = name;
        this.description = description;
        this.jobs = jobs;
    }

    public JobField(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, JobField oldVersion, JobField newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description, List<Job> jobs) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
        this.jobs = jobs;
    }
}
