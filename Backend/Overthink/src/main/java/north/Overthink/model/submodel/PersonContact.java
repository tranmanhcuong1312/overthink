package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "person_contact")
@Data
public class PersonContact extends StandardModel<PersonContact> {

    @ManyToOne
    @JoinColumn(name = "person")
    @StandardJsonIgnoreProperties
    private Person person;

    @ManyToOne
    @JoinColumn(name = "contact_type")
    @FieldCanHaveCondition
    private ContactType contactType;

    @Column(name = "description")
    private String description;

    @Column(name = "value")
    @FieldCanHaveCondition
    private String value;

    public PersonContact(){

    }

    public PersonContact(PersonContact personContact){
        this.person = personContact.getPerson();
        this.contactType = personContact.getContactType();
        this.description = personContact.getDescription();
        this.value = personContact.getDescription();
    }

    public PersonContact(ContactType contactType, String description, String value) {
        super();
        this.person = person;
        this.contactType = contactType;
        this.description = description;
        this.value = value;
    }

    public PersonContact(Integer id, ContactType contactType, String description, String value) {
        super(id);
        this.person = person;
        this.contactType = contactType;
        this.description = description;
        this.value = value;
    }

    public PersonContact(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, PersonContact oldVersion, PersonContact newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, Person person, ContactType contactType, String description, String value) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.person = person;
        this.contactType = contactType;
        this.description = description;
        this.value = value;
    }
}
