package north.Overthink.model.templatemodel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConditionPaging {

    private List<Condition> conditions;

    private CustomPageRequest customPageRequest;

    private boolean includeVoided = false;
}
