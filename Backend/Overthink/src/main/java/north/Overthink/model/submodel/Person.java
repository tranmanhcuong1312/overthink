package north.Overthink.model.submodel;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import north.Overthink.annotation.StandardJsonIgnoreProperties;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "person")
@Data
public class Person extends StandardModel<Person> {

    @Column(name = "gender")
    private String gender;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "death")
    private boolean death;

    @Column(name = "death_date")
    @CreatedDate
    private LocalDateTime deathDate;

    @Column(name = "death_cause")
    private String deathCause;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = User.class)
    @StandardJsonIgnoreProperties
    private List<User> users;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PersonName.class)
    @StandardJsonIgnoreProperties
    private List<PersonName> personNames;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PersonAddress.class)
    @StandardJsonIgnoreProperties
    private List<PersonAddress> personAddresses;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "person")
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PersonJob.class)
//    @StandardJsonIgnoreProperties
//    private List<PersonJob> personJobs;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PersonContact.class)
    @StandardJsonIgnoreProperties
    private List<PersonContact> personContacts;

    public Person(){

    }

    public Person(Person person){
        this.gender = person.getGender();
        this.birthday = person.getBirthday();
        this.death = person.isDeath();
        this.deathDate = person.getDeathDate();
        this.deathCause = person.getDeathCause();
        this.users = person.getUsers();
        this.personNames = person.getPersonNames();
        this.personAddresses = person.getPersonAddresses();
        this.personContacts = person.getPersonContacts();
    }

    public Person(Integer id, String gender, LocalDate birthday, boolean death, LocalDateTime deathDate, String deathCause, List<User> users, List<PersonName> personNames, List<PersonAddress> personAddresses, List<PersonJob> personJobs, List<PersonContact> personContacts) {
        super(id);
        this.gender = gender;
        this.birthday = birthday;
        this.death = death;
        this.deathDate = deathDate;
        this.deathCause = deathCause;
        this.users = users;
        this.personNames = personNames;
        this.personAddresses = personAddresses;
        this.personContacts = personContacts;
    }

    public Person(String gender, LocalDate birthday, boolean death, LocalDateTime deathDate, String deathCause, List<User> users, List<PersonName> personNames, List<PersonAddress> personAddresses, List<PersonJob> personJobs, List<PersonContact> personContacts) {
        this.gender = gender;
        this.birthday = birthday;
        this.death = death;
        this.deathDate = deathDate;
        this.deathCause = deathCause;
        this.users = users;
        this.personNames = personNames;
        this.personAddresses = personAddresses;
        this.personContacts = personContacts;
    }

    public Person(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, Person oldVersion, Person newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String gender, LocalDate birthday, boolean death, LocalDateTime deathDate, String deathCause, List<User> users, List<PersonName> personNames, List<PersonAddress> personAddresses, List<PersonContact> personContacts) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.gender = gender;
        this.birthday = birthday;
        this.death = death;
        this.deathDate = deathDate;
        this.deathCause = deathCause;
        this.users = users;
        this.personNames = personNames;
        this.personAddresses = personAddresses;
        this.personContacts = personContacts;
    }
}
