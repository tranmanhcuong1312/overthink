package north.Overthink.model.submodel;

import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.model.StandardModel;
import org.joda.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "transaction_type")
@Data
public class TransactionType extends StandardModel<TransactionType> {

    @Column(name = "name")
    @FieldCanHaveCondition
    private String name;

    @Column(name = "description")
    private String description;

    public TransactionType(){

    }

    public TransactionType(TransactionType transactionType){
        this.name = transactionType.getName();
        this.description = transactionType.getDescription();
    }

    public TransactionType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TransactionType(Integer id, String name, String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    public TransactionType(Integer id, User creator, Date dateCreated, User changedBy, Date dateChanged, TransactionType oldVersion, TransactionType newVersion, boolean voided, User voidedBy, Date voidedDate, String voidedCause, String name, String description) {
        super(id, creator, dateCreated, changedBy, dateChanged, oldVersion, newVersion, voided, voidedBy, voidedDate, voidedCause);
        this.name = name;
        this.description = description;
    }
}
