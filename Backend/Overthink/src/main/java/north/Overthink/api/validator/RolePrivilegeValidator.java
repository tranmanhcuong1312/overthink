package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.RolePrivilege;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = RolePrivilege.class)
public class RolePrivilegeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return RolePrivilege.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Role privilege validator");
    }
}
