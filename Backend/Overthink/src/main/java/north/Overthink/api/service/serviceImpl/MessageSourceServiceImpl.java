package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.customservice.MessageSourceService;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

import java.util.Locale;

public class MessageSourceServiceImpl implements MessageSourceService {

    @Override
    public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return null;
    }

    @Override
    public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
        return null;
    }

    @Override
    public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        return null;
    }

    @Override
    public String getMessage(String code, Object object) {
        return code + ": " + object.getClass().getName();
    }
}
