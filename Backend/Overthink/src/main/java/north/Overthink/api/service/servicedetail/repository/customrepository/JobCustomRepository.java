package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;

import java.util.List;

public interface JobCustomRepository {

    List<Job> getJobsWithCondition(String query, int firstResult, int maxResult);
}
