package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ActivityType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ActivityTypeServiceImpl extends InjectionServiceImpl implements StandardService<ActivityType, Integer> {

    @Autowired
    private StandardServiceDetail<ActivityType, Integer> standardServiceDetail;

    @Override
    public ActivityType save(ActivityType activityType) {
        return standardServiceDetail.save(activityType);
    }

    @Override
    public ActivityType update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        ActivityType activityType = standardServiceDetail.update(customObject);
        return activityType;
    }

    @Override
    public ActivityType get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<ActivityType> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public ActivityType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
