package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.RevenueRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ActivityType;
import north.Overthink.model.submodel.Revenue;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class RevenueServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Revenue, Integer> {

    @Autowired
    private RevenueRepository revenueRepository;

    @Override
    public Revenue save(Revenue revenue) {
        getStandardModelChangedHandler().createHandler(revenue);
        return revenueRepository.save(revenue);
    }

    @Override
    public Revenue update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Revenue revenue = revenueRepository
                    .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Revenue.class, customObject, revenue
        );
        return revenueRepository.save(revenue);
    }

    @Override
    public Revenue get(Integer integer) {
        return revenueRepository.findById(integer).get();
    }

    @Override
    public Page<Revenue> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Revenue.class);
        return revenueRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Revenue voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Revenue revenue = revenueRepository
                    .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Revenue.class, voidedStandardModel, revenue
        );
        return revenueRepository.save(revenue);
    }
}
