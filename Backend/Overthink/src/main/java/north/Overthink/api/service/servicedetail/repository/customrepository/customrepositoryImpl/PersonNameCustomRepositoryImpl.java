package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PersonNameCustomRepository;
import north.Overthink.model.submodel.PersonName;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonNameCustomRepositoryImpl implements PersonNameCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PersonName> getPersonNamesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<PersonName> typedQuery = entityManager
                .createQuery(query, PersonName.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
