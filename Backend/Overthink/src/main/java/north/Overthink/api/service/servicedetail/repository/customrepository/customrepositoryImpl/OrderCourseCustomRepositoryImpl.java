package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.OrderCourseCustomRepository;
import north.Overthink.model.submodel.OrderCourse;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderCourseCustomRepositoryImpl implements OrderCourseCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrderCourse> getOrderCoursesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<OrderCourse> typedQuery = entityManager
                .createQuery(query, OrderCourse.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
