package north.Overthink.api.handler;

import north.Overthink.model.templatemodel.UserInfoAuthentication;

import java.io.Serializable;

public class UserContext implements Serializable {

    private static UserInfoAuthentication userInfoAuthentication;

    public UserContext(UserInfoAuthentication userInfoAuthentication) {
        this.userInfoAuthentication = userInfoAuthentication;
    }

    public static UserInfoAuthentication getUserInfoAuthentication() {
        return userInfoAuthentication;
    }

    public static void setUserInfoAuthentication(UserInfoAuthentication userInfoAuthentication) {
        UserContext.userInfoAuthentication = userInfoAuthentication;
    }
}
