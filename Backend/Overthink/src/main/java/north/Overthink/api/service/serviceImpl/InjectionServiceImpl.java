package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.handler.JsonMapperHandler;
import north.Overthink.api.handler.ServiceContext;
import north.Overthink.api.service.customservice.InjectionService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class InjectionServiceImpl extends ServiceContext implements InjectionService {

    @Autowired
    private JsonMapperHandler jsonMapperHandler;

    public JsonMapperHandler getJsonMapperHandler() {
        return jsonMapperHandler;
    }

}
