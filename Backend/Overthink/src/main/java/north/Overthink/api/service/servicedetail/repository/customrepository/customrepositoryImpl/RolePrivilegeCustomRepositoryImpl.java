package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.RolePrivilegeCustomRepository;
import north.Overthink.model.submodel.RolePrivilege;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class RolePrivilegeCustomRepositoryImpl implements RolePrivilegeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RolePrivilege> getRolePrivilegesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<RolePrivilege> typedQuery = entityManager
                .createQuery(query, RolePrivilege.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
