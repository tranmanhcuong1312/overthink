package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.NotificationType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class NotificationTypeServiceImpl extends InjectionServiceImpl implements StandardService<NotificationType, Integer> {

    @Autowired
    private StandardServiceDetail<NotificationType, Integer> standardServiceDetail;

    @Override
    public NotificationType save(NotificationType notificationType) {
        return standardServiceDetail.save(notificationType);
    }

    @Override
    public NotificationType update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public NotificationType get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<NotificationType> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public NotificationType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
