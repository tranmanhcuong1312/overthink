package north.Overthink.api.handler;

import north.Overthink.model.StandardModel;
import north.Overthink.model.VoidedStandardModel;

public interface StandardModelChanged {

    <T extends StandardModel> void created(T t);

    <T extends StandardModel> void changed(T t);

    <T extends StandardModel> void changedWithVersions(T oldT, T newT);

    <T extends StandardModel> void voided(T t, VoidedStandardModel voidedStandardModel);
}
