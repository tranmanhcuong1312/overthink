package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.OrderCourse;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class OrderCourseServiceImpl extends InjectionServiceImpl implements StandardService<OrderCourse, Integer> {

    @Autowired
    private StandardServiceDetail<OrderCourse, Integer> standardServiceDetail;

    @Override
    public OrderCourse save(OrderCourse orderCourse) {
        return standardServiceDetail.save(orderCourse);
    }

    @Override
    public OrderCourse update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public OrderCourse get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<OrderCourse> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public OrderCourse voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
