package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.TaskType;

import java.util.List;

public interface TaskTypeCustomRepository {

    List<TaskType> getTaskTypesWithCondition(String query, int firstResult, int maxResult);
}
