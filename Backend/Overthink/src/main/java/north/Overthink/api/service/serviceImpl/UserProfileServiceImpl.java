package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserProfile;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserProfileServiceImpl extends InjectionServiceImpl implements StandardService<UserProfile, Integer> {

    @Autowired
    private StandardServiceDetail<UserProfile, Integer> standardServiceDetail;

    @Override
    public UserProfile save(UserProfile userProfile) {
        return standardServiceDetail.save(userProfile);
    }

    @Override
    public UserProfile update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public UserProfile get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<UserProfile> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public UserProfile voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
