package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.OrderCourse;

import java.util.List;

public interface OrderCourseCustomRepository {

    List<OrderCourse> getOrderCoursesWithCondition(String query, int firstResult, int maxResult);
}
