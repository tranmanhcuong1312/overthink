package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.TransactionTypeCustomRepository;
import north.Overthink.model.submodel.TransactionType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class TransactionTypeCustomRepositoryImpl implements TransactionTypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<TransactionType> getTransactionTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<TransactionType> typedQuery = entityManager
                .createQuery(query, TransactionType.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
