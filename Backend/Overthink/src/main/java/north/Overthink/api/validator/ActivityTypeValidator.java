package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.ActivityType;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = ActivityType.class)
public class ActivityTypeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return ActivityType.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Active type validator");
    }
}
