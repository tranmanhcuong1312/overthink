package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.GlobalProperty;

import java.util.List;

public interface GlobalPropertyCustomRepository {

    List<GlobalProperty> getGlobalPropertyWithCondition(String query, int firstResult, int maxResult);
}
