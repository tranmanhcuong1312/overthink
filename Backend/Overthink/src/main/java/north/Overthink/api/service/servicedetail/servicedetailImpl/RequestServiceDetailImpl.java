package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.RequestRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Request;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class RequestServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Request, Integer> {

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public Request save(Request request) {
        getStandardModelChangedHandler().createHandler(request);
        return requestRepository.save(request);
    }

    @Override
    public Request update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Request request = requestRepository
                    .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Request.class, customObject, request
        );
        return requestRepository.save(request);
    }

    @Override
    public Request get(Integer integer) {
        return requestRepository.findById(integer).get();
    }

    @Override
    public Page<Request> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Request.class);
        return requestRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Request voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Request request = requestRepository
                        .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Request.class, voidedStandardModel, request
        );
        return requestRepository.save(request);
    }
}
