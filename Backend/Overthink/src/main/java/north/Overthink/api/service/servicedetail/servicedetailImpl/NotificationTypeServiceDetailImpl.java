package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.NotificationTypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.NotificationType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class NotificationTypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<NotificationType, Integer> {

    @Autowired
    private NotificationTypeRepository notificationTypeRepository;

    @Override
    public NotificationType save(NotificationType notificationType) {
        getStandardModelChangedHandler().createHandler(notificationType);
        return notificationTypeRepository.save(notificationType);
    }

    @Override
    public NotificationType update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        NotificationType notificationType = notificationTypeRepository
                                        .findById((int) customObject.get("id")).get();
        NotificationType newNotificationType = new NotificationType(notificationType);
        getStandardModelChangedHandler().updateWithVersionHandler(
                NotificationType.class, customObject, notificationType, newNotificationType
        );
        newNotificationType = notificationTypeRepository.save(newNotificationType);
        notificationTypeRepository.save(notificationType);
        return notificationType;
    }

    @Override
    public NotificationType get(Integer integer) {
        return notificationTypeRepository.findById(integer).get();
    }

    @Override
    public Page<NotificationType> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), NotificationType.class);
        return notificationTypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public NotificationType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        NotificationType notificationType = notificationTypeRepository
                                        .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                NotificationType.class, voidedStandardModel, notificationType
        );
        return notificationTypeRepository.save(notificationType);
    }
}
