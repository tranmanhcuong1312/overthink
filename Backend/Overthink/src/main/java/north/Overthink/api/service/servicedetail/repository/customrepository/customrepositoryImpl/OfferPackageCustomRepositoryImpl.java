package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.OfferPackageCustomRepository;
import north.Overthink.model.submodel.OfferPackage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class OfferPackageCustomRepositoryImpl implements OfferPackageCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OfferPackage> getOfferPackagesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<OfferPackage> typedQuery = entityManager
                .createQuery(query, OfferPackage.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
