package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Role;
import north.Overthink.model.submodel.UserProfile;

import java.util.List;

public interface UserProfileCustomRepository {

    List<UserProfile> getUserProfilesWithCondition(String query, int firstResult, int maxResult);
}
