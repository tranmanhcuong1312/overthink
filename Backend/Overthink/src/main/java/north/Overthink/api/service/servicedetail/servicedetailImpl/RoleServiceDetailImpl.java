package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.RoleRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.exception.ResourceNotFoundException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Role;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@ServiceDetail
public class RoleServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Role, Integer> {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) {
        getStandardModelChangedHandler().createHandler(role);
        return roleRepository.save(role);
    }

    @Override
    public Role update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Role role = roleRepository
                .findById((int) customObject.get("id")).get();
        Role newRole = new Role(role);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Role.class, customObject, role, newRole
        );
        newRole = roleRepository.save(newRole);
        roleRepository.save(role);
        return newRole;
    }

    @Override
    public Role get(Integer integer) {
        try {
            return roleRepository.findById(integer).get();
        }catch (NoSuchElementException e){
            throw  new ResourceNotFoundException().resourceNotFound();
        }
    }

    @Override
    public Page<Role> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Role.class);
        return roleRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Role voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        Role role = roleRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Role.class, voidedStandardModel, role
        );
        return roleRepository.save(role);
    }
}
