package north.Overthink.api.controller.customcontroller;

import north.Overthink.api.handler.utils.ResponsePreConditionUtils;
import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.customservice.CustomOrderService;
import north.Overthink.model.submodel.Order;
import north.Overthink.model.submodel.Role;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/custom_order")
public class CustomOrderController {

    @Autowired
    private CustomOrderService customOrderService;

    @Autowired
    private StandardService<Role, Integer> standardService;

    @PostMapping("/course")
    public Order orderCourse(@RequestBody CustomObject customObject) throws NoSuchFieldException {
        return customOrderService.orderCourse(customObject);
    }

    @PostMapping("/offer")
    public Order orderOffer(@RequestBody CustomObject customObject) throws NoSuchFieldException {
        return customOrderService.orderOffer(customObject);
    }

    @PostMapping("/test")
    public ResponseEntity<?> responseEntity(@RequestBody ConditionPaging conditionPaging)
            throws IOException, NoSuchFieldException{
        return new ResponseEntity(
                ResponsePreConditionUtils.checkNotNUll(standardService.get(conditionPaging)),
                HttpStatus.ACCEPTED
        );
    }
}
