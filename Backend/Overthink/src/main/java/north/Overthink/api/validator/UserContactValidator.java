package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.UserContact;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = UserContact.class)
public class UserContactValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserContact.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("User contact validator");
    }
}
