package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.TransactionType;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = TransactionType.class)
public class TransactionTypeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return TransactionType.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Transaction type validator");
    }
}
