package north.Overthink.api.handler.handlerImpl;

import north.Overthink.api.handler.FieldReflectionHandler;
import north.Overthink.api.handler.StandardModelChanged;
import north.Overthink.api.handler.StandardModelChangedHandler;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.StandardModel;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;

@Component
public class StandardModelChangedHandlerImpl implements StandardModelChangedHandler, Serializable {

    @Autowired
    private FieldReflectionHandler fieldReflectionHandler;

    @Autowired
    private StandardModelChanged standardModelChanged;

    public FieldReflectionHandler getFieldReflectionHandler() {
        return fieldReflectionHandler;
    }

    public StandardModelChanged getStandardModelChanged() {
        return standardModelChanged;
    }

    @Override
    public <T extends StandardModel> void createHandler(T t) {
        getStandardModelChanged().created(t);
    }

    @Override
    public <T extends StandardModel> void updateHandler(Class<T> t, CustomObject customObject, T oldT) throws NoSuchThingForUpdateException, NoIdForUpdateException {
        getFieldReflectionHandler().update(t, customObject, oldT);
        getStandardModelChanged().changed(oldT);
    }

    @Override
    public <T extends StandardModel> void updateWithVersionHandler(Class<T> t, CustomObject customObject, T oldT, T newT) throws NoSuchThingForUpdateException, NoIdForUpdateException {
        getFieldReflectionHandler().update(t, customObject, newT);
        getStandardModelChanged().changedWithVersions(oldT, newT);
    }

    @Override
    public <T extends StandardModel> void voidedHandler(Class<T> t, VoidedStandardModel voidedStandardModel, T oldT) throws IOException, NoIdForUpdateException {
        getFieldReflectionHandler().voided(t, voidedStandardModel, oldT);
        getStandardModelChanged().voided(oldT, voidedStandardModel);
    }
}
