package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.JobCustomRepository;
import north.Overthink.model.submodel.Job;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class JobCustomRepositoryImpl implements JobCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Job> getJobsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Job> typedQuery = entityManager
                .createQuery(query, Job.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
