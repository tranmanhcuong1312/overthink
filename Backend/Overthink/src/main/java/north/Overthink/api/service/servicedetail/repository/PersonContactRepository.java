package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.PersonContactCustomRepository;
import north.Overthink.model.submodel.PersonContact;
import north.Overthink.model.submodel.QPersonContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonContactRepository extends JpaRepository<PersonContact, Integer>, PersonContactCustomRepository,
        QuerydslPredicateExecutor<PersonContact>, QuerydslBinderCustomizer<QPersonContact> {

    @Override
    default public void customize(QuerydslBindings bindings, QPersonContact root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
