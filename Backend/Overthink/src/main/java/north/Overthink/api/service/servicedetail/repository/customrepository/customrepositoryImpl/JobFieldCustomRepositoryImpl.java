package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.JobFieldCustomRepository;
import north.Overthink.model.submodel.JobField;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class JobFieldCustomRepositoryImpl implements JobFieldCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<JobField> getJobFieldWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<JobField> typedQuery = entityManager
                .createQuery(query, JobField.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
