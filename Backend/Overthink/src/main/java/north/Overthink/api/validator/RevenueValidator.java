package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Revenue;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Revenue.class)
public class RevenueValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Revenue.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Revenue validator");
    }
}
