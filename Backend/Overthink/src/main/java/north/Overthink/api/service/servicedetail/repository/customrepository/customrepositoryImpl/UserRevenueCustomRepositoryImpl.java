package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.UserRevenueCustomRepository;
import north.Overthink.model.submodel.UserRevenue;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserRevenueCustomRepositoryImpl implements UserRevenueCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserRevenue> getUserRevenuesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<UserRevenue> typedQuery = entityManager
                .createQuery(query, UserRevenue.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
