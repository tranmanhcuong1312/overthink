package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.UserProfile;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = UserProfile.class)
public class UserProfileValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserProfile.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("User contact validator");
    }
}
