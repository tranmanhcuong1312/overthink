package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.NotificationType;

import java.util.List;

public interface NotificationTypeCustomRepository {

    List<NotificationType> getNotificationTypesWithCondition(String query, int firstResult, int maxResult);
}
