package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.OrderCourseRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.OrderCourse;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class OrderCourseServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<OrderCourse, Integer> {

    @Autowired
    private OrderCourseRepository orderCourseRepository;

    @Override
    public OrderCourse save(OrderCourse orderCourse) {
        getStandardModelChangedHandler().createHandler(orderCourse);
        return orderCourseRepository.save(orderCourse);
    }

    @Override
    public OrderCourse update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        OrderCourse orderCourse = orderCourseRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                OrderCourse.class, customObject, orderCourse
        );
        return orderCourseRepository.save(orderCourse);
    }

    @Override
    public OrderCourse get(Integer integer) {
        return orderCourseRepository.findById(integer).get();
    }

    @Override
    public Page<OrderCourse> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), OrderCourse.class);
        return orderCourseRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public OrderCourse voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        OrderCourse orderCourse = orderCourseRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                OrderCourse.class, voidedStandardModel, orderCourse
        );
        return orderCourseRepository.save(orderCourse);
    }
}
