package north.Overthink.api.service;

import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.data.domain.Page;

import java.io.IOException;

public interface StandardService<T, ID> {

    T save(T t);

    T update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException;

    T get(ID id);

    Page<T> get(ConditionPaging conditionPaging) throws NoSuchFieldException;

    T voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException;
}
