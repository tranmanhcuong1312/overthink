package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.UserContactRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserContact;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class UserContactServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<UserContact, Integer> {

    @Autowired
    private UserContactRepository userContactRepository;

    @Override
    public UserContact save(UserContact userContact) {
        getStandardModelChangedHandler().createHandler(userContact);
        return userContactRepository.save(userContact);
    }

    @Override
    public UserContact update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        UserContact userContact = userContactRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                UserContact.class, customObject, userContact
        );
        return userContactRepository.save(userContact);
    }

    @Override
    public UserContact get(Integer integer) {
        return userContactRepository.findById(integer).get();
    }

    @Override
    public Page<UserContact> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), UserContact.class);
        return userContactRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public UserContact voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        UserContact userContact = userContactRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                UserContact.class, voidedStandardModel, userContact
        );
        return userContactRepository.save(userContact);
    }
}
