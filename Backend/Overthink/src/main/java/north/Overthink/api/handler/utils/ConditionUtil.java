package north.Overthink.api.handler.utils;

import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.Data;
import north.Overthink.annotation.FieldCanHaveCondition;
import north.Overthink.api.handler.FieldReflectionHandler;
import north.Overthink.api.handler.JsonMapperHandler;
import north.Overthink.model.StandardModel;
import north.Overthink.model.templatemodel.Condition;
import north.Overthink.model.templatemodel.CustomPageRequest;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

@Component
@Data
public class ConditionUtil implements Serializable, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Autowired
    private JsonMapperHandler jsonMapperHandler;

    @Autowired
    private FieldReflectionHandler fieldReflectionHandler;

    @Autowired
    private PredicateUtils predicateUtils;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public Pageable getPageable(CustomPageRequest customPageRequest){
        return customPageRequest.getPageable();
    }

    public boolean checkFieldCanHaveConditionOrNot(Field field){
        field.setAccessible(true);
        if(field.isAnnotationPresent(FieldCanHaveCondition.class)){
            return true;
        }
        return false;
    }

    public Field getField(String name, Class c) throws NoSuchFieldException {
        return fieldReflectionHandler.getField(name, c);
    }

    public List<Condition> filterConditions(List<Condition> conditions, Class c) throws NoSuchFieldException {
        Iterator<Condition> i = conditions.iterator();
        while(i.hasNext()){
            if(!checkFieldCanHaveConditionOrNot(getField(i.next().getField(), c))){
                i.remove();
            }
        }
        return conditions;
    }

    public <T extends StandardModel> BooleanExpression getPredicates(List<Condition> conditions, Class<T> c) throws NoSuchFieldException {
        return getPredicateUtils().getPredicates(filterConditions(conditions, c), c);
    }
}
