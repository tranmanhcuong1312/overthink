package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.CoursePackage;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(CoursePackage.class)
public class CoursePackageValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return CoursePackage.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Course Package Validator");
    }
}
