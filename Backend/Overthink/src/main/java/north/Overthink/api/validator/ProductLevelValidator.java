package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.ProductLevel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = ProductLevel.class)
public class ProductLevelValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return ProductLevel.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Product level");
    }
}
