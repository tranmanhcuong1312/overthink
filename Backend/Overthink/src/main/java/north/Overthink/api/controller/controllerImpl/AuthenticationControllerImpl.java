package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.customcontroller.AuthenticationController;
import north.Overthink.api.handler.utils.JwtTokenUtil;
import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.serviceImpl.UserDetailServiceImpl;
import north.Overthink.model.submodel.User;
import north.Overthink.model.templatemodel.AuthenticationRequest;
import north.Overthink.model.templatemodel.FinalKeyWords;
import north.Overthink.model.templatemodel.UserInfoAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin
public class AuthenticationControllerImpl implements AuthenticationController {

    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private StandardService<User, Integer> standardService;

    @Override
    public UserInfoAuthentication createAuthenticationToken(
            AuthenticationRequest authenticationRequest,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        return login(authenticationRequest, request, response);
    }

    @Override
    public UserInfoAuthentication saveUser(User user,
                                           HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {
        AuthenticationRequest authenticationRequest =
                new AuthenticationRequest(user.getUsername(), user.getPassword());
        User saveUser = standardService.save(user);
        if(saveUser != null){
            return login(authenticationRequest, request, response);
        }else{
            throw new Exception("Register fail");
        }
    }

    public UserInfoAuthentication login(AuthenticationRequest authenticationRequest,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Exception {
        UserInfoAuthentication userInfoAuthentication = userDetailService.createAuthentication(authenticationRequest);
        if(userInfoAuthentication != null){
//            HttpSession oldSession = request.getSession(false);
//
//            if(oldSession != null){
//                oldSession.invalidate();
//            }

//            HttpSession newSession = request.getSession(true);
//
//            newSession.setAttribute(FinalKeyWords.USER_INFO_AUTHENTICATION, userInfoAuthentication);
//            newSession.setMaxInactiveInterval((int) JwtTokenUtil.JWT_TOKEN_VALIDITY);
//
//            Cookie user = new Cookie(
//                    FinalKeyWords.USER_INFO_AUTHENTICATION,
//                    authenticationRequest.getUsername());
//
//            response.addCookie(user);
        }
        return userInfoAuthentication;
    }

    @Override
    public Boolean logout(HttpServletRequest request,
                          HttpServletResponse response) {
//        Cookie[] cookies = request.getCookies();
//        for(Cookie cookie: cookies){
//            cookie.setMaxAge(0);
//            response.addCookie(cookie);
//        }
//
//        HttpSession session = request.getSession(false);
//        if(session != null){
//            session.invalidate();
//        }

        return userDetailService.logout();
    }
}
