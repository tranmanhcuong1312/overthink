package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.OfferPackage;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(OfferPackage.class)
public class OfferPackageValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OfferPackage.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Offer package validator");
    }
}
