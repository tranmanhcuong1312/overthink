package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.UserRevenueRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;

import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserRevenue;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class UserRevenueServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<UserRevenue, Integer> {

    @Autowired
    private UserRevenueRepository userRevenueRepository;

    @Override
    public UserRevenue save(UserRevenue userRevenue) {
        getStandardModelChangedHandler().createHandler(userRevenue);
        return userRevenueRepository.save(userRevenue);
    }

    @Override
    public UserRevenue update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        UserRevenue userRevenue = userRevenueRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                UserRevenue.class, customObject, userRevenue
        );
        return userRevenueRepository.save(userRevenue);
    }

    @Override
    public UserRevenue get(Integer integer) {
        return userRevenueRepository.findById(integer).get();
    }

    @Override
    public Page<UserRevenue> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), UserRevenue.class);
        return userRevenueRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public UserRevenue voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        UserRevenue userRevenue = userRevenueRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                UserRevenue.class, voidedStandardModel, userRevenue
        );
        return userRevenueRepository.save(userRevenue);
    }
}
