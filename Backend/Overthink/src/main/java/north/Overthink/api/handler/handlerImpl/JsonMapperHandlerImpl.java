package north.Overthink.api.handler.handlerImpl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import north.Overthink.api.handler.JsonMapperHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


@Component
public class JsonMapperHandlerImpl implements JsonMapperHandler {

    private final ObjectMapper objectMapper;

    public JsonMapperHandlerImpl(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    @Override
    public String toString(Object object) throws IOException {
        return getObjectMapper().convertValue(object, String.class);
    }

    @Override
    public <T> T toJson(String string, Class<T> clazz) throws IOException {
        return getObjectMapper().convertValue(string, clazz);
    }

    @Override
    public <T, K> T jsonToJson(K k, Class<T> clazz) throws IOException {
        return getObjectMapper().convertValue(k, clazz);
    }

    @Override
    public <T> List<T> toListJson(String string, Class<T> clazz) throws IOException {
        return getObjectMapper().readValue(string, new TypeReference<List<T>>() {});
    }
}
