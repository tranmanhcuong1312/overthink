package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.RevenueCustomRepository;
import north.Overthink.model.submodel.Revenue;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class RevenueCustomRepositoryImpl implements RevenueCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Revenue> getRevenuesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Revenue> typedQuery = entityManager
                .createQuery(query, Revenue.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
