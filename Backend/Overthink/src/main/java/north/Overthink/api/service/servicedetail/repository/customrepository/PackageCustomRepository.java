package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.Package;

import java.util.List;

public interface PackageCustomRepository {

    List<Package> getPackagesWithCondition(String query, int firstResult, int maxResult);
}
