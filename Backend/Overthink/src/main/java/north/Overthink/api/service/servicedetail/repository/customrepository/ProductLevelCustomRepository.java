package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.ProductLevel;

import java.util.List;

public interface ProductLevelCustomRepository {

    List<ProductLevel> getProductLevelsWithCondition(String query, int firstResult, int maxResult);
}
