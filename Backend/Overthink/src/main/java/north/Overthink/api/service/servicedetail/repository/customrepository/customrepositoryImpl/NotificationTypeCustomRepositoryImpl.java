package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.NotificationTypeCustomRepository;
import north.Overthink.model.submodel.NotificationType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class NotificationTypeCustomRepositoryImpl implements NotificationTypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<NotificationType> getNotificationTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<NotificationType> typedQuery = entityManager
                .createQuery(query, NotificationType.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
