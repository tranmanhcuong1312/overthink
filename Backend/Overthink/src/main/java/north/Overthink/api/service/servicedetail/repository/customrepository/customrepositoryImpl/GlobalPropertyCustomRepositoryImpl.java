package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.GlobalPropertyCustomRepository;
import north.Overthink.model.submodel.GlobalProperty;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class GlobalPropertyCustomRepositoryImpl implements GlobalPropertyCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<GlobalProperty> getGlobalPropertyWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<GlobalProperty> typedQuery = entityManager
                .createQuery(query, GlobalProperty.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
