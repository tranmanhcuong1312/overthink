package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.PersonName;

import java.util.List;

public interface PersonNameCustomRepository {

    List<PersonName> getPersonNamesWithCondition(String query, int firstResult, int maxResult);
}
