package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.handler.utils.ConditionUtil;
import north.Overthink.api.service.servicedetail.repository.customrepository.InjectionCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class InjectionCustomRepositoryImpl implements InjectionCustomRepository {

    @Autowired
    private ConditionUtil conditionManualUtil;

    public ConditionUtil getConditionUtil() {
        return conditionManualUtil;
    }
}
