package north.Overthink.api.handler;

import java.io.IOException;
import java.util.List;

public interface JsonMapperHandler {

    String toString(Object object) throws IOException;

    <T> T toJson(String string, Class<T> clazz) throws  IOException;

    <T, K> T jsonToJson(K k, Class<T> clazz) throws IOException;

    <T> List<T> toListJson(String string, Class<T> clazz) throws IOException;
}
