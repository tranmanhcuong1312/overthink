package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.PersonAddress;

import java.util.List;

public interface PersonAddressCustomRepository {

    List<PersonAddress> getPersonAddressWithCondition(String query, int firstResult, int maxResult);
}
