package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.OfferCustomRepository;
import north.Overthink.model.submodel.Offer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class OfferCustomRepositoryImpl implements OfferCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Offer> getOffersWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Offer> typedQuery = entityManager
                .createQuery(query, Offer.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
