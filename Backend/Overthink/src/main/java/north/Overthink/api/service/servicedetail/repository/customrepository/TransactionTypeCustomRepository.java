package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.TransactionType;

import java.util.List;

public interface TransactionTypeCustomRepository {

    List<TransactionType> getTransactionTypesWithCondition(String query, int firstResult, int maxResult);
}
