package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Request;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Request.class)
public class RequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Request.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Request validator");
    }
}
