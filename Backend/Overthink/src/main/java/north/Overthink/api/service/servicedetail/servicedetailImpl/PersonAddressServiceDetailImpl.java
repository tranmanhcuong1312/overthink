package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PersonAddressRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.PersonAddress;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PersonAddressServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<PersonAddress, Integer> {

    @Autowired
    private PersonAddressRepository personAddressRepository;

    @Override
    public PersonAddress save(PersonAddress personAddress) {
        getStandardModelChangedHandler().createHandler(personAddress);
        return personAddressRepository.save(personAddress);
    }

    @Override
    public PersonAddress update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        PersonAddress personAddress = personAddressRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                PersonAddress.class, customObject, personAddress
        );
        return personAddressRepository.save(personAddress);
    }

    @Override
    public PersonAddress get(Integer integer) {
        return personAddressRepository.findById(integer).get();
    }

    @Override
    public Page<PersonAddress> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), PersonAddress.class);
        return personAddressRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public PersonAddress voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        PersonAddress personAddress = personAddressRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                PersonAddress.class, voidedStandardModel, personAddress
        );
        return personAddressRepository.save(personAddress);
    }
}
