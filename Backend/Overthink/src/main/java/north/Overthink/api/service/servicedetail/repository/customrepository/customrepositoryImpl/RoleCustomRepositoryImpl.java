package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.RoleCustomRepository;
import north.Overthink.model.submodel.Role;
import north.Overthink.model.templatemodel.Condition;
import north.Overthink.model.templatemodel.ConditionPaging;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class RoleCustomRepositoryImpl extends InjectionCustomRepositoryImpl implements RoleCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Role> getRolesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Role> typedQuery = entityManager
                .createQuery(query, Role.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
