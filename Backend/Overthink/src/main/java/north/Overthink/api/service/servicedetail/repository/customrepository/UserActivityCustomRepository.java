package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.UserActivity;

import java.util.List;

public interface UserActivityCustomRepository {

    List<UserActivity> getUserActivesWithCondition(String query, int firstResult, int maxResult);
}
