package north.Overthink.api.handler;

import north.Overthink.model.StandardModel;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.reflections.Reflections;

import java.beans.Introspector;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ClassReflectionHandler implements Serializable, ApplicationContextAware {

    @Autowired
    private FieldReflectionHandler fieldReflectionHandler;

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public Set<Class<? extends StandardModel>> getListModelClassHaveFieldReference(Class clazz){
        return getReflectionByPackage(clazz)
                .getSubTypesOf(StandardModel.class)
                .stream()
                .filter(c -> (!fieldReflectionHandler.getClassHaveClassField(c, clazz)))
                .collect(Collectors.toSet());
    }

    public Reflections getReflectionByPackage(Class clazz){
        return new Reflections(clazz.getClassLoader());
    }

    String getClassNameDecapitalize(Class clazz){
        String[] strings = clazz.getName().split("\\.");
        return Introspector.decapitalize(strings[strings.length - 1]);
    }
    
    String getClassName(Class clazz){
        String[] strings = clazz.getName().split("\\.");
        return strings[strings.length - 1];
    }
}
