package north.Overthink.api.handler;

import north.Overthink.model.templatemodel.UserInfoAuthentication;

import java.io.Serializable;

public class Context implements Serializable {

    private final static ThreadLocal<Object> THREAD_LOCAL = new ThreadLocal<>();

    public static void setUserAuthenticated(UserInfoAuthentication userInfoAuthentication){
//        if(userInfoAuthentication != null)
//            THREAD_LOCAL.set(userInfoAuthentication);
        UserContext.setUserInfoAuthentication(userInfoAuthentication);
    }

    public static UserInfoAuthentication getUserAuthenticated(){
//        return (UserInfoAuthentication) THREAD_LOCAL.get();
        return UserContext.getUserInfoAuthentication();
    }

    public static void clearThreadLocal(){
        THREAD_LOCAL.remove();
    }
}
