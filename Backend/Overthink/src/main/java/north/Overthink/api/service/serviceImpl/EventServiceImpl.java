package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Event;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EventServiceImpl extends InjectionServiceImpl implements StandardService<Event, Integer> {

    @Autowired
    private StandardServiceDetail<Event, Integer> standardServiceDetail;

    @Override
    public Event save(Event event) {
        return standardServiceDetail.save(event);
    }

    @Override
    public Event update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public Event get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<Event> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public Event voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
