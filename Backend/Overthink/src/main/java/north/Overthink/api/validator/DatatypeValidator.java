package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Datatype;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Datatype.class)
public class DatatypeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Datatype.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Datatype validator");
    }
}
