package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Order;

import java.util.List;

public interface OrderCustomRepository {

    List<Order> getOrdersWithCondition(String query, int firstResult, int maxResult);
}
