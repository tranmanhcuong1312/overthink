package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.PersonNameCustomRepository;
import north.Overthink.model.submodel.PersonName;
import north.Overthink.model.submodel.QPersonName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonNameRepository extends JpaRepository<PersonName, Integer>, PersonNameCustomRepository,
        QuerydslPredicateExecutor<PersonName>, QuerydslBinderCustomizer<QPersonName> {

    @Override
    default public void customize(QuerydslBindings bindings, QPersonName root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
