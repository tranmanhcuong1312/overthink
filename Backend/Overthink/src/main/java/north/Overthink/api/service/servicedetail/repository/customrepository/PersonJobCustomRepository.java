package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.PersonJob;

import java.util.List;

public interface PersonJobCustomRepository {

    List<PersonJob> getPersonJobsWithCondition(String query, int firstResult, int maxResult);
}
