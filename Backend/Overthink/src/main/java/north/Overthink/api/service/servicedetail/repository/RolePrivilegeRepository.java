package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.RolePrivilegeCustomRepository;
import north.Overthink.model.submodel.QRolePrivilege;
import north.Overthink.model.submodel.RolePrivilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePrivilegeRepository extends JpaRepository<RolePrivilege, Integer>, RolePrivilegeCustomRepository,
        QuerydslPredicateExecutor<RolePrivilege>, QuerydslBinderCustomizer<QRolePrivilege> {

    @Override
    default public void customize(QuerydslBindings bindings, QRolePrivilege root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
