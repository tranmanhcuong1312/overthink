package north.Overthink.api.handler;

import org.springframework.http.ResponseEntity;

public interface StandardResponseHandler {

    <T> ResponseEntity<T> response(T t);
}
