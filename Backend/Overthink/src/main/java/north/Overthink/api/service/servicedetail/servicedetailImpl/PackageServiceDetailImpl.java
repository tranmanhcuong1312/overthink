package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PackageRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Package;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PackageServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Package, Integer> {

    @Autowired
    private PackageRepository packageRepository;

    @Override
    public Package save(Package aPackage) {
        getStandardModelChangedHandler().createHandler(aPackage);
        return packageRepository.save(aPackage);
    }

    @Override
    public Package update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Package aPackage = packageRepository.findById((int) customObject.get("id")).get();
        Package newPackage = new Package(aPackage);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Package.class, customObject, aPackage, newPackage
        );
        newPackage = packageRepository.save(newPackage);
        packageRepository.save(aPackage);
        return newPackage;
    }

    @Override
    public Package get(Integer integer) {
        return packageRepository.findById(integer).get();
    }

    @Override
    public Page<Package> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Package.class);
        return packageRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Package voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        Package aPackage = packageRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Package.class, voidedStandardModel, aPackage
        );
        return packageRepository.save(aPackage);
    }
}
