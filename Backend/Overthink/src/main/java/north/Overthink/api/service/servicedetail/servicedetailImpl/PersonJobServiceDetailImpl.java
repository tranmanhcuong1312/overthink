package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PersonJobRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.PersonJob;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PersonJobServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<PersonJob, Integer> {

    @Autowired
    private PersonJobRepository personJobRepository;

    @Override
    public PersonJob save(PersonJob personJob) {
        getStandardModelChangedHandler().createHandler(personJob);
        return personJobRepository.save(personJob);
    }

    @Override
    public PersonJob update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        PersonJob personJob = personJobRepository
                        .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                PersonJob.class, customObject, personJob
        );
        return personJobRepository.save(personJob);
    }

    @Override
    public PersonJob get(Integer integer) {
        return personJobRepository.findById(integer).get();
    }

    @Override
    public Page<PersonJob> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), PersonJob.class);
        return personJobRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public PersonJob voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        PersonJob personJob = personJobRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                PersonJob.class, voidedStandardModel, personJob
        );
        return personJobRepository.save(personJob);
    }
}
