package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Role;
import north.Overthink.model.templatemodel.ConditionPaging;

import java.util.List;

public interface RoleCustomRepository {

    List<Role> getRolesWithCondition(String query, int firstResult, int maxResult);
}
