package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.TransactionTypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.TransactionType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class TransactionTypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<TransactionType, Integer> {

    @Autowired
    private TransactionTypeRepository transactionTypeRepository;

    @Override
    public TransactionType save(TransactionType transactionType) {
        getStandardModelChangedHandler().createHandler(transactionType);
        return transactionTypeRepository.save(transactionType);
    }

    @Override
    public TransactionType update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        TransactionType transactionType = transactionTypeRepository
                                .findById((int) customObject.get("id")).get();
        TransactionType newTransactionType = new TransactionType(transactionType);
        getStandardModelChangedHandler().updateWithVersionHandler(
                TransactionType.class, customObject, transactionType, newTransactionType
        );
        newTransactionType = transactionTypeRepository.save(newTransactionType);
        transactionTypeRepository.save(transactionType);
        return newTransactionType;
    }

    @Override
    public TransactionType get(Integer integer) {
        return transactionTypeRepository.findById(integer).get();
    }

    @Override
    public Page<TransactionType> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), TransactionType.class);
        return transactionTypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public TransactionType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        TransactionType transactionType = transactionTypeRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                TransactionType.class, voidedStandardModel, transactionType
        );
        return transactionTypeRepository.save(transactionType);
    }
}
