package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.ProductLevelCustomRepository;
import north.Overthink.model.submodel.ProductLevel;
import north.Overthink.model.submodel.QProductLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductLevelRepository extends JpaRepository<ProductLevel, Integer>, ProductLevelCustomRepository,
        QuerydslPredicateExecutor<ProductLevel>, QuerydslBinderCustomizer<QProductLevel> {

    @Override
    default public void customize(QuerydslBindings bindings, QProductLevel root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
