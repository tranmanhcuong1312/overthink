package north.Overthink.api.aspect;

import north.Overthink.api.handler.ServiceDetailContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ApplicationAspect {

    @Autowired
    private static ServiceDetailContext serviceDetailContext;

    @Before("execution(* north.Overthink.OverthinkApplication.*.*(..))")
    public void before(JoinPoint joinPoint){

    }
}
