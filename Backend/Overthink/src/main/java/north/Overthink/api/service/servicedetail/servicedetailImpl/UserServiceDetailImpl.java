package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.UserRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Role;
import north.Overthink.model.submodel.User;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@ServiceDetail
public class UserServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<User, Integer> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public User save(User user) {
        Role role = new Role();
        role.setId(3);
        user.setRole(role);
        user.setPassword(encoder.encode(user.getPassword()));
        user.setDateCreated(new Date());
        return userRepository.save(user);
    }

    @Override
    public User update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        User user = userRepository
                .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                User.class, customObject, user
        );
        return userRepository.save(user);
    }

    @Override
    public User get(Integer integer) {
        return userRepository.findById(integer).get();
    }

    @Override
    public Page<User> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), User.class);
        return userRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public User voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        User user = userRepository
                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                User.class, voidedStandardModel, user
        );
        return userRepository.save(user);
    }
}
