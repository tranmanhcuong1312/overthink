package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Package;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Package.class)
public class PackageValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Package.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Package validator");
    }
}
