package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Datatype;
import north.Overthink.model.submodel.Role;

import java.util.List;

public interface DatatypeCustomRepository {

    List<Datatype> getDatatypeWithCondition(String query, int firstResult, int maxResult);
}
