package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.handler.Context;
import north.Overthink.api.handler.utils.JwtTokenUtil;
import north.Overthink.api.service.customservice.AuthenticationService;
import north.Overthink.api.service.servicedetail.repository.UserRepository;
import north.Overthink.model.submodel.User;
import north.Overthink.model.templatemodel.AuthenticationRequest;
import north.Overthink.model.templatemodel.UserInfoAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailServiceImpl implements UserDetailsService, AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = null;
        if(username.contains(",")) {
            String[] userString = username.split(",");
            try{
                user = userRepository.findByUsernameEquals(userString[0]);
                if(!encoder.matches(userString[1], user.getPassword())){
                    user = null;
                }
                if(user == null){
                    throw new UsernameNotFoundException("User not found");
                }
            } catch (Exception e){
                throw new UsernameNotFoundException("user not found");
            }
        } else {
            try{
                user = userRepository.findByUsernameEquals(username);
                if(user == null){
                    throw new UsernameNotFoundException("User not found");
                }
            } catch (Exception e){
                throw new UsernameNotFoundException("user not found");
            }
        }

        return new org.springframework.security
                .core.userdetails.User
                (user.getUsername(), user.getPassword(), new ArrayList<>());
    }

    @Override
    public UserInfoAuthentication createAuthentication(AuthenticationRequest authenticationRequest) throws Exception {
        authentication(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = loadUserByUsername(
                authenticationRequest.getUsername() + "," +
                authenticationRequest.getPassword());
        final String token = jwtTokenUtil.generateToken(userDetails);
        User user = userRepository.findByUsernameEquals(userDetails.getUsername());

        UserInfoAuthentication userInfoAuthentication
                        = new UserInfoAuthentication(user, token);
        try {
            Context.setUserAuthenticated(userInfoAuthentication);
        }catch (NullPointerException e){

        }

        return userInfoAuthentication;
    }

    @Override
    public Boolean logout() {
        Context.clearThreadLocal();
        return true;
    }

    public void authentication(String username,
                               String password) throws Exception{
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username + "," + password,
                            password
                    ));
        }catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
