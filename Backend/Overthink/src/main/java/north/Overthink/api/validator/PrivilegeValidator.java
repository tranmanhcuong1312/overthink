package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Privilege;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Privilege.class)
public class PrivilegeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Privilege.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Privilege validator");
    }
}
