package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.PersonAddress;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = PersonAddress.class)
public class PersonAddressValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PersonAddress.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Person address validator");
    }
}
