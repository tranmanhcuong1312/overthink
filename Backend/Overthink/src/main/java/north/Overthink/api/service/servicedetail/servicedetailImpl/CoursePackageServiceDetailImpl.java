package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.CoursePackageRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.CoursePackage;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class CoursePackageServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<CoursePackage, Integer> {

    @Autowired
    private CoursePackageRepository coursePackageRepository;

    @Override
    public CoursePackage save(CoursePackage coursePackage) {
        getStandardModelChangedHandler().createHandler(coursePackage);
        return coursePackageRepository.save(coursePackage);
    }

    @Override
    public CoursePackage update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        CoursePackage coursePackage = coursePackageRepository
                        .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                CoursePackage.class, customObject, coursePackage
        );
        return coursePackageRepository.save(coursePackage);
    }

    @Override
    public CoursePackage get(Integer integer) {
        return coursePackageRepository.findById(integer).get();
    }

    @Override
    public Page<CoursePackage> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), CoursePackage.class);
        return coursePackageRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public CoursePackage voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        CoursePackage coursePackage = coursePackageRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                CoursePackage.class, voidedStandardModel, coursePackage
        );
        return coursePackageRepository.save(coursePackage);
    }
}
