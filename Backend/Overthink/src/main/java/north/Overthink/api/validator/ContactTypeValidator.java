package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.ContactType;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = ContactType.class)
public class ContactTypeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return ContactType.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Contact type validator");
    }
}
