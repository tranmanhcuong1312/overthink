package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Transaction;

import java.util.List;

public interface TransactionCustomRepository {

    List<Transaction> getTransactionsWithCondition(String query, int firstResult, int maxResult);
}
