package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.User;

import java.util.List;

public interface UserCustomRepository {

    List<User> getUsersWithCondition(String query, int firstResult, int maxResult);
}
