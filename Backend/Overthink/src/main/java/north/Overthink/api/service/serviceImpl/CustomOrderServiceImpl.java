package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.handler.Context;
import north.Overthink.api.service.customservice.CustomOrderService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.servicedetailImpl.*;
import north.Overthink.model.submodel.*;
import north.Overthink.model.templatemodel.*;
import org.springframework.stereotype.Service;

@Service
public class CustomOrderServiceImpl extends InjectionServiceImpl implements CustomOrderService {

    @Override
    public Order orderCourse(CustomObject customObject) throws NoSuchFieldException {
        Course course = (Course) getCourseServiceDetail().get((Integer) customObject.get("id"));
        Order order = new Order();
        order.setBuyer(Context.getUserAuthenticated().getUser());
        order.setSeller(course.getCreator());
        Transaction transaction = new Transaction();
        transaction.setTransactionType((TransactionType) getTransactionTypeServiceDetail().get(1));
        transaction.setValue(course.getPrice());
        transaction.setContent(course.getId() + course.getName());
        transaction.setState(State.WAITING);
        order.setTransaction((Transaction) getTransactionServiceDetail().save(transaction));
        Order order1 = (Order) getOrderServiceDetail().save(order);
        return order1;
    }

    @Override
    public Order orderOffer(CustomObject customObject) throws NoSuchFieldException {
        Offer offer = (Offer) getOfferServiceDetail().get((Integer) customObject.get("id"));
        Order order = new Order();
        order.setBuyer(Context.getUserAuthenticated().getUser());
        order.setSeller(offer.getCreator());
        Transaction transaction = new Transaction();
        transaction.setTransactionType((TransactionType) getTransactionTypeServiceDetail().get(1));
        transaction.setValue(offer.getPrice());
        transaction.setContent(offer.getId() + offer.getName());
        transaction.setState(State.WAITING);
        order.setTransaction((Transaction) getTransactionServiceDetail().save(transaction));
        Order order1 = (Order) getOrderServiceDetail().save(order);
        return order1;
    }

    private StandardServiceDetail getCourseServiceDetail(){
        return getStandardServiceDetailMap().get(CourseServiceDetailImpl.class);
    }

    private StandardServiceDetail getOfferServiceDetail(){
        return getStandardServiceDetailMap().get(OfferServiceDetailImpl.class);
    }

    private StandardServiceDetail getTransactionTypeServiceDetail(){
        return getStandardServiceDetailMap().get(TransactionTypeServiceDetailImpl.class);
    }

    private StandardServiceDetail getOrderServiceDetail(){
        return getStandardServiceDetailMap().get(OrderServiceDetailImpl.class);
    }

    private StandardServiceDetail getTransactionServiceDetail(){
        return getStandardServiceDetailMap().get(TransactionServiceDetailImpl.class);
    }
}
