package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.Privilege;

import java.util.List;

public interface PrivilegeCustomRepository {

    List<Privilege> getPrivilegesWithCondition(String query, int firstResult, int maxResult);
}
