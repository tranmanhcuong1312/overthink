package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Task;

import java.util.List;

public interface TaskCustomRepository {

    List<Task> getTaskTypesWithCondition(String query, int firstResult, int maxResult);
}
