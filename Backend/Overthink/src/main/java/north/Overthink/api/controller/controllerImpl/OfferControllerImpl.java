package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Offer;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/offer")
public class OfferControllerImpl implements StandardController<Offer, Integer> {

    @Autowired
    private StandardService<Offer, Integer> standardService;

    @Override
    public Offer save(Offer offer) {
        return standardService.save(offer);
    }

    @Override
    public Offer save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public Offer get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<Offer> get(ConditionPaging conditionPaging) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPaging);
    }

    @Override
    public Offer voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
