package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Request;

import java.util.List;

public interface RequestCustomRepository {

    List<Request> getRequestsWithCondition(String query, int firstResult, int maxResult);
}
