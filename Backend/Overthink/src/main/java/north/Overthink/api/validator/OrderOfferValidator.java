package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.OrderOffer;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = OrderOffer.class)
public class OrderOfferValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderOffer.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Order offer validator");
    }
}
