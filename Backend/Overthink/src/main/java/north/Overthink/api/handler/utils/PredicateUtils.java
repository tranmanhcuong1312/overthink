package north.Overthink.api.handler.utils;

import com.querydsl.core.types.dsl.*;
import north.Overthink.model.StandardModel;
import north.Overthink.model.templatemodel.Condition;
import north.Overthink.model.templatemodel.ConditionPaging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Component
public class PredicateUtils<T extends StandardModel> implements Serializable {

    @Autowired
    private DataDeclaredUtils dataDeclaredUtils;

    public BooleanExpression getPredicates(List<Condition> conditions, Class<T> c) throws NoSuchFieldException {
        BooleanExpression booleanExpression = Expressions.asBoolean(true).isTrue();
        for(Condition condition: conditions){
            booleanExpression = booleanExpression.and(getPredicate(condition, c));
        }
        return booleanExpression;
    }

    public BooleanExpression getPredicate(Condition condition, Class<T> c) throws NoSuchFieldException {
        PathBuilder<T> pathBuilder = new PathBuilder(c.getClass(),
                dataDeclaredUtils.getModelNameDecapitalize(c));
        if(isNumber(condition.getValue())){
            NumberPath numberPath =
                    pathBuilder.getNumber(
                            condition.getField(),
                            dataDeclaredUtils.getTypeOfField(c, condition.getField())
                    );
            return buildNumberPathBuilder(numberPath, condition);
        } else if(isWords(condition.getValue())){
            StringPath stringPath =
                    pathBuilder.getString(condition.getField());
            return buildStringBuildPath(stringPath, condition);
        } else if(isEnum(condition.getValue())){
            StringPath stringPath =
                    pathBuilder.getString(condition.getField());
            return buildOtherBuildPath(stringPath, condition);
        } else if(isBoolean(condition.getValue())){
            StringPath stringPath =
                    pathBuilder.getString(condition.getField());
            return buildOtherBuildPath(stringPath, condition);
        }
        return null;
    }

    public boolean isNumber(Object object){
        if(object instanceof Number)
            return true;
        return false;
    }

    public boolean isWords(Object object){
        if(object instanceof String)
            return true;
        return false;
    }

    public boolean isEnum(Object object){
        if(object instanceof Enum)
            return true;
        return false;
    }

    public boolean isBoolean(Object object){
        if(object instanceof Boolean)
            return true;
        return false;
    }

    public BooleanExpression buildNumberPathBuilder(NumberPath numberPath, Condition condition){
        switch (condition.getConditionKey()){
            case EQUAL:
                return numberPath.eq(condition.getValue());
            case GREATER:
                return numberPath.gt((Number) condition.getValue());
            case SMALLER:
                return numberPath.lt((Number) condition.getValue());
            case GREATER_AND_EQUAL:
                return numberPath.goe((Number) condition.getValue());
            case SMALLER_AND_EQUAL:
                return numberPath.loe((Number) condition.getValue());
            default:
                return null;
        }
    }

    public BooleanExpression buildStringBuildPath(StringPath stringPath, Condition condition){
        switch (condition.getConditionKey()){
            case EQUAL:
                return stringPath.eq((String) condition.getValue());
            case CONTAIN:
                return stringPath.contains((String) condition.getValue());
            default:
                return null;
        }
    }

    public BooleanExpression buildOtherBuildPath(StringPath stringPath, Condition condition){
        switch (condition.getConditionKey()){
            case EQUAL:
                return stringPath.eq(String.valueOf(condition.getValue()));
            default:
                return null;
        }
    }
}
