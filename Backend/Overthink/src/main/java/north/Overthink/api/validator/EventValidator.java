package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Event;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = Event.class)
public class EventValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Event.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Event validator");
        Event event = (Event) target;
        if(event.getStartDate().after(event.getEndDate()) ||
            event.getStartDate().equals(event.getEndDate())){
            errors.rejectValue("startDate", null, "Start date cannot be equal or after end date");
        }
    }
}
