package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.UserContact;

import java.util.List;

public interface UserContactCustomRepository {

    List<UserContact> getUserContactsWithCondition(String query, int firstResult, int maxResult);
}
