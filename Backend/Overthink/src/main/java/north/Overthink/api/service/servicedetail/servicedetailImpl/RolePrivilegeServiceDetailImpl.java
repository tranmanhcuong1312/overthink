package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.RolePrivilegeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.RolePrivilege;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class RolePrivilegeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<RolePrivilege, Integer> {

    @Autowired
    private RolePrivilegeRepository rolePrivilegeRepository;

    @Override
    public RolePrivilege save(RolePrivilege rolePrivilege) {
        getStandardModelChangedHandler().createHandler(rolePrivilege);
        return rolePrivilegeRepository.save(rolePrivilege);
    }

    @Override
    public RolePrivilege update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        RolePrivilege rolePrivilege = rolePrivilegeRepository
                                .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                RolePrivilege.class, customObject, rolePrivilege
        );
        return rolePrivilegeRepository.save(rolePrivilege);
    }

    @Override
    public RolePrivilege get(Integer integer) {
        return rolePrivilegeRepository.findById(integer).get();
    }

    @Override
    public Page<RolePrivilege> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), RolePrivilege.class);
        return rolePrivilegeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public RolePrivilege voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        RolePrivilege rolePrivilege = rolePrivilegeRepository
                                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                RolePrivilege.class, voidedStandardModel, rolePrivilege
        );
        return rolePrivilegeRepository.save(rolePrivilege);
    }
}
