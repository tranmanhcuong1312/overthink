package north.Overthink.api.service.customservice;

import north.Overthink.model.templatemodel.AuthenticationRequest;
import north.Overthink.model.templatemodel.UserInfoAuthentication;

public interface AuthenticationService {

    UserInfoAuthentication createAuthentication(AuthenticationRequest authenticationRequest) throws Exception;

    Boolean logout();
}
