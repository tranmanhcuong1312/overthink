package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.OfferPackage;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class OfferPackageServiceImpl extends InjectionServiceImpl implements StandardService<OfferPackage, Integer> {

    @Autowired
    private StandardServiceDetail<OfferPackage, Integer> standardServiceDetail;

    @Override
    public OfferPackage save(OfferPackage offerPackage) {
        return standardServiceDetail.save(offerPackage);
    }

    @Override
    public OfferPackage update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public OfferPackage get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<OfferPackage> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public OfferPackage voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
