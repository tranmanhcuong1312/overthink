package north.Overthink.api.handler.utils;

import north.Overthink.model.StandardModel;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.beans.Introspector;
import java.io.Serializable;
import java.lang.reflect.Field;

@Component
public class DataDeclaredUtils<T extends StandardModel> implements Serializable {

    String getColumnNameOfField(Field field){
        if(field.isAnnotationPresent(Column.class)){
            Column column = field.getAnnotation(Column.class);
            return column.name();
        }else if(field.isAnnotationPresent(JoinColumn.class)){
            JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
            return joinColumn.name();
        }
        return null;
    }

    String getTableNameOfModel(Class<T> clazz){
        if(clazz.isAnnotationPresent(Table.class)){
            Table table = clazz.getAnnotation(Table.class);
            return table.name();
        }
        return null;
    }

    String getModelNameDecapitalize(Class<T> clazz){
        String[] strings = clazz.getName().split("\\.");
        return Introspector.decapitalize(strings[strings.length - 1]);
    }

    <T extends StandardModel> Class<?> getTypeOfId(Class<T> clazz) throws NoSuchFieldException {
        try{
            return clazz.getDeclaredField("id").getType();
        }catch (NoSuchFieldException e){
            return clazz.getSuperclass().getDeclaredField("id").getType();
        }
    }

    <T extends StandardModel> Class<?> getTypeOfField(Class<T> clazz, String fieldName) throws NoSuchFieldException{
        try{
            return clazz.getDeclaredField(fieldName).getType();
        } catch (NoSuchFieldException e) {
            return clazz.getSuperclass().getDeclaredField(fieldName).getType();
        }
    }
}
