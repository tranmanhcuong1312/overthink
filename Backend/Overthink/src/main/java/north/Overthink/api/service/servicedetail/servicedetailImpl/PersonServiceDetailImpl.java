package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PersonRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Person;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PersonServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Person, Integer> {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person save(Person person) {
        getStandardModelChangedHandler().createHandler(person);
        return personRepository.save(person);
    }

    @Override
    public Person update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Person person = personRepository
                    .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Person.class, customObject, person
        );
        return personRepository.save(person);
    }

    @Override
    public Person get(Integer integer) {
        return personRepository.findById(integer).get();
    }

    @Override
    public Page<Person> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Person.class);
        return personRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Person voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Person person = personRepository
                    .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Person.class, voidedStandardModel, person
        );
        return personRepository.save(person);
    }
}
