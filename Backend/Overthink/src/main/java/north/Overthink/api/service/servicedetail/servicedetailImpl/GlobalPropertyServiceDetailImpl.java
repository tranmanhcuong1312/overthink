package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.GlobalPropertyRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.GlobalProperty;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class GlobalPropertyServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<GlobalProperty, Integer> {

    @Autowired
    private GlobalPropertyRepository globalPropertyRepository;

    @Override
    public GlobalProperty save(GlobalProperty globalProperty) {
        getStandardModelChangedHandler().createHandler(globalProperty);
        return globalPropertyRepository.save(globalProperty);
    }

    @Override
    public GlobalProperty update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        GlobalProperty globalProperty = globalPropertyRepository.findById((int) customObject.get("id")).get();
        GlobalProperty newGlobalProperty = new GlobalProperty(globalProperty);
        getStandardModelChangedHandler().updateWithVersionHandler(
                GlobalProperty.class, customObject, globalProperty, newGlobalProperty
        );
        newGlobalProperty = globalPropertyRepository.save(newGlobalProperty);
        globalPropertyRepository.save(globalProperty);
        return newGlobalProperty;
    }

    @Override
    public GlobalProperty get(Integer integer) {
        return globalPropertyRepository.findById(integer).get();
    }

    @Override
    public Page<GlobalProperty> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), GlobalProperty.class);
        return globalPropertyRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public GlobalProperty voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        GlobalProperty globalProperty = globalPropertyRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                GlobalProperty.class, voidedStandardModel, globalProperty
        );
        return globalPropertyRepository.save(globalProperty);
    }
}
