package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.ActivityTypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ActivityType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;

@ServiceDetail
public class ActivityTypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<ActivityType, Integer> {

    @Autowired
    private ActivityTypeRepository activityTypeRepository;

    @Override
    public ActivityType save(ActivityType activityType) {
        getStandardModelChangedHandler().createHandler(activityType);
        return activityTypeRepository.save(activityType);
    }

    @Override
    public ActivityType update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        ActivityType activityType = activityTypeRepository
                .findById((int) customObject.get("id")).get();
        ActivityType newActivityType = new ActivityType(activityType);
        getStandardModelChangedHandler().updateWithVersionHandler(
                ActivityType.class, customObject, activityType, newActivityType
        );
        newActivityType = activityTypeRepository.save(newActivityType);
        activityTypeRepository.save(activityType);
        return newActivityType;
    }

    @Override
    public ActivityType get(Integer integer) {
        return activityTypeRepository.findById(integer).get();
    }

    @Override
    public Page<ActivityType> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), ActivityType.class);
        return activityTypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest())
        );
    }

    @Override
    public ActivityType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        ActivityType activityType = activityTypeRepository
                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                ActivityType.class, voidedStandardModel, activityType
        );
        return activityTypeRepository.save(activityType);
    }
}
