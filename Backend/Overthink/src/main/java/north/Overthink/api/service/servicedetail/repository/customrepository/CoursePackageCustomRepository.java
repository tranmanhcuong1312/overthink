package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.CoursePackage;

import java.util.List;

public interface CoursePackageCustomRepository {

    List<CoursePackage> getCoursePackagesWithCondition(String query, int firstResult, int maxResult);
}
