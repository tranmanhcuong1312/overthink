package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.TaskRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Task;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class TaskServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Task, Integer> {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task save(Task task) {
        getStandardModelChangedHandler().createHandler(task);
        return taskRepository.save(task);
    }

    @Override
    public Task update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Task task = taskRepository
                .findById((int) customObject.get("id")).get();
        Task newTask = new Task(task);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Task.class, customObject, task, newTask
        );
        newTask = taskRepository.save(newTask);
        taskRepository.save(task);
        return newTask;
    }

    @Override
    public Task get(Integer integer) {
        return taskRepository.findById(integer).get();
    }

    @Override
    public Page<Task> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Task.class);
        return taskRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Task voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Task task = taskRepository
                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Task.class, voidedStandardModel, task
        );
        return taskRepository.save(task);
    }
}
