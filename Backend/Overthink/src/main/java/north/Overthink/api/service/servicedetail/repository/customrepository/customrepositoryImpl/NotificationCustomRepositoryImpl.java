package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.NotificationCustomRepository;
import north.Overthink.model.submodel.Notification;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class NotificationCustomRepositoryImpl implements NotificationCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Notification> getNotificationsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Notification> typedQuery = entityManager
                .createQuery(query, Notification.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
