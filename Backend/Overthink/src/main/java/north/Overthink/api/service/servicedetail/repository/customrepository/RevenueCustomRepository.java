package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Revenue;

import java.util.List;

public interface RevenueCustomRepository {

    List<Revenue> getRevenuesWithCondition(String query, int firstResult, int maxResult);
}
