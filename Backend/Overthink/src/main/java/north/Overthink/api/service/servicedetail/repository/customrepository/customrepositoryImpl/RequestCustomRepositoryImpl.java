package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.RequestCustomRepository;
import north.Overthink.model.submodel.Request;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class RequestCustomRepositoryImpl implements RequestCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Request> getRequestsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Request> typedQuery = entityManager
                .createQuery(query, Request.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
