package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.OrderCourse;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = OrderCourse.class)
public class OrderCourseValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderCourse.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Order course validator");
    }
}
