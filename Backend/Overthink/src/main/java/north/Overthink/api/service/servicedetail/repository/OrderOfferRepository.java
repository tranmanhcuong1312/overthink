package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.OrderOfferCustomRepository;
import north.Overthink.model.submodel.OrderOffer;
import north.Overthink.model.submodel.QOrderOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderOfferRepository extends JpaRepository<OrderOffer, Integer>, OrderOfferCustomRepository,
        QuerydslPredicateExecutor<OrderOffer>, QuerydslBinderCustomizer<QOrderOffer> {

    @Override
    default public void customize(QuerydslBindings bindings, QOrderOffer root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
