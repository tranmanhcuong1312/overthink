package north.Overthink.api.service.servicedetail.servicedetailImpl;

import north.Overthink.api.handler.FieldReflectionHandler;
import north.Overthink.api.handler.StandardModelChanged;
import north.Overthink.api.handler.StandardModelChangedHandler;
import north.Overthink.api.handler.utils.ConditionUtil;
import north.Overthink.api.handler.utils.PredicateUtils;
import north.Overthink.api.service.servicedetail.customservicedetail.InjectionServiceDetail;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class InjectionServiceDetailImpl implements InjectionServiceDetail {

    @Autowired
    private FieldReflectionHandler fieldReflectionHandler;

    @Autowired
    private StandardModelChanged standardModelChanged;

    @Autowired
    private StandardModelChangedHandler standardModelChangedHandler;

    @Autowired
    private ConditionUtil conditionUtil;

    public ConditionUtil getConditionUtil() {
        return conditionUtil;
    }

    public FieldReflectionHandler getFieldReflectionHandler() {
        return fieldReflectionHandler;
    }

    public StandardModelChanged getStandardModelChanged() {
        return standardModelChanged;
    }

    public StandardModelChangedHandler getStandardModelChangedHandler() {
        return standardModelChangedHandler;
    }

}
