package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.TaskTypeCustomRepository;
import north.Overthink.model.submodel.TaskType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class TaskTypeCustomRepositoryImpl implements TaskTypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<TaskType> getTaskTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<TaskType> typedQuery = entityManager
                .createQuery(query, TaskType.class);
        if(firstResult != 0)
            typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
            typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
