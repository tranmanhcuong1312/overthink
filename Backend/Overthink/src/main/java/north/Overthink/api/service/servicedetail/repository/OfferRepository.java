package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.OfferCustomRepository;
import north.Overthink.model.submodel.Offer;
import north.Overthink.model.submodel.QOffer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Integer>, OfferCustomRepository,
        QuerydslPredicateExecutor<Offer>, QuerydslBinderCustomizer<QOffer> {

    @Override
    default public void customize(QuerydslBindings bindings, QOffer root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
