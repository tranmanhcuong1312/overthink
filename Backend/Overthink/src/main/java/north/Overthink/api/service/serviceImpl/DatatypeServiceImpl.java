package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Datatype;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DatatypeServiceImpl extends InjectionServiceImpl implements StandardService<Datatype, Integer> {

    @Autowired
    private StandardServiceDetail<Datatype, Integer> standardServiceDetail;

    @Override
    public Datatype save(Datatype datatype) {
        return standardServiceDetail.save(datatype);
    }

    @Override
    public Datatype update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public Datatype get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<Datatype> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public Datatype voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
