package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.PersonName;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = PersonName.class)
public class PersonNameValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PersonName.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Person name validator");
    }
}
