package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Job;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/job")
public class JobControllerImpl implements StandardController<Job, Integer> {

    @Autowired
    private StandardService<Job, Integer> standardService;

    @Override
    public Job save(Job job) {
        return standardService.save(job);
    }

    @Override
    public Job save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public Job get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<Job> get(ConditionPaging conditionPaging) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPaging);
    }

    @Override
    public Job voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
