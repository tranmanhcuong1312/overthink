package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.PersonContact;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = PersonContact.class)
public class PersonContactValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PersonContact.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Person contact validator");
    }
}
