package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ProductLevel;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ProductLevelServiceImpl extends InjectionServiceImpl implements StandardService<ProductLevel, Integer> {

    @Autowired
    private StandardServiceDetail<ProductLevel, Integer> standardServiceDetail;

    @Override
    public ProductLevel save(ProductLevel productLevel) {
        return standardServiceDetail.save(productLevel);
    }

    @Override
    public ProductLevel update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public ProductLevel get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<ProductLevel> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public ProductLevel voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
