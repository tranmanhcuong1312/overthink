package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PersonJobCustomRepository;
import north.Overthink.model.submodel.PersonJob;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonJobCustomRepositoryImpl implements PersonJobCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PersonJob> getPersonJobsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<PersonJob> typedQuery = entityManager
                .createQuery(query, PersonJob.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
