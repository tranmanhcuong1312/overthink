package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.UserContactCustomRepository;
import north.Overthink.model.submodel.UserContact;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserContactCustomRepositoryImpl implements UserContactCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserContact> getUserContactsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<UserContact> typedQuery = entityManager
                .createQuery(query, UserContact.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
