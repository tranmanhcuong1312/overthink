package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.TaskTypeCustomRepository;
import north.Overthink.model.submodel.QTaskType;
import north.Overthink.model.submodel.TaskType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskTypeRepository extends JpaRepository<TaskType, Integer>, TaskTypeCustomRepository,
        QuerydslPredicateExecutor<TaskType>, QuerydslBinderCustomizer<QTaskType> {

    @Override
    default public void customize(QuerydslBindings bindings, QTaskType root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
