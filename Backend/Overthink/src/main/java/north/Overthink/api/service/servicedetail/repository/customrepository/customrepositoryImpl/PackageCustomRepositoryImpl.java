package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PackageCustomRepository;
import north.Overthink.model.submodel.Package;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PackageCustomRepositoryImpl implements PackageCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Package> getPackagesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Package> typedQuery = entityManager
                .createQuery(query, Package.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
