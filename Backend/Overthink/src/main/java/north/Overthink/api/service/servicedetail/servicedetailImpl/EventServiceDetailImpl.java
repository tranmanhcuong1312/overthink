package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.EventRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Event;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class EventServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Event, Integer> {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public Event save(Event event) {
        getStandardModelChangedHandler().createHandler(event);
        return eventRepository.save(event);
    }

    @Override
    public Event update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Event event = eventRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Event.class, customObject, event
        );
        return eventRepository.save(event);
    }

    @Override
    public Event get(Integer integer) {
        return eventRepository.findById(integer).get();
    }

    @Override
    public Page<Event> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Event.class);
        return eventRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Event voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Event event = eventRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Event.class, voidedStandardModel, event
        );
        return eventRepository.save(event);
    }
}
