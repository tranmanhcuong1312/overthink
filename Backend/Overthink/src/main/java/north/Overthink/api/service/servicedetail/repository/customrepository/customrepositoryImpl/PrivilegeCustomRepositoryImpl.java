package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PrivilegeCustomRepository;
import north.Overthink.model.submodel.Privilege;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PrivilegeCustomRepositoryImpl implements PrivilegeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Privilege> getPrivilegesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Privilege> typedQuery = entityManager
                .createQuery(query, Privilege.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
