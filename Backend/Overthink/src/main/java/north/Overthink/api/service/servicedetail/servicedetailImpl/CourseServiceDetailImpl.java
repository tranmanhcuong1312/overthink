package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.CourseRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Course;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class CourseServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Course, Integer> {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public Course save(Course course) {
        getStandardModelChangedHandler().createHandler(course);
        return courseRepository.save(course);
    }

    @Override
    public Course update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Course course = courseRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Course.class, customObject, course
        );
        return courseRepository.save(course);
    }

    @Override
    public Course get(Integer integer) {
        return courseRepository.findById(integer).get();
    }

    @Override
    public Page<Course> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Course.class);
        return courseRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Course voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Course course = courseRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Course.class, voidedStandardModel, course
        );
        return courseRepository.save(course);
    }
}
