package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.ActivityType;

import java.util.List;

public interface ActivityTypeCustomRepository {

    List<ActivityType> getActiveTypesWithCondition(String query, int firstResult, int maxResult);
}
