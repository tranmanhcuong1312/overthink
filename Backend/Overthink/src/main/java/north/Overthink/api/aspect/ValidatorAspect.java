package north.Overthink.api.aspect;

import north.Overthink.api.handler.utils.ValidatorUtil;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.CustomObject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ValidatorAspect {

    @Autowired
    private ValidatorUtil validatorUtil;

    @Before("execution(* north.Overthink.api.service.servicedetail.StandardServiceDetail.save(..))")
    public void before(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        validatorUtil.validator(args[0], null);
    }

    @Before("execution(* north.Overthink.api.service.servicedetail.StandardServiceDetail.update(..))")
    public void beforeUpdate(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        CustomObject customObject = (CustomObject) args[0];
        if(customObject.get("id") == null){
            throw new NoIdForUpdateException().noIdFound();
        }
    }

    @Before("execution(* north.Overthink.api.service.servicedetail.StandardServiceDetail.voided(..))")
    public void beforeVoided(JoinPoint joinPoint){
        VoidedStandardModel voidedStandardModel = (VoidedStandardModel) joinPoint.getArgs()[0];
        if(voidedStandardModel.getId() == 0){
            throw new NoIdForUpdateException().noIdFound();
        }
    }
}
