package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.GlobalProperty;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class GlobalPropertyServiceImpl extends InjectionServiceImpl implements StandardService<GlobalProperty, Integer> {

    @Autowired
    private StandardServiceDetail<GlobalProperty, Integer> standardServiceDetail;

    @Override
    public GlobalProperty save(GlobalProperty globalProperty) {
        return standardServiceDetail.save(globalProperty);
    }

    @Override
    public GlobalProperty update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public GlobalProperty get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<GlobalProperty> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public GlobalProperty voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
