package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.ActivityTypeCustomRepository;
import north.Overthink.model.submodel.ActivityType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ActivityTypeCustomRepositoryImpl implements ActivityTypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ActivityType> getActiveTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<ActivityType> typedQuery = entityManager
                .createQuery(query, ActivityType.class);
        if(firstResult != 0)
            typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
            typedQuery.setMaxResults(maxResult);

//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<ActiveType> cq = cb.createQuery(ActiveType.class);
//        Root<ActiveType> root = cq.from(ActiveType.class);

        return typedQuery.getResultList();
    }
}
