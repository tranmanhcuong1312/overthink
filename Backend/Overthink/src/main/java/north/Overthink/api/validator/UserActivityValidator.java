package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.UserActivity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = UserActivity.class)
public class UserActivityValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserActivity.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("User active validator");
    }
}
