package north.Overthink.api.handler;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;

@Component
public class CustomPermission implements PermissionEvaluator {

    public CustomPermission(){

    }

    public boolean hasPermission(String permission){
        return false;
    }

    public boolean hasPermission(Object target, String permission){
        return false;
    }

    public boolean hasPermission(Authentication authentication, String permission){
        return false;
    }

    public boolean hasPermission(Collection<Object> collection, String permission){
        return false;
    }

    public boolean hasPermission(Authentication authentication, Object permission) {
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }
}
