package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PersonNameRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.PersonName;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PersonNameServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<PersonName, Integer> {

    @Autowired
    private PersonNameRepository personNameRepository;

    @Override
    public PersonName save(PersonName personName) {
        getStandardModelChangedHandler().createHandler(personName);
        return personNameRepository.save(personName);
    }

    @Override
    public PersonName update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        PersonName personName = personNameRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                PersonName.class, customObject, personName
        );
        return personNameRepository.save(personName);
    }

    @Override
    public PersonName get(Integer integer) {
        return personNameRepository.findById(integer).get();
    }

    @Override
    public Page<PersonName> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), PersonName.class);
        return personNameRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public PersonName voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        PersonName personName = personNameRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                PersonName.class, voidedStandardModel, personName
        );
        return personNameRepository.save(personName);
    }
}
