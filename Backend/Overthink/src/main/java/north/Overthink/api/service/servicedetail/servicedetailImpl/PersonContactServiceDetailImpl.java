package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PersonContactRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.PersonContact;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PersonContactServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<PersonContact, Integer> {

    @Autowired
    private PersonContactRepository personContactRepository;

    @Override
    public PersonContact save(PersonContact personContact) {
        getStandardModelChangedHandler().createHandler(personContact);
        return personContactRepository.save(personContact);
    }

    @Override
    public PersonContact update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        PersonContact personContact = personContactRepository
                                .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                PersonContact.class, customObject, personContact
        );
        return personContactRepository.save(personContact);
    }

    @Override
    public PersonContact get(Integer integer) {
        return personContactRepository.findById(integer).get();
    }

    @Override
    public Page<PersonContact> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), PersonContact.class);
        return personContactRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public PersonContact voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        PersonContact personContact = personContactRepository
                                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                PersonContact.class, voidedStandardModel, personContact
        );
        return personContactRepository.save(personContact);
    }
}
