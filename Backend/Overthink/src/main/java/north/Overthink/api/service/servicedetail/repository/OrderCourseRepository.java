package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.OrderCourseCustomRepository;
import north.Overthink.model.submodel.OrderCourse;
import north.Overthink.model.submodel.QOrderCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderCourseRepository extends JpaRepository<OrderCourse, Integer>, OrderCourseCustomRepository,
        QuerydslPredicateExecutor<OrderCourse>, QuerydslBinderCustomizer<QOrderCourse> {

    @Override
    default public void customize(QuerydslBindings bindings, QOrderCourse root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
