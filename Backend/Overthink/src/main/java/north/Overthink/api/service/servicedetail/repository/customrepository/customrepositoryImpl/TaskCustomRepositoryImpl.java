package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.TaskCustomRepository;
import north.Overthink.model.submodel.Task;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class TaskCustomRepositoryImpl implements TaskCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Task> getTaskTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Task> typedQuery = entityManager
                .createQuery(query, Task.class);
        if(firstResult != 0)
            typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
            typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
