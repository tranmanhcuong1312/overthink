package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.User;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserServiceImpl extends InjectionServiceImpl implements StandardService<User, Integer> {

    @Autowired
    private StandardServiceDetail<User, Integer> standardServiceDetail;

    @Override
    public User save(User user) {
        return standardServiceDetail.save(user);
    }

    @Override
    public User update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public User get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<User> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public User voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
