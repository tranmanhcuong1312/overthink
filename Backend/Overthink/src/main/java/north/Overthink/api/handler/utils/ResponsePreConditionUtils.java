package north.Overthink.api.handler.utils;

import north.Overthink.exception.ResourceNotFoundException;

import java.util.ArrayList;

public class ResponsePreConditionUtils {

    public static <T> T checkNotNUll(T t) {
        if(t == null || ((ArrayList) t).size() == 0)
            throw new ResourceNotFoundException().notFoundResourceSatisfy();
        return t;
    }
}
