package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.TransactionRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Transaction;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class TransactionServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Transaction, Integer> {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Transaction save(Transaction transaction) {
        getStandardModelChangedHandler().createHandler(transaction);
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Transaction transaction = transactionRepository
                            .findById((int) customObject.get("id")).get();
        Transaction newTransaction = new Transaction(transaction);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Transaction.class, customObject, transaction, newTransaction
        );
        newTransaction = transactionRepository.save(newTransaction);
        transactionRepository.save(transaction);
        return newTransaction;
    }

    @Override
    public Transaction get(Integer integer) {
        return transactionRepository.findById(integer).get();
    }

    @Override
    public Page<Transaction> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Transaction.class);
        return transactionRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Transaction voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        Transaction transaction = transactionRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Transaction.class, voidedStandardModel, transaction
        );
        return transactionRepository.save(transaction);
    }
}
