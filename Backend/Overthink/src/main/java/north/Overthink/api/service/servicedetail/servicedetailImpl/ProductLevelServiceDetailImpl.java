package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.ProductLevelRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ActivityType;
import north.Overthink.model.submodel.ProductLevel;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class ProductLevelServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<ProductLevel, Integer> {

    @Autowired
    private ProductLevelRepository productLevelRepository;

    @Override
    public ProductLevel save(ProductLevel productLevel) {
        getStandardModelChangedHandler().createHandler(productLevel);
        return productLevelRepository.save(productLevel);
    }

    @Override
    public ProductLevel update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        ProductLevel productLevel = productLevelRepository
                            .findById((int) customObject.get("id")).get();
        ProductLevel newProductLevel = new ProductLevel(productLevel);
        getStandardModelChangedHandler().updateWithVersionHandler(
                ProductLevel.class, customObject, productLevel, newProductLevel
        );
        newProductLevel = productLevelRepository.save(newProductLevel);
        productLevelRepository.save(productLevel);
        return newProductLevel;
    }

    @Override
    public ProductLevel get(Integer integer) {
        return productLevelRepository.findById(integer).get();
    }

    @Override
    public Page<ProductLevel> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), ProductLevel.class);
        return productLevelRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public ProductLevel voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        ProductLevel productLevel = productLevelRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                ProductLevel.class, voidedStandardModel, productLevel
        );
        return productLevelRepository.save(productLevel);
    }
}
