package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.JobField;

import java.util.List;

public interface JobFieldCustomRepository {

    List<JobField> getJobFieldWithCondition(String query, int firstResult, int maxResult);
}
