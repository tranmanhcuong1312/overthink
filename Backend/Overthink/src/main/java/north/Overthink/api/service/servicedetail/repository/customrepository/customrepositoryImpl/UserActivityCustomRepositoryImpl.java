package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.UserActivityCustomRepository;
import north.Overthink.model.submodel.UserActivity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserActivityCustomRepositoryImpl implements UserActivityCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserActivity> getUserActivesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<UserActivity> typedQuery = entityManager
                .createQuery(query, UserActivity.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
