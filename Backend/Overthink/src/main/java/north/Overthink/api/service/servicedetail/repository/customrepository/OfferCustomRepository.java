package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Offer;

import java.util.List;

public interface OfferCustomRepository {

    List<Offer> getOffersWithCondition(String query, int firstResult, int maxResult);
}
