package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.NotificationRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Notification;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class NotificationServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Notification, Integer> {

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public Notification save(Notification notification) {
        getStandardModelChangedHandler().createHandler(notification);
        return notificationRepository.save(notification);
    }

    @Override
    public Notification update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Notification notification = notificationRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Notification.class, customObject, notification
        );
        return notificationRepository.save(notification);
    }

    @Override
    public Notification get(Integer integer) {
        return notificationRepository.findById(integer).get();
    }

    @Override
    public Page<Notification> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Notification.class);
        return notificationRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Notification voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Notification notification = notificationRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Notification.class, voidedStandardModel, notification
        );
        return notificationRepository.save(notification);
    }
}
