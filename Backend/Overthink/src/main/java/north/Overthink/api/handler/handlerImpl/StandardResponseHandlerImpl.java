package north.Overthink.api.handler.handlerImpl;

import north.Overthink.api.handler.StandardResponseHandler;
import north.Overthink.api.handler.utils.ResponsePreConditionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class StandardResponseHandlerImpl implements Serializable, StandardResponseHandler {

    @Override
    public <T> ResponseEntity<T> response(T t) {
        return new ResponseEntity<T>(ResponsePreConditionUtils.checkNotNUll(t), HttpStatus.OK);
    }
}
