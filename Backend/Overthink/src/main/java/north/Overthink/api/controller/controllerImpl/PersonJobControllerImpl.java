package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.PersonJob;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/person_job")
public class PersonJobControllerImpl implements StandardController<PersonJob, Integer> {

    @Autowired
    private StandardService<PersonJob, Integer> standardService;

    @Override
    public PersonJob save(PersonJob personJob) {
        return standardService.save(personJob);
    }

    @Override
    public PersonJob save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public PersonJob get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<PersonJob> get(ConditionPaging conditionPagingList) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPagingList);
    }

    @Override
    public PersonJob voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
