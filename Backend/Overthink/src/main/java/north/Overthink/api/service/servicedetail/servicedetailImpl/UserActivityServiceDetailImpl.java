package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.UserActivityRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserActivity;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class UserActivityServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<UserActivity, Integer> {

    @Autowired
    private UserActivityRepository userActivityRepository;

    @Override
    public UserActivity save(UserActivity userActivity) {
        getStandardModelChangedHandler().createHandler(userActivity);
        return userActivityRepository.save(userActivity);
    }

    @Override
    public UserActivity update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        UserActivity userActivity = userActivityRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                UserActivity.class, customObject, userActivity
        );
        return userActivityRepository.save(userActivity);
    }

    @Override
    public UserActivity get(Integer integer) {
        return userActivityRepository.findById(integer).get();
    }

    @Override
    public Page<UserActivity> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), UserActivity.class);
        return userActivityRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public UserActivity voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        UserActivity userActivity = userActivityRepository
                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                UserActivity.class, voidedStandardModel, userActivity
        );
        return userActivityRepository.save(userActivity);
    }
}
