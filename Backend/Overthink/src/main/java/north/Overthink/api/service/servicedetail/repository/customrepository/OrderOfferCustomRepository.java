package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.OrderOffer;

import java.util.List;

public interface OrderOfferCustomRepository {

    List<OrderOffer> getOrderOffersWithCondition(String query, int firstResult, int maxResult);
}
