package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.JobFieldCustomRepository;
import north.Overthink.model.submodel.JobField;
import north.Overthink.model.submodel.QJobField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface JobFieldRepository extends JpaRepository<JobField, Integer>, JobFieldCustomRepository,
        QuerydslPredicateExecutor<JobField>, QuerydslBinderCustomizer<QJobField> {

    @Override
    default public void customize(QuerydslBindings bindings, QJobField root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
