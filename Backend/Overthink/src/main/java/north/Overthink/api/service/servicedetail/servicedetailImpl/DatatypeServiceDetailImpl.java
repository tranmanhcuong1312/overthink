package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.DatatypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Datatype;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class DatatypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Datatype, Integer> {

    @Autowired
    private DatatypeRepository datatypeRepository;

    @Override
    public Datatype save(Datatype datatype) {
        getStandardModelChangedHandler().createHandler(datatype);
        return datatypeRepository.save(datatype);
    }

    @Override
    public Datatype update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Datatype datatype = datatypeRepository.findById((int) customObject.get("id")).get();
        Datatype newDatatype = new Datatype(datatype);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Datatype.class, customObject, datatype, newDatatype
        );
        newDatatype = datatypeRepository.save(newDatatype);
        datatypeRepository.save(datatype);
        return newDatatype;
    }

    @Override
    public Datatype get(Integer integer) {
        return datatypeRepository.findById(integer).get();
    }

    @Override
    public Page<Datatype> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Datatype.class);
        return datatypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Datatype voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        Datatype datatype = datatypeRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Datatype.class, voidedStandardModel, datatype
        );
        return datatypeRepository.save(datatype);
    }
}
