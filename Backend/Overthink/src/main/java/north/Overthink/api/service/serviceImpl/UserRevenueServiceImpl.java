package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserRevenue;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserRevenueServiceImpl extends InjectionServiceImpl implements StandardService<UserRevenue, Integer> {

    @Autowired
    private StandardServiceDetail<UserRevenue, Integer> standardServiceDetail;

    @Override
    public UserRevenue save(UserRevenue userRevenue) {
        return standardServiceDetail.save(userRevenue);
    }

    @Override
    public UserRevenue update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public UserRevenue get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<UserRevenue> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public UserRevenue voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
