package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.PersonAddressCustomRepository;
import north.Overthink.model.submodel.PersonAddress;
import north.Overthink.model.submodel.QPersonAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonAddressRepository extends JpaRepository<PersonAddress, Integer>, PersonAddressCustomRepository,
        QuerydslPredicateExecutor<PersonAddress>, QuerydslBinderCustomizer<QPersonAddress> {

    @Override
    default public void customize(QuerydslBindings bindings, QPersonAddress root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
