package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.OrderOfferRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.OrderOffer;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class OrderOfferServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<OrderOffer, Integer> {

    @Autowired
    private OrderOfferRepository orderOfferRepository;

    @Override
    public OrderOffer save(OrderOffer orderOffer) {
        getStandardModelChangedHandler().createHandler(orderOffer);
        return orderOfferRepository.save(orderOffer);
    }

    @Override
    public OrderOffer update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        OrderOffer orderOffer = orderOfferRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                OrderOffer.class, customObject, orderOffer
        );
        return orderOfferRepository.save(orderOffer);
    }

    @Override
    public OrderOffer get(Integer integer) {
        return orderOfferRepository.findById(integer).get();
    }

    @Override
    public Page<OrderOffer> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), OrderOffer.class);
        return orderOfferRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public OrderOffer voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        OrderOffer orderOffer = orderOfferRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                OrderOffer.class, voidedStandardModel, orderOffer
        );
        return orderOfferRepository.save(orderOffer);
    }
}
