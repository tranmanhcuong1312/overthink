package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.ContactTypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ContactType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class ContactTypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<ContactType, Integer> {

    @Autowired
    private ContactTypeRepository contactTypeRepository;

    @Override
    public ContactType save(ContactType contactType) {
        getStandardModelChangedHandler().createHandler(contactType);
        return contactTypeRepository.save(contactType);
    }

    @Override
    public ContactType update(CustomObject customObject) throws NoSuchThingForUpdateException, NoIdForUpdateException {
        ContactType contactType = contactTypeRepository.findById((int) customObject.get("id")).get();
        ContactType newContactType = new ContactType(contactType);
        getStandardModelChangedHandler().updateWithVersionHandler(
                ContactType.class, customObject, contactType, newContactType
        );
        newContactType = contactTypeRepository.save(newContactType);
        contactTypeRepository.save(contactType);
        return newContactType;
    }

    @Override
    public ContactType get(Integer integer) {
        return contactTypeRepository.findById(integer).get();
    }

    @Override
    public Page<ContactType> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), ContactType.class);
        return contactTypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public ContactType voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        ContactType contactType = contactTypeRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                ContactType.class, voidedStandardModel, contactType
        );
        return contactTypeRepository.save(contactType);
    }
}
