package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.ProductLevel;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/product_level")
public class ProductLevelControllerImpl implements StandardController<ProductLevel, Integer> {

    @Autowired
    private StandardService<ProductLevel, Integer> standardService;

    @Override
    public ProductLevel save(ProductLevel productLevel) {
        return standardService.save(productLevel);
    }

    @Override
    public ProductLevel save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public ProductLevel get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<ProductLevel> get(ConditionPaging conditionPaging) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPaging);
    }

    @Override
    public ProductLevel voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
