package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.ContactTypeCustomRepository;
import north.Overthink.api.service.servicedetail.repository.customrepository.RoleCustomRepository;
import north.Overthink.model.submodel.ContactType;
import north.Overthink.model.submodel.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ContactTypeCustomRepositoryImpl implements ContactTypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ContactType> getContactTypesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<ContactType> typedQuery = entityManager
                .createQuery(query, ContactType.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
