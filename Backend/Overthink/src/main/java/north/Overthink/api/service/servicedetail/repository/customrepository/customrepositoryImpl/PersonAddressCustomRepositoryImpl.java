package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PersonAddressCustomRepository;
import north.Overthink.model.submodel.PersonAddress;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonAddressCustomRepositoryImpl implements PersonAddressCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PersonAddress> getPersonAddressWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<PersonAddress> typedQuery = entityManager
                .createQuery(query, PersonAddress.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
