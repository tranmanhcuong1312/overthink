package north.Overthink.api.handler.handlerImpl;

import north.Overthink.api.handler.Context;
import north.Overthink.api.handler.StandardModelChanged;
import north.Overthink.model.StandardModel;
import north.Overthink.model.VoidedStandardModel;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Component
public class StandardModelChangedImpl implements StandardModelChanged {

    @Override
    public <T extends StandardModel> void created(T t) {
        t.setCreator(
                Context.getUserAuthenticated().getUser()
        );
        t.setDateCreated(getCurrentTime());
    }

    @Override
    public <T extends StandardModel> void changed(T t) {
        t.setChangedBy(
                Context.getUserAuthenticated().getUser()
        );
        t.setDateChanged(getCurrentTime());
    }

    @Override
    public <T extends StandardModel> void changedWithVersions(T oldT, T newT) {
        changed(oldT);
        created(newT);
        oldT.setNewVersion(newT);
        newT.setOldVersion(oldT);
        oldT.setVoided(true);
        oldT.setVoidedBy(Context.getUserAuthenticated().getUser());
        oldT.setVoidedDate(getCurrentTime());
    }

    @Override
    public <T extends StandardModel> void voided(T t, VoidedStandardModel voidedStandardModel) {
        if(t.isVoided() && !voidedStandardModel.isVoided()){
            t.setVoided(voidedStandardModel.isVoided());
            t.setVoidedBy(null);
            t.setVoidedDate(null);
            t.setVoidedCause(null);
        }else if(!t.isVoided() && voidedStandardModel.isVoided()){
            t.setVoided(voidedStandardModel.isVoided());
            t.setVoidedBy(
                    Context.getUserAuthenticated().getUser()
            );
            t.setVoidedDate(getCurrentTime());
            t.setVoidedCause(voidedStandardModel.getVoidedCause());
        }else{

        }

    }

    private Date getCurrentTime(){
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        return calendar.getTime();
    }
}
