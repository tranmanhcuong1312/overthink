package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.OfferRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Offer;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class OfferServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Offer, Integer> {

    @Autowired
    private OfferRepository offerRepository;

    @Override
    public Offer save(Offer offer) {
        getStandardModelChangedHandler().createHandler(offer);
        return offerRepository.save(offer);
    }

    @Override
    public Offer update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Offer offer = offerRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Offer.class, customObject, offer
        );
        return offerRepository.save(offer);
    }

    @Override
    public Offer get(Integer integer) {
        return offerRepository.findById(integer).get();
    }

    @Override
    public Page<Offer> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Offer.class);
        return offerRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Offer voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Offer offer = offerRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Offer.class, voidedStandardModel, offer
        );
        return offerRepository.save(offer);
    }
}
