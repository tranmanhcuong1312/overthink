package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.PersonContact;

import java.util.List;

public interface PersonContactCustomRepository {

    List<PersonContact> getPersonContactsWithCondition(String query, int firstResult, int maxResult);
}
