package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Transaction;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TransactionServiceImpl extends InjectionServiceImpl implements StandardService<Transaction, Integer> {

    @Autowired
    private StandardServiceDetail<Transaction, Integer> standardServiceDetail;

    @Override
    public Transaction save(Transaction transaction) {
        return standardServiceDetail.save(transaction);
    }

    @Override
    public Transaction update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public Transaction get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<Transaction> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public Transaction voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
