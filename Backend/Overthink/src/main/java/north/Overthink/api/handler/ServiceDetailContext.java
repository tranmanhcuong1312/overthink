package north.Overthink.api.handler;

import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.Serializable;
import java.util.Map;

public abstract class ServiceDetailContext implements Serializable, ApplicationContextAware {

    private ApplicationContext applicationContext;

    private static Map<String, Object> serviceDetails;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Autowired
    private ClassReflectionHandler classReflectionHandler;

    private Map<String, StandardServiceDetail> standardServiceDetailMap;

    @Autowired
    private void setStandardServiceDetailMap(Map<String, StandardServiceDetail> standardServiceDetailMap){
        this.standardServiceDetailMap = standardServiceDetailMap;
    }

    public Map<String, StandardServiceDetail> getStandardServiceDetailMap() {
        return standardServiceDetailMap;
    }

    public StandardServiceDetail getStandardDetail(Class clazz){
        return standardServiceDetailMap.get(classReflectionHandler.getClassNameDecapitalize(clazz));
    }

}
