package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.Person;

import java.util.List;

public interface PersonCustomRepository {

    List<Person> getPersonsWithCondition(String query, int firstResult, int maxResult);
}
