package north.Overthink.api.handler;

import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.StandardModel;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.persistence.Id;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Component
public class FieldReflectionHandler implements Serializable, ApplicationContextAware {

    @Autowired
    private JsonMapperHandler jsonMapperHandler;

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    private final List<Field> fieldsCantUpdateWithoutCondition = Arrays.asList(
      StandardModel.class.getDeclaredFields()
    );

    public <T extends StandardModel> Field getIdField(Class<T> c){
        if(StandardModel.class.isAssignableFrom(c)){
            List<Field> fields = getSubStandardModelField(c);
            for(Field field: fields){
                if(field.isAnnotationPresent(Id.class)){
                    return field;
                }
            }
        }
        return null;
    }

    public List<Field> getFields(Class c){
        return Arrays.asList(c.getDeclaredFields());
    }

    public Field getField(String fieldName, Class c) throws NoSuchFieldException {
        Field field = null;

        try {
            field = c.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e){

        }

        if(field == null){
            field = StandardModel.class.getDeclaredField(fieldName);
        }

        field.setAccessible(true);

        return field;
    }

    public List<Field> getStandardModelField(){
        return Arrays.asList(StandardModel.class.getDeclaredFields());
    }

    public <T extends StandardModel> List<Field> getSubStandardModelField(Class<T> t){
        List<Field> fields = new ArrayList<>();
        Stream.of(getFields(t), getStandardModelField()).forEach(fields::addAll);
        return fields;
    }

    public <T extends StandardModel> void update(Class<T> t, CustomObject newT, T oldT) throws NoIdForUpdateException, NoSuchThingForUpdateException {

        checkUpdateWithoutId(t, oldT);

        checkNoSuchThingForUpdate(t, newT);

        setSubStandardModelField(t, newT, oldT);

        setStandardModelField(newT, oldT);
    }

    public <T extends StandardModel> void voided(Class<T> t, VoidedStandardModel voidedStandardModel, T oldT) throws NoIdForUpdateException, IOException {
        checkUpdateWithoutId(t, oldT);
    }

    public <T extends StandardModel> void setSubStandardModelField(Class<T> t, CustomObject newT, T oldT){
        for(Field field: t.getDeclaredFields()){
            field.setAccessible(true);
            try{
                String fieldName = field.getName();
                if(newT.containsKey(fieldName)){
                    Object value = newT.get(fieldName);
                    Field fieldNeedChange =
                            oldT.getClass().getDeclaredField(fieldName);
                    fieldNeedChange.setAccessible(true);
                    fieldNeedChange.set(oldT, value);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public <T extends StandardModel> void setStandardModelField(CustomObject newT, T oldT){
        for(Field field: StandardModel.class.getDeclaredFields()){
            field.setAccessible(true);
            try {
                String fieldName = field.getName();
                if (newT.containsKey(fieldName)) {
                    Object value = newT.get(fieldName);
                    Field fieldNeedChange =
                            oldT.getClass().getSuperclass().getDeclaredField(fieldName);
                    fieldNeedChange.setAccessible(true);
                    if(unchanged(oldT, fieldNeedChange)){
                        if(oldT.getCreator() == null){
                            if(fieldNeedChange.getName() != getIdField(oldT.getClass()).getName()){
                                fieldNeedChange.set(oldT, value);
                            }
                        }else{
                            fieldNeedChange.set(oldT, value);
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public <T extends StandardModel> void checkUpdateWithoutId(Class<T> t, T oldT) {
        if(oldT == null){
            throw new NoIdForUpdateException().noIdFound();
        }
    }

    public <T extends StandardModel> void checkNoSuchThingForUpdate(Class<T> t, CustomObject newT) {
        if(newT.size() < 2){
            throw new NoSuchThingForUpdateException().badRequest();
        }
    }

    public <T extends StandardModel> boolean unchanged(T oldT, Field field) throws NoSuchFieldException {
        if("creator".equals(field.getName()) && oldT.getCreator() != null){
            return false;
        }
        if("dateCreated".equals(field.getName()) && oldT.getDateCreated() != null){
            return false;
        }
        if("changedBy".equals(field.getName()) && oldT.getChangedBy() != null){
            return false;
        }
        if("dateChanged".equals(field.getName())
                && (
                        oldT.getChangedBy() == null ||
                        oldT.getChangedBy() != null && oldT.getDateChanged() != null
                    )
            ){
            return false;
        }
        if("oldVersion".equals(field.getName())
                && (oldT.getChangedBy() == null ||
                    oldT.getChangedBy() != null && oldT.getOldVersion() != null
                    )
            ){
            return false;
        }
        if("newVersion".equals(field.getName())
                && (oldT.getChangedBy() == null ||
                    oldT.getChangedBy() != null && oldT.getNewVersion() != null
                    )
            ){
            return false;
        }
        if("voided".equals(field.getName()) && oldT.isVoided() == true){
            return false;
        }
        if("voidedBy".equals(field.getName()) && oldT.getVoidedBy() != null){
            return false;
        }
        if("voidedDate".equals(field.getName())
                && (oldT.isVoided() == false || oldT.getVoidedDate() != null)){
            return false;
        }
        if("voidedCause".equals(field.getName())
                && (oldT.isVoided() == false || oldT.getVoidedCause() != null)){
            return false;
        }

        return true;
    }

    public <T extends StandardModel> boolean getClassHaveClassField(Class clazz, Class fieldClass){
        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        for(Field field: fields){
            if(Objects.equals(field.getType(), clazz)){
                return true;
            }
        }
        return false;
    }
}
