package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Event;

import java.util.List;

public interface EventCustomRepository {

    List<Event> getEventsWithCondition(String query, int firstResult, int maxResult);
}
