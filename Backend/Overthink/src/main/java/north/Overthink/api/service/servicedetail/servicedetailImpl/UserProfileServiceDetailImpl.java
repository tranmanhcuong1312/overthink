package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.UserProfileRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserProfile;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class UserProfileServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<UserProfile, Integer> {

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Override
    public UserProfile save(UserProfile userProfile) {
        getStandardModelChangedHandler().createHandler(userProfile);
        return userProfileRepository.save(userProfile);
    }

    @Override
    public UserProfile update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        UserProfile userProfile = userProfileRepository
                            .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                UserProfile.class, customObject, userProfile
        );
        return userProfileRepository.save(userProfile);
    }

    @Override
    public UserProfile get(Integer integer) {
        return userProfileRepository.findById(integer).get();
    }

    @Override
    public Page<UserProfile> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), UserProfile.class);
        return userProfileRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public UserProfile voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        UserProfile userProfile = userProfileRepository
                            .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                UserProfile.class, voidedStandardModel, userProfile
        );
        return userProfileRepository.save(userProfile);
    }
}
