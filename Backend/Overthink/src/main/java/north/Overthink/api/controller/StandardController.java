package north.Overthink.api.controller;

import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

public interface StandardController<T, ID> {

    @PostMapping
    T save(@RequestBody T t);

    @PostMapping(value = "/update")
    T save(@RequestBody CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException;

    @GetMapping
    T get(@RequestParam("id") ID id);

    @PutMapping(value = "/conditions")
    Page<T> get(@RequestBody ConditionPaging conditionPaging) throws IOException, NoSuchFieldException;

    @PostMapping("/voided")
    T voided(@RequestBody VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException;
}
