package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.EventCustomRepository;
import north.Overthink.model.submodel.Event;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class EventCustomRepositoryImpl implements EventCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Event> getEventsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Event> typedQuery = entityManager
                .createQuery(query, Event.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
