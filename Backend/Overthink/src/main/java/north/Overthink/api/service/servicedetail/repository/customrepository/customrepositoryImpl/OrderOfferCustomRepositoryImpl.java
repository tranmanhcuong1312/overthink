package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.OrderOfferCustomRepository;
import north.Overthink.model.submodel.OrderOffer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderOfferCustomRepositoryImpl implements OrderOfferCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrderOffer> getOrderOffersWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<OrderOffer> typedQuery = entityManager
                .createQuery(query, OrderOffer.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
