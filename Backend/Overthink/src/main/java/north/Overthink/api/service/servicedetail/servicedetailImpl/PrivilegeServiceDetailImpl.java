package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.PrivilegeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Privilege;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class PrivilegeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Privilege, Integer> {

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Override
    public Privilege save(Privilege privilege) {
        getStandardModelChangedHandler().createHandler(privilege);
        return privilegeRepository.save(privilege);
    }

    @Override
    public Privilege update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Privilege privilege = privilegeRepository
                        .findById((int) customObject.get("id")).get();
        Privilege newPrivilege = new Privilege(privilege);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Privilege.class, customObject, privilege, newPrivilege
        );
        newPrivilege = privilegeRepository.save(newPrivilege);
        privilegeRepository.save(privilege);
        return newPrivilege;
    }

    @Override
    public Privilege get(Integer integer) {
        return privilegeRepository.findById(integer).get();
    }

    @Override
    public Page<Privilege> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Privilege.class);
        return privilegeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Privilege voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Privilege privilege = privilegeRepository
                        .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Privilege.class, voidedStandardModel, privilege
        );
        return privilegeRepository.save(privilege);
    }
}
