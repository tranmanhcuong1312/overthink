package north.Overthink.api.service.customservice;

import north.Overthink.model.submodel.Order;
import north.Overthink.model.templatemodel.CustomObject;

public interface CustomOrderService {

    Order orderCourse(CustomObject customObject) throws NoSuchFieldException;

    Order orderOffer(CustomObject customObject) throws NoSuchFieldException;
}
