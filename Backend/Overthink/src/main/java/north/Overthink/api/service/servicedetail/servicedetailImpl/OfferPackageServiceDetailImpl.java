package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.OfferPackageRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.OfferPackage;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class OfferPackageServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<OfferPackage, Integer> {

    @Autowired
    private OfferPackageRepository offerPackageRepository;

    @Override
    public OfferPackage save(OfferPackage offerPackage) {
        getStandardModelChangedHandler().createHandler(offerPackage);
        return offerPackageRepository.save(offerPackage);
    }

    @Override
    public OfferPackage update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        OfferPackage offerPackage = offerPackageRepository
                                .findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                OfferPackage.class, customObject, offerPackage
        );
        return offerPackageRepository.save(offerPackage);
    }

    @Override
    public OfferPackage get(Integer integer) {
        return offerPackageRepository.findById(integer).get();
    }

    @Override
    public Page<OfferPackage> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), OfferPackage.class);
        return offerPackageRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public OfferPackage voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        OfferPackage offerPackage = offerPackageRepository
                                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                OfferPackage.class, voidedStandardModel, offerPackage
        );
        return offerPackageRepository.save(offerPackage);
    }
}
