package north.Overthink.api.service.customservice;

import org.springframework.context.MessageSource;

public interface MessageSourceService extends MessageSource {

    String getMessage(String code, Object object);
}
