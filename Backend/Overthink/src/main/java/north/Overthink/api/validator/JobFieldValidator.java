package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.JobField;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = JobField.class)
public class JobFieldValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return JobField.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Job field validator");
    }
}
