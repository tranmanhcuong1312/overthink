package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Revenue;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RevenueServiceImpl extends InjectionServiceImpl implements StandardService<Revenue, Integer> {

    @Autowired
    private StandardServiceDetail<Revenue, Integer> standardServiceDetail;

    @Override
    public Revenue save(Revenue revenue) {
        return standardServiceDetail.save(revenue);
    }

    @Override
    public Revenue update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public Revenue get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<Revenue> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public Revenue voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
