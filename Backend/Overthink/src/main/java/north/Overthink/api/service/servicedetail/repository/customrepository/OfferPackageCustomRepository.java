package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.OfferPackage;

import java.util.List;

public interface OfferPackageCustomRepository {

    List<OfferPackage> getOfferPackagesWithCondition(String query, int firstResult, int maxResult);
}
