package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.OrderRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Order;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class OrderServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Order, Integer> {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        getStandardModelChangedHandler().createHandler(order);
        return orderRepository.save(order);
    }

    @Override
    public Order update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Order order = orderRepository.findById((int) customObject.get("id")).get();
        getStandardModelChangedHandler().updateHandler(
                Order.class, customObject, order
        );
        return orderRepository.save(order);
    }

    @Override
    public Order get(Integer integer) {
        return orderRepository.findById(integer).get();
    }

    @Override
    public Page<Order> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Order.class);
        return orderRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Order voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        Order order = orderRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Order.class, voidedStandardModel, order
        );
        return orderRepository.save(order);
    }
}
