package north.Overthink.api.handler.utils;

import north.Overthink.annotation.CustomValidator;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ValidatorUtil implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext(){
        return this.applicationContext;
    }

    public void validator(Object object, Errors errors){

        List<Validator> validators = new ArrayList<>();
        List<Validator> validatorsWillBeUse = new ArrayList<>();

        Class clazz = object.getClass();
        Map map = getApplicationContext().getBeansOfType(Validator.class);

        for(Object key: map.keySet()){
            validators.add((Validator) map.get(key));
        }

        if(!validators.isEmpty()){
            for(Validator validator: validators){
                if(validator.getClass().isAnnotationPresent(CustomValidator.class)){
                    CustomValidator customValidator =
                            (CustomValidator) validator.getClass().getAnnotation(
                                    CustomValidator.class
                            );

                    if(clazz.equals(customValidator.value())){
                        validatorsWillBeUse.add(validator);
                    }
                }
            }

            for(Validator validator: validatorsWillBeUse){
                validator.validate(object, errors);
            }
        }
    }
}
