package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.TaskTypeRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.TaskType;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class TaskTypeServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<TaskType, Integer> {

    @Autowired
    private TaskTypeRepository taskTypeRepository;

    @Override
    public TaskType save(TaskType taskType) {
        getStandardModelChangedHandler().createHandler(taskType);
        return taskTypeRepository.save(taskType);
    }

    @Override
    public TaskType update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        TaskType taskType = taskTypeRepository
                    .findById((int) customObject.get("id")).get();
        TaskType newTaskType = new TaskType(taskType);
        getStandardModelChangedHandler().updateWithVersionHandler(
                TaskType.class, customObject, taskType, newTaskType
        );
        newTaskType = taskTypeRepository.save(newTaskType);
        taskTypeRepository.save(taskType);
        return newTaskType;
    }

    @Override
    public TaskType get(Integer integer) {
        return taskTypeRepository.findById(integer).get();
    }

    @Override
    public Page<TaskType> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), TaskType.class);
        return taskTypeRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public TaskType voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        TaskType taskType = taskTypeRepository
                .findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                TaskType.class, voidedStandardModel, taskType
        );
        return taskTypeRepository.save(taskType);
    }
}
