package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.Notification;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(Notification.class)
public class NotificationValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Notification.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("Notification validator");
        Notification notification = (Notification) target;
    }
}
