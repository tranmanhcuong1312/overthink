package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.JobField;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/job_field")
public class JobFieldControllerImpl implements StandardController<JobField, Integer> {

    @Autowired
    private StandardService<JobField, Integer> standardService;

    @Override
    public JobField save(JobField jobField) {
        return standardService.save(jobField);
    }

    @Override
    public JobField save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public JobField get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<JobField> get(ConditionPaging conditionPaging) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPaging);
    }

    @Override
    public JobField voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
