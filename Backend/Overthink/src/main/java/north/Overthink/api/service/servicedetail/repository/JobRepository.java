package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.JobCustomRepository;
import north.Overthink.model.submodel.Job;
import north.Overthink.model.submodel.QJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer>, JobCustomRepository,
        QuerydslPredicateExecutor<Job>, QuerydslBinderCustomizer<QJob> {

    @Override
    default public void customize(QuerydslBindings bindings, QJob root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
