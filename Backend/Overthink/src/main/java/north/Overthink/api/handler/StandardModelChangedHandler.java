package north.Overthink.api.handler;

import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.StandardModel;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.templatemodel.CustomObject;

import java.io.IOException;

public interface StandardModelChangedHandler {

    <T extends StandardModel> void createHandler(T t);

    <T extends StandardModel> void updateHandler(Class<T> t, CustomObject customObject, T oldT) throws NoSuchThingForUpdateException, NoIdForUpdateException;

    <T extends StandardModel> void updateWithVersionHandler(Class<T> t, CustomObject customObject, T oldT, T newT) throws NoSuchThingForUpdateException, NoIdForUpdateException;

    <T extends StandardModel> void voidedHandler(Class<T> t, VoidedStandardModel voidedStandardModel, T oldT) throws IOException, NoIdForUpdateException;
}
