package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Notification;

import java.util.List;

public interface NotificationCustomRepository {

    List<Notification> getNotificationsWithCondition(String query, int firstResult, int maxResult);
}
