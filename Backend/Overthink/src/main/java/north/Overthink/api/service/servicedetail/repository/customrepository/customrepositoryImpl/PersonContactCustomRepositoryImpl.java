package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.PersonContactCustomRepository;
import north.Overthink.model.submodel.PersonContact;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonContactCustomRepositoryImpl implements PersonContactCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PersonContact> getPersonContactsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<PersonContact> typedQuery = entityManager
                .createQuery(query, PersonContact.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
