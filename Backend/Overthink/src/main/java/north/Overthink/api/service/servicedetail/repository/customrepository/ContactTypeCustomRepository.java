package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.ContactType;

import java.util.List;

public interface ContactTypeCustomRepository {

    List<ContactType> getContactTypesWithCondition(String query, int firstResult, int maxResult);
}
