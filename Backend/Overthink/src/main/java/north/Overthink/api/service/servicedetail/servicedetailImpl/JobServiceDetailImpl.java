package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.JobRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Job;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class JobServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<Job, Integer> {

    @Autowired
    private JobRepository jobRepository;

    @Override
    public Job save(Job job) {
        getStandardModelChangedHandler().createHandler(job);
        return jobRepository.save(job);
    }

    @Override
    public Job update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        Job job = jobRepository.findById((int) customObject.get("id")).get();
        Job newJob = new Job(job);
        getStandardModelChangedHandler().updateWithVersionHandler(
                Job.class, customObject, job, newJob
        );
        newJob = jobRepository.save(newJob);
        jobRepository.save(job);
        return newJob;
    }

    @Override
    public Job get(Integer integer) {
        return jobRepository.findById(integer).get();
    }

    @Override
    public Page<Job> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), Job.class);
        return jobRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public Job voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        Job job = jobRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                Job.class, voidedStandardModel, job
        );
        return jobRepository.save(job);
    }
}
