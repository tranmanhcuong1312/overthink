package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.CourseCustomRepository;
import north.Overthink.model.submodel.Course;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class CourseCustomRepositoryImpl implements CourseCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Course> getCoursesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Course> typedQuery = entityManager
                .createQuery(query, Course.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
