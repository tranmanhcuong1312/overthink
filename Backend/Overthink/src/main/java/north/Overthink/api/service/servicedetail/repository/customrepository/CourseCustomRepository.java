package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Course;

import java.util.List;

public interface CourseCustomRepository {

    List<Course> getCoursesWithCondition(String query, int firstResult, int maxResult);
}
