package north.Overthink.api.controller.customcontroller;

import north.Overthink.model.submodel.User;
import north.Overthink.model.templatemodel.AuthenticationRequest;
import north.Overthink.model.templatemodel.UserInfoAuthentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationController {

    @PostMapping("/authenticate")
    UserInfoAuthentication createAuthenticationToken(
            @RequestBody AuthenticationRequest authenticationRequest,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception;

    @PostMapping("/register")
    UserInfoAuthentication saveUser(@RequestBody User user,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws Exception;

    @PostMapping("/out")
    Boolean logout(HttpServletRequest request,
                   HttpServletResponse response);
}
