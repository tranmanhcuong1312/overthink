package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.CoursePackageCustomRepository;
import north.Overthink.model.submodel.CoursePackage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class CoursePackageCustomRepositoryImpl implements CoursePackageCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CoursePackage> getCoursePackagesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<CoursePackage> typedQuery = entityManager
                .createQuery(query, CoursePackage.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
