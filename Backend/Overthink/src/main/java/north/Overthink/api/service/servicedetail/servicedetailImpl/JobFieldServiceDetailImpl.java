package north.Overthink.api.service.servicedetail.servicedetailImpl;

import com.querydsl.core.types.dsl.BooleanExpression;
import north.Overthink.annotation.ServiceDetail;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.api.service.servicedetail.repository.JobFieldRepository;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.JobField;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

@ServiceDetail
public class JobFieldServiceDetailImpl extends InjectionServiceDetailImpl implements StandardServiceDetail<JobField, Integer> {

    @Autowired
    private JobFieldRepository jobFieldRepository;

    @Override
    public JobField save(JobField jobField) {
        getStandardModelChangedHandler().createHandler(jobField);
        return jobFieldRepository.save(jobField);
    }

    @Override
    public JobField update(CustomObject customObject)
            throws NoSuchThingForUpdateException, NoIdForUpdateException {
        JobField jobField = jobFieldRepository.findById((int) customObject.get("id")).get();
        JobField newJobField = new JobField(jobField);
        getStandardModelChangedHandler().updateWithVersionHandler(
                JobField.class, customObject, jobField, newJobField
        );
        newJobField = jobFieldRepository.save(newJobField);
        jobFieldRepository.save(jobField);
        return newJobField;
    }

    @Override
    public JobField get(Integer integer) {
        return jobFieldRepository.findById(integer).get();
    }

    @Override
    public Page<JobField> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        BooleanExpression booleanExpression =
                getConditionUtil().getPredicates(conditionPaging.getConditions(), JobField.class);
        return jobFieldRepository.findAll(booleanExpression,
                getConditionUtil().getPageable(conditionPaging.getCustomPageRequest()));
    }

    @Override
    public JobField voided(VoidedStandardModel voidedStandardModel)
            throws IOException, NoIdForUpdateException {
        JobField jobField = jobFieldRepository.findById(voidedStandardModel.getId()).get();
        getStandardModelChangedHandler().voidedHandler(
                JobField.class, voidedStandardModel, jobField
        );
        return jobFieldRepository.save(jobField);
    }
}
