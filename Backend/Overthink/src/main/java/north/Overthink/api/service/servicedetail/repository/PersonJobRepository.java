package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.PersonJobCustomRepository;
import north.Overthink.model.submodel.PersonJob;
import north.Overthink.model.submodel.QPersonJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonJobRepository extends JpaRepository<PersonJob, Integer>, PersonJobCustomRepository,
        QuerydslPredicateExecutor<PersonJob>, QuerydslBinderCustomizer<QPersonJob> {

    @Override
    default public void customize(QuerydslBindings bindings, QPersonJob root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
