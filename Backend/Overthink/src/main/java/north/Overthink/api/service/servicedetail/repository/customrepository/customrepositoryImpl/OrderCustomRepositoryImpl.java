package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.OrderCustomRepository;
import north.Overthink.model.submodel.Order;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class OrderCustomRepositoryImpl implements OrderCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Order> getOrdersWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Order> typedQuery = entityManager
                .createQuery(query, Order.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
