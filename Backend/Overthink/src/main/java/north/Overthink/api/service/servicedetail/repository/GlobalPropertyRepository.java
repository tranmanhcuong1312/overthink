package north.Overthink.api.service.servicedetail.repository;

import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import north.Overthink.api.service.servicedetail.repository.customrepository.GlobalPropertyCustomRepository;
import north.Overthink.model.submodel.GlobalProperty;
import north.Overthink.model.submodel.QGlobalProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalPropertyRepository extends JpaRepository<GlobalProperty, Integer>, GlobalPropertyCustomRepository,
        QuerydslPredicateExecutor<GlobalProperty>, QuerydslBinderCustomizer<QGlobalProperty> {

    @Override
    default public void customize(QuerydslBindings bindings, QGlobalProperty root){
        bindings.bind(String.class)
                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
