package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.DatatypeCustomRepository;
import north.Overthink.model.submodel.Datatype;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class DatatypeCustomRepositoryImpl implements DatatypeCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Datatype> getDatatypeWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<Datatype> typedQuery = entityManager
                .createQuery(query, Datatype.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
