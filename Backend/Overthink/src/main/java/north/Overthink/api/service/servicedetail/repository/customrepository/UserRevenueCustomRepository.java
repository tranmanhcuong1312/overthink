package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.Role;
import north.Overthink.model.submodel.UserRevenue;

import java.util.List;

public interface UserRevenueCustomRepository {

    List<UserRevenue> getUserRevenuesWithCondition(String query, int firstResult, int maxResult);
}
