package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.Offer;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class OfferServiceImpl extends InjectionServiceImpl implements StandardService<Offer, Integer> {

    @Autowired
    private StandardServiceDetail<Offer, Integer> standardServiceDetail;

    @Override
    public Offer save(Offer offer) {
        offer.setActive(true);
        return standardServiceDetail.save(offer);
    }

    @Override
    public Offer update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public Offer get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<Offer> get(ConditionPaging conditionPaging)
            throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public Offer voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
