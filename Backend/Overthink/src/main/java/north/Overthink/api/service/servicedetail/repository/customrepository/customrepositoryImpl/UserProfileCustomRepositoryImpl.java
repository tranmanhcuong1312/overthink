package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.UserProfileCustomRepository;
import north.Overthink.model.submodel.UserProfile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserProfileCustomRepositoryImpl implements UserProfileCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserProfile> getUserProfilesWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<UserProfile> typedQuery = entityManager
                .createQuery(query, UserProfile.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
