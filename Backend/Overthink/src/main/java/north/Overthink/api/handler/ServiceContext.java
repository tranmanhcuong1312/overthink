package north.Overthink.api.handler;

import org.springframework.context.ApplicationContextAware;

import java.io.Serializable;

public abstract class ServiceContext extends ServiceDetailContext implements Serializable, ApplicationContextAware {

}
