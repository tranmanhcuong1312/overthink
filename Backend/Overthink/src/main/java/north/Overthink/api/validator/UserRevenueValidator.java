package north.Overthink.api.validator;

import north.Overthink.annotation.CustomValidator;
import north.Overthink.model.submodel.UserRevenue;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@CustomValidator(value = UserRevenue.class)
public class UserRevenueValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserRevenue.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        System.out.println("User revenue validator");
    }
}
