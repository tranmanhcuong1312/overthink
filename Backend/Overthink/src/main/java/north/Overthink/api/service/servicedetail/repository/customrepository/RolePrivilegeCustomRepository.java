package north.Overthink.api.service.servicedetail.repository.customrepository;

import north.Overthink.model.submodel.RolePrivilege;

import java.util.List;

public interface RolePrivilegeCustomRepository {

    List<RolePrivilege> getRolePrivilegesWithCondition(String query, int firstResult, int maxResult);
}
