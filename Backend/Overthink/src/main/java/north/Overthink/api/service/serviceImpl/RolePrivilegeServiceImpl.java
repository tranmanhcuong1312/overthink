package north.Overthink.api.service.serviceImpl;

import north.Overthink.api.service.StandardService;
import north.Overthink.api.service.servicedetail.StandardServiceDetail;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.RolePrivilege;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RolePrivilegeServiceImpl extends InjectionServiceImpl implements StandardService<RolePrivilege, Integer> {

    @Autowired
    private StandardServiceDetail<RolePrivilege, Integer> standardServiceDetail;

    @Override
    public RolePrivilege save(RolePrivilege rolePrivilege) {
        return standardServiceDetail.save(rolePrivilege);
    }

    @Override
    public RolePrivilege update(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardServiceDetail.update(customObject);
    }

    @Override
    public RolePrivilege get(Integer integer) {
        return standardServiceDetail.get(integer);
    }

    @Override
    public Page<RolePrivilege> get(ConditionPaging conditionPaging) throws NoSuchFieldException {
        return standardServiceDetail.get(conditionPaging);
    }

    @Override
    public RolePrivilege voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardServiceDetail.voided(voidedStandardModel);
    }
}
