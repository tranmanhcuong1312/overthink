package north.Overthink.api.service.servicedetail.repository.customrepository.customrepositoryImpl;

import north.Overthink.api.service.servicedetail.repository.customrepository.ProductLevelCustomRepository;
import north.Overthink.model.submodel.ProductLevel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductLevelCustomRepositoryImpl implements ProductLevelCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ProductLevel> getProductLevelsWithCondition(String query, int firstResult, int maxResult) {
        TypedQuery<ProductLevel> typedQuery = entityManager
                .createQuery(query, ProductLevel.class);
        if(firstResult != 0)
                typedQuery.setFirstResult(firstResult);
        if(maxResult != 0)
                typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }
}
