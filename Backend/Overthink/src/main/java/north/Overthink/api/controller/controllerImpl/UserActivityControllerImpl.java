package north.Overthink.api.controller.controllerImpl;

import north.Overthink.api.controller.StandardController;
import north.Overthink.api.service.StandardService;
import north.Overthink.exception.NoIdForUpdateException;
import north.Overthink.exception.NoSuchThingForUpdateException;
import north.Overthink.model.VoidedStandardModel;
import north.Overthink.model.submodel.UserActivity;
import north.Overthink.model.templatemodel.ConditionPaging;
import north.Overthink.model.templatemodel.CustomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/user_activity")
public class UserActivityControllerImpl implements StandardController<UserActivity, Integer> {

    @Autowired
    private StandardService<UserActivity, Integer> standardService;

    @Override
    public UserActivity save(UserActivity userActivity) {
        return standardService.save(userActivity);
    }

    @Override
    public UserActivity save(CustomObject customObject) throws NoSuchThingForUpdateException, NoSuchFieldException, NoIdForUpdateException {
        return standardService.update(customObject);
    }

    @Override
    public UserActivity get(Integer integer) {
        return standardService.get(integer);
    }

    @Override
    public Page<UserActivity> get(ConditionPaging conditionPaging) throws IOException, NoSuchFieldException {
        return standardService.get(conditionPaging);
    }

    @Override
    public UserActivity voided(VoidedStandardModel voidedStandardModel) throws IOException, NoIdForUpdateException {
        return standardService.voided(voidedStandardModel);
    }
}
