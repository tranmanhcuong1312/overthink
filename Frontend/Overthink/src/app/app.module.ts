import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from 'src/app/web/common/configuration/token.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './web/main/main.component';
import { SystemComponent } from './web/system/system.component';
import { AuthenticationComponent } from './web/authentication/authentication.component';
import { HeaderComponent } from './web/main/layout/header/header.component';
import { FooterComponent } from './web/main/layout/footer/footer.component';
import { LoginComponent } from './web/authentication/login/login.component';
import { HeaderProfileComponent } from './web/main/layout/header-profile/header-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SystemComponent,
    AuthenticationComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HeaderProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports:[
    MainComponent,
    HeaderComponent,
    FooterComponent,
    HeaderProfileComponent
  ],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
