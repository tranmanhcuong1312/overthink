import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationComponent } from './web/authentication/authentication.component';
import { MainComponent } from './web/main/main.component';
import { SystemComponent } from './web/system/system.component';

const routes: Routes = [
  {
    path: 'authentication',
    loadChildren: () => import('./web/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'system',
    loadChildren: () => import('./web/system/system.module').then(m => m.SystemModule)
  },
  {
    path: '',
    loadChildren: () => import('./web/main/main.module').then(m => m.MainModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
