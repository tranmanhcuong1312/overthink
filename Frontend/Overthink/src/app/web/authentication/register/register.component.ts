import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service'; 
import { Router } from "@angular/router"
import { environment } from 'src/environments/environment';
import { UserInforAuthentication } from '../../common/models/template-models/user-infor-authentication.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private builder: FormBuilder, 
              private service: AuthenticationService,
              private router: Router) {
    this.registerForm = this.builder.group({
      registeredEmail: ['', [Validators.required]],
      username:['', [Validators.required]],
      password: ['', [Validators.required]],
      secretQuestion: ['', [Validators.required]],
      secretAnswer: ['', [Validators.required]]
    })
   }

  ngOnInit(): void {
  }


  register(){
    console.log(this.registerForm.value);
    console.log(this.registerForm.value.role);
    this.service.register(this.registerForm.value).subscribe(
      data => {
        let userInforAuthentication = new UserInforAuthentication(data);
        localStorage.setItem(environment.UserInforAuthenticationProperty, JSON.stringify(userInforAuthentication));
        this.router.navigate(['']);
      },
      error => {
        console.log(error);
      }
    )
  }
}
