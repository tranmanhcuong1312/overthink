import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service'; 
import {Router} from "@angular/router"
import { environment } from 'src/environments/environment';
import { UserInforAuthentication } from 'src/app/web/common/models/template-models/user-infor-authentication.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private builder: FormBuilder, 
    private service: AuthenticationService,
    private router: Router) 
    {
      this.loginForm = this.builder.group({
        username:['', [Validators.required]],
        password: ['', [Validators.required]],
      })
    }

  ngOnInit(): void {

  }

  login(){
    this.service.login(this.loginForm.value).subscribe(
      data => {
        let userInforAuthentication = new UserInforAuthentication(data);
        localStorage.setItem(environment.UserInforAuthenticationProperty, JSON.stringify(userInforAuthentication));
        this.router.navigate(['']);
      },
      error =>{
        console.log(error);
      }
    )
  }

}
