import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../common/services/authentication.service'; 
import {Router} from "@angular/router"

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})

export class AuthenticationComponent implements OnInit {

  constructor(private builder: FormBuilder, 
              private service: AuthenticationService,
              private router: Router) {
   }

  ngOnInit(): void {

  }

}
