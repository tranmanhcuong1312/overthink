import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { ConditionPaging } from '../../common/models/template-models/condition-paging.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { JobField } from '../../common/models/sub-models/job-field.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private jobFieldService: StandardModelService,
              private conditionPagingService: ConditionPagingService) { }

  ngOnInit(): void {
    
  }
}
