import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/web/common/models/sub-models/user.model';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';

@Component({
  selector: 'app-header-profile',
  templateUrl: './header-profile.component.html',
  styleUrls: ['./header-profile.component.css']
})
export class HeaderProfileComponent implements OnInit {

  isLogin: boolean = false;
  isLoginAdmin: boolean = false;
  authenticatedUser: User | undefined;

  username: string = "";
  role: string = "";

  constructor(private auth: AuthenticationService,
    private standard: StandardModelService,
    private condition: ConditionPagingService,
    private router: Router) { }

ngOnInit(): void {
this.checkLogin();
this.getDataLogin();
this.checkLoginAdmin();
}

checkLogin(){
this.authenticatedUser = this.auth.getAuthenticatedUser().user;
if(this.authenticatedUser != null || this.authenticatedUser != undefined){
this.isLogin = true;
}else{
this.isLogin = false;
}
}

  getDataLogin(){
    if(this.authenticatedUser != null || this.authenticatedUser != undefined){
      this.username = this.authenticatedUser.username;
      this.role = this.authenticatedUser.role.name;
    }
  }

  checkLoginAdmin(){
    if(this.authenticatedUser != null || this.authenticatedUser != undefined){
      if(this.authenticatedUser.role.name != "user"){
        this.isLoginAdmin = true;
      }else{
        this.isLoginAdmin = false;
      }
    }
  }

}
