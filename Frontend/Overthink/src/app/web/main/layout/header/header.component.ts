import { Component, OnInit, SimpleChanges } from '@angular/core';
import { JobField } from 'src/app/web/common/models/sub-models/job-field.model';
import { User } from 'src/app/web/common/models/sub-models/user.model';
import { ConditionPaging } from 'src/app/web/common/models/template-models/condition-paging.model';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogin: boolean = false;
  isLoginAdmin: boolean = false;
  authenticatedUser: User | undefined;

  jobFieldList: JobField[] | null | undefined;

  username: string = "";
  role: string = "";

  constructor(private auth: AuthenticationService,
              private standard: StandardModelService,
              private condition: ConditionPagingService,
              private router: Router) { }

  ngOnInit(): void {
    this.checkLogin();
    this.getDataLogin();
    this.checkLoginAdmin();
    this.getJobFieldList();
  }

  checkLogin(){
    this.authenticatedUser = this.auth.getAuthenticatedUser().user;
    if(this.authenticatedUser != null || this.authenticatedUser != undefined){
      this.isLogin = true;
    }else{
      this.isLogin = false;
    }
  }

  getDataLogin(){
    if(this.authenticatedUser != null || this.authenticatedUser != undefined){
      this.username = this.authenticatedUser.username;
      this.role = this.authenticatedUser.role.name;
    }
  }

  checkLoginAdmin(){
    if(this.authenticatedUser != null || this.authenticatedUser != undefined){
      if(this.authenticatedUser.role.name != "user"){
        this.isLoginAdmin = true;
      }else{
        this.isLoginAdmin = false;
      }
    }
  }

  getJobFieldList(){
    let conditionPaging: ConditionPaging = 
      this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  
      this.standard.getWithCondition(conditionPaging, "job_field").subscribe(
        data => {
          this.jobFieldList = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  logout(){
    this.auth.logout();
    this.checkLogin();
    this.checkLoginAdmin();
    this.router.navigate(['']);
  }
}
