import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  isProfile: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.headerByLink();
  }

  headerByLink(){
    let url = this.router.url;
    if(url.includes('profile')){
      this.isProfile = true
    }else{
      this.isProfile = false;
    }
  }
}
