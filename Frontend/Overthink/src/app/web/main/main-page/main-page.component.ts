import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Course } from '../../common/models/sub-models/course.model';
import { Offer } from '../../common/models/sub-models/offer.model';
import { Request } from '../../common/models/sub-models/request.model';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { ConditionKey } from '../../common/models/template-models/condition-key.enum';
import { CustomOrderService } from 'src/app/web/common/services/custom-order.service';
import { error } from 'selenium-webdriver';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  courseList: Course[] = [];
  offerList: Offer[] = [];
  requestList: Request[] = [];
  eventList: Event[] = [];

  page: number = 0;
  size: number = 0;

  constructor(private standard: StandardModelService,
              private authentication: AuthenticationService,
              private conditionPaging: ConditionPagingService,
              private customOrder: CustomOrderService) 
  {

  }

  ngOnInit(): void {
    this.getEventList();
    this.getOfferList();
    this.getCourseList();
    this.getRequestList();
  }

  getConditionActive(){
    return this.conditionPaging.getCondition("active", ConditionKey.EQUAL, true);
  }

  getConditionPagingWithActive(){
    return  this.conditionPaging.getConditionPagingWithListCondition(
        [this.getConditionActive()], new CustomPageRequest(this.page, this.size, Direction.ASC, "id"), false 
      );
  }

  getConditionPaging(){
    return  this.conditionPaging.getConditionPagingWithListCondition(
        [], new CustomPageRequest(this.page, this.size, Direction.ASC, "id"), false 
      );
  }

  getCourseList(){
    this.standard.getWithCondition(this.getConditionPagingWithActive(), "course").subscribe(
      data => {
        this.courseList = data
      },
      error => {
        console.log(error);
      }
    )
  }

  getOfferList(){
    this.standard.getWithCondition(this.getConditionPagingWithActive(), "offer").subscribe(
      data => {
        this.offerList = data,
        console.log(this.offerList)
      },
      error => {
        console.log(error);
      }
    )
  }

  getRequestList(){
    this.standard.getWithCondition(this.getConditionPagingWithActive(), "request").subscribe(
      data => {
        this.requestList = data
      },
      error => {
        console.log(error);
      }
    )
  }

  getEventList(){
    this.standard.getWithCondition(this.getConditionPaging(), "event").subscribe(
      data => {
        this.eventList = data
      },
      error => {
        console.log(error);
      }
    )
  }

  openOrderCourseForm(course: Course){
    let map = new Map<String, any>();
    map.set("id", course.id);
    const object = Object.fromEntries(map);
    this.customOrder.orderCourse(object).subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  openOrderOffersForm(offer: Offer){
    let map = new Map<String, any>();
    map.set("id", offer.id);
    const object = Object.fromEntries(map);
    this.customOrder.orderOffer(object).subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }
}
