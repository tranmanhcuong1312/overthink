import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { OfferComponent } from './offer/offer.component';
import { CourseComponent } from './course/course.component';
import { RequestComponent } from './request/request.component';
import { OrderComponent } from './order/order.component';
import { ActiveComponent } from './active/active.component';
import { RevenueComponent } from './revenue/revenue.component';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [OfferComponent, CourseComponent, RequestComponent, OrderComponent, ActiveComponent, RevenueComponent, EvaluationComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProfileModule { }
