import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { UserProfile } from 'src/app/web/common/models/sub-models/user-profile.model';
import { UserActivity } from 'src/app/web/common/models/sub-models/user-activity.model';

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrls: ['./active.component.css']
})
export class ActiveComponent implements OnInit {

  userProfile!: UserProfile;
  userActivities: UserActivity[] = [];

  constructor(private standard: StandardModelService,
    private authentication: AuthenticationService,
    private condition: ConditionPagingService) { }

  ngOnInit(): void {
    this.getUserProfile();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getAuthenticatedUser(){
    return this.authentication.getAuthenticatedUser().user;
  }

  getConditionId(){
    return this.condition.getCondition("user", ConditionKey.EQUAL, this.getAuthenticatedUser().id);
  }

  getConditionProfileId(){
    return this.condition.getCondition("userProfile", ConditionKey.EQUAL, this.userProfile.id);
  }

  getConditionPagingWithId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getConditionPagingWithProfileId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionProfileId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getUserProfile(){
    this.standard.getWithCondition(this.getConditionPaging(), 'user_profile').subscribe(
      data => {
        this.userProfile = data[0];
        this.getActivitys();
      },
      error => {
        console.log(error);
      }
    )
  }

  getActivitys(){
    this.standard.getWithCondition(this.getConditionPagingWithProfileId(), 'user_activity').subscribe(
      data => {
        this.userActivities = data
      },
      error => {
        console.log(error);
      }
    )
  }

}
