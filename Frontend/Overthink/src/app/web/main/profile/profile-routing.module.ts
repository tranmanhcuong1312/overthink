import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActiveComponent } from './active/active.component';
import { CourseComponent } from './course/course.component';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { OfferComponent } from './offer/offer.component';
import { OrderComponent } from './order/order.component';
import { ProfileComponent } from './profile.component';
import { RequestComponent } from './request/request.component';
import { RevenueComponent } from './revenue/revenue.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children:[
      {
        path: 'active',
        component: ActiveComponent
      },
      {
        path: 'course',
        component: CourseComponent
      },
      {
        path: 'evaluation',
        component: EvaluationComponent
      },
      {
        path: 'offer',
        component: OfferComponent
      },
      {
        path: 'order',
        component: OrderComponent
      },
      {
        path: 'request',
        component: RequestComponent
      },
      {
        path: 'revenue',
        component: RevenueComponent
      },
      {
        path: '',
        component: ProfileComponent
      },
      {
        path: '**',
        component: ProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
