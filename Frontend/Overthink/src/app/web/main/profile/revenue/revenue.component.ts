import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { UserProfile } from 'src/app/web/common/models/sub-models/user-profile.model';
import { UserRevenue } from 'src/app/web/common/models/sub-models/user-revenue.model';
import { Revenue } from 'src/app/web/common/models/sub-models/revenue.model';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.css']
})
export class RevenueComponent implements OnInit {

  userProfile!: UserProfile;
  userRevenue!: UserRevenue;
  revenues: Revenue[] = [];

  constructor(private standard: StandardModelService,
    private authentication: AuthenticationService,
    private condition: ConditionPagingService) { }

  ngOnInit(): void {
    this.getUserProfile();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getAuthenticatedUser(){
    return this.authentication.getAuthenticatedUser().user;
  }

  getConditionId(){
    return this.condition.getCondition("user", ConditionKey.EQUAL, this.getAuthenticatedUser().id);
  }

  getConditionProfileId(){
    return this.condition.getCondition("userProfile", ConditionKey.EQUAL, this.userProfile.id);
  }

  getConditionRevenueId(){
    return this.condition.getCondition("userRevenue", ConditionKey.EQUAL, this.userRevenue.id);
  }

  getConditionPagingWithId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getConditionPagingWithProfileId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionProfileId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getConditionPagingWithRevenueId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionRevenueId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getUserProfile(){
    this.standard.getWithCondition(this.getConditionPaging(), 'user_profile').subscribe(
      data => {
        this.userProfile = data[0];
        this.getUserRevenue();
      },
      error => {
        console.log(error);
      }
    )
  }

  getUserRevenue(){
    this.standard.getWithCondition(this.getConditionPagingWithProfileId(), 'user_revenue').subscribe(
      data => {
        this.userRevenue = data[0];
        console.log(this.userRevenue)
        this.getRevenues();
      },
      error => {
        console.log(error);
      }
    )
  }

  getRevenues(){
    this.standard.getWithCondition(this.getConditionPagingWithRevenueId(), 'revenue').subscribe(
      data => {
        this.revenues = data
        console.log(this.revenues)
      },
      error => {
        console.log(error);
      }
    )
  }
}
