import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Offer } from 'src/app/web/common/models/sub-models/offer.model';
import { Job } from 'src/app/web/common/models/sub-models/job.model';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { ProductLevel } from 'src/app/web/common/models/sub-models/product-level.model';
import { VoidedStandardModel } from 'src/app/web/common/models/voided-standard-model.model';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  jobs: Job[] = [];
  offers: Offer[] = [];
  offerForm: FormGroup;
  offerChange!: Offer;
  jobChoose!: Job;
  productLevel!: ProductLevel;

  constructor(private standard: StandardModelService,
              private authentication: AuthenticationService,
              private condition: ConditionPagingService,
              private builder: FormBuilder) 
  {
    this.offerForm = this.builder.group({
      name: ['', Validators.required],
      content: ['', Validators.required],
      job: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getJobs();
    this.getOffers();
    this.getProductLevel();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getAuthenticatedUser(){
    return this.authentication.getAuthenticatedUser().user;
  }

  getConditionId(){
    return this.condition.getCondition("creator", ConditionKey.EQUAL, this.getAuthenticatedUser().id);
  }

  getConditionPagingWithId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getJobs(){
    this.standard.getWithCondition(this.getConditionPaging(), 'job').subscribe(
      data => {
        this.jobs = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  getOffers(){
    this.standard.getWithCondition(this.getConditionPagingWithId(), 'offer').subscribe(
      data => {
        this.offers = data
      },
      error => {
        console.log(error);
      }
    )
  }

  getProductLevel(){
    this.standard.get(1, 'product_level').subscribe(
      data => {
        this.productLevel = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  openCreateForm(jobNeed: Job){
    this.offerForm.setValue({
      name: '',
      content: '',
      job: jobNeed.name
      
    })
    this.jobChoose = jobNeed;
  }

  openEditForm(offer: Offer){
    this.offerForm.setValue({
      name: offer.name,
      content: offer.content,
      job: ''
    })
    this.offerChange = offer;
  }

  createOffer(){
    let map = new Map<String, any>();
    Object.keys(this.offerForm.controls).forEach(key => {
      map.set(key, this.offerForm.controls[key].value);
    });
    map.set('job', this.jobChoose);
    map.set('productLevel', this.productLevel);
    map.set('orderCount', 0);
    map.set('evaluationRate', 0);
    const object = Object.fromEntries(map);
    this.standard.save( object, "offer").subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  updateOffer(){
    let map = new Map<String, any>(); 
    if(this.offerForm.value != null){
      Object.keys(this.offerForm.controls).forEach(key => {
        map.set(key, this.offerForm.controls[key].value);
      });
      map.set("id", this.offerChange.id);
      map.delete("job");
      const object = Object.fromEntries(map);
      this.standard.update(object , "offer").subscribe(
        data => {
          window.location.reload();
        },
        error =>{
          console.log(error);
        }
      )
    }else{
      throw new Error('Form need value');
    }
  }

  voidedOffer(offer: Offer){
    let voided = new VoidedStandardModel(offer.id, true, '');
    this.standard.voided(voided, 'offer').subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }
}
