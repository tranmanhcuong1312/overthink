import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Offer } from 'src/app/web/common/models/sub-models/offer.model';
import { Job } from 'src/app/web/common/models/sub-models/job.model';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { Order } from 'src/app/web/common/models/sub-models/order.model';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orders: Order[] = [];

  constructor(private standard: StandardModelService,
    private authentication: AuthenticationService,
    private condition: ConditionPagingService,
    private builder: FormBuilder) 
  {
 
  }

  ngOnInit(): void {
   this.getOrder();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getAuthenticatedUser(){
    return this.authentication.getAuthenticatedUser().user;
  }

  getConditionId(){
    return this.condition.getCondition("creator", ConditionKey.EQUAL, this.getAuthenticatedUser().id);
  }

  getConditionPagingWithId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getOrder(){
    this.standard.getWithCondition(this.getConditionPagingWithId(), "order").subscribe(
      data => {
        this.orders = data
      },
      error => {
        console.log(error);
      }
    )
  }
}
