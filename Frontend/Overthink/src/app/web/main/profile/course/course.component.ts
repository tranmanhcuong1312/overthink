import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { error } from 'selenium-webdriver';
import { Course } from 'src/app/web/common/models/sub-models/course.model';
import { Job } from 'src/app/web/common/models/sub-models/job.model';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';
import { VoidedStandardModel } from 'src/app/web/common/models/voided-standard-model.model';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  jobs: Job[] = [];
  courses: Course[] = [];
  courseForm: FormGroup;
  courseChange!: Course;
  jobChoose!: Job;

  constructor(private standard: StandardModelService,
    private authentication: AuthenticationService,
    private condition: ConditionPagingService,
    private builder: FormBuilder) 
  {
  this.courseForm = this.builder.group({
    name: ['', Validators.required],
    content: ['', Validators.required],
    job: ['', Validators.required],
    lessons: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.getJobs();
    this.getCourses();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getAuthenticatedUser(){
    return this.authentication.getAuthenticatedUser().user;
  }

  getConditionId(){
    return this.condition.getCondition("creator", ConditionKey.EQUAL, this.getAuthenticatedUser().id);
  }

  getConditionPagingWithId(){
    return  this.condition.getConditionPagingWithListCondition(
      [this.getConditionId()], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
    );
  }

  getJobs(){
    this.standard.getWithCondition(this.getConditionPaging(), 'job').subscribe(
      data => {
        this.jobs = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  getCourses(){
    this.standard.getWithCondition(this.getConditionPagingWithId(), 'course').subscribe(
      data => {
        this.courses = data
      },
      error => {
        console.log(error);
      }
    )
  }

  openCreateForm(jobNeed: Job){
    this.courseForm.setValue({
      name: '',
      content: '',
      job: jobNeed.name,
      lessons: 0
    })
    this.jobChoose = jobNeed;
  }

  openEditForm(course: Course){
    this.courseForm.setValue({
      name: course.name,
      content: course.content,
      job: '',
      lessons: course.lessons
    })
    this.courseChange = course;
  }

  createCourse(){
    let map = new Map<String, any>();
    Object.keys(this.courseForm.controls).forEach(key => {
      map.set(key, this.courseForm.controls[key].value);
    });
    map.set('job', this.jobChoose);
    map.set('evaluationRate', 0);
    map.set('active', true);  
    const object = Object.fromEntries(map);
    this.standard.save( object, "course").subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  updateCourse(){
    let map = new Map<String, any>(); 
    if(this.courseForm.value != null){
      Object.keys(this.courseForm.controls).forEach(key => {
        map.set(key, this.courseForm.controls[key].value);
      });
      map.set("id", this.courseChange.id);
      map.delete("job");
      const object = Object.fromEntries(map);
      this.standard.update(object , "course").subscribe(
        data => {
          window.location.reload();
        },
        error =>{
          console.log(error);
        }
      )
    }else{
      throw new Error('Form need value');
    }
  }

  voidedCourse(course: Course){
    let voided = new VoidedStandardModel(course.id, true, '');
    this.standard.voided(voided, 'course').subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

}
