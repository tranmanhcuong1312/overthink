import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UserProfile } from '../../common/models/sub-models/user-profile.model';
import { User } from '../../common/models/sub-models/user.model';
import { AuthenticationService } from '../../common/services/authentication.service';
import { ConditionPagingService } from '../../common/services/condition-paging.service';
import { StandardModelService } from '../../common/services/standard-model.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  authenticatedUser:User | undefined;
  userProfile!:UserProfile;
  isNotProfilePage: boolean = false;

  userProfileForm: FormGroup;

  constructor(private auth: AuthenticationService,
    private standard: StandardModelService,
    private condition: ConditionPagingService,
    private router: Router,
    private build: FormBuilder) {
      this.userProfileForm = this.build.group({
        name: ['', Validators.required],
        profileLink: ['', Validators.required]
      })
     }

  ngOnInit(): void {
    this.checkLink();
    this.getUserProfile();
  }

  getUserProfile(){
    this.authenticatedUser = this.auth.getAuthenticatedUser().user
    this.standard.get(this.authenticatedUser.id, "user_profile").subscribe(
      data => {
        this.userProfile = data
        this.setFormValue(data);
      },
      error => {
        console.log(error);
      }
    )
  }

  setFormValue(userProfile: UserProfile){
    this.userProfileForm.setValue({
      name: userProfile.name,
      profileLink: userProfile.profileLink
    })
  }

  updateProfile(){
    let map = new Map<String, any>();
    if(this.userProfileForm.value != null){
      Object.keys(this.userProfileForm.controls).forEach(key => {
        map.set(key, this.userProfileForm.controls[key].value);
      });
      map.set("id", this.userProfile.id);
      const object = Object.fromEntries(map);
      this.standard.update(object , "user_profile").subscribe(
        data => {
          window.location.reload();
        },
        error =>{
          console.log(error);
        }
      )
    }
  }

  checkLink(){
    if(this.router.url == '/profile'){
      this.isNotProfilePage = false;
    }else{
      this.isNotProfilePage = true;
    }
  }
}
