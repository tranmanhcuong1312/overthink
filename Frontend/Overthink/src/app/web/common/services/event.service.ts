import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  AddressesRemovedEvent= new EventEmitter();
  
  constructor() { }
}
