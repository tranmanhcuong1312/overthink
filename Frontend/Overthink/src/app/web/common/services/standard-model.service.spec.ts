import { TestBed } from '@angular/core/testing';

import { StandardModelService } from './standard-model.service';

describe('StandardModelService', () => {
  let service: StandardModelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StandardModelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
