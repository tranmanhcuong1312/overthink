import { Injectable } from '@angular/core';
import { ConditionPaging } from 'src/app/web/common/models/template-models/condition-paging.model';
import { CustomPageRequest } from 'src/app/web/common/models/template-models/custom-page-request.model';
import { Condition } from 'src/app/web/common/models/template-models/condition.model';
import { ConditionKey } from 'src/app/web/common/models/template-models/condition-key.enum';
import { Direction } from 'src/app/web/common/models/template-models/direction.enum';

@Injectable({
  providedIn: 'root'
})
export class ConditionPagingService {

  constructor() { }

  getCuctomPageRequest(page: number, size: number, direction: Direction, sortBy: string){
    return new CustomPageRequest(page, size, direction, sortBy);
  }

  getCondition(field: string, conditionKey: ConditionKey, value: any){
    return new Condition(field, conditionKey, value);
  }

  getConditionPaging(condition: Condition, customPageRequest: CustomPageRequest, includeVoided: boolean){
    if(condition != null)
      return new ConditionPaging([condition], customPageRequest, includeVoided);
    else
      return new ConditionPaging([], customPageRequest, includeVoided);
  }

  getConditionPagingWithListCondition(conditions: Condition[], customPageRequest: CustomPageRequest, includeVoided: boolean){
    return new ConditionPaging(conditions, customPageRequest, includeVoided);
  }

  getNextPage(page: number, size: number, direction: Direction, sortBy: string){
    return this.getCuctomPageRequest(page+1, size, direction, sortBy);
  }

  getPreviousPage(page: number, size: number, direction: Direction, sortBy: string){
    if(page != 0)
      return this.getCuctomPageRequest(page-1, size, direction, sortBy);
    return this.getCuctomPageRequest(page, size, direction, sortBy);
  }
}
