import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserInforAuthentication } from '../models/template-models/user-infor-authentication.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http : HttpClient) { }

  login(formData: any): Observable<any>{
    return this.http.post(`http://localhost:8080/authenticate`, formData);
  }

  register(formData: any): Observable<any>{
    return this.http.post(`${environment.OverthinkServer}` + `/register`, formData);
  }

  logout(){
    localStorage.removeItem(environment.UserInforAuthenticationProperty);
  }

  getAuthenticatedUser(){
    let userInforAuthentication: UserInforAuthentication;
    userInforAuthentication = JSON.parse(localStorage.getItem(environment.UserInforAuthenticationProperty) || "{}");
    return userInforAuthentication;{}
  }
}
