import { Injectable } from '@angular/core';
import { HttpClient, HttpParams , HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CustomOrderService {

  slash: string = `/`;

  constructor(private http : HttpClient, private authentication: AuthenticationService) { }

  orderCourse(data: any): Observable<any>{
    return this.http.post(`${environment.OverthinkServer}` + this.slash + `custom_order/course`, data, {headers: this.getHeaders()});
  }

  orderOffer(data: any): Observable<any>{
    return this.http.post(`${environment.OverthinkServer}` + this.slash + `custom_order/offer`, data, {headers: this.getHeaders()});
  }

  getHeaders(){
    return new HttpHeaders().set('Authorization', this.authentication.getAuthenticatedUser().token);
  }
}
