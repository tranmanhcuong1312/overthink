import { TestBed } from '@angular/core/testing';

import { ConditionPagingService } from './condition-paging.service';

describe('ConditionPagingService', () => {
  let service: ConditionPagingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConditionPagingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
