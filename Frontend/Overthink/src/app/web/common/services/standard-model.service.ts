import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams , HttpHeaders} from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { CustomObject } from '../models/template-models/custom-object.model';
import { VoidedStandardModel } from '../models/voided-standard-model.model';
import { ConditionPaging } from '../models/template-models/condition-paging.model';
import { ModelDontNeedHeaderForGet } from '../models/template-models/ModelDontNeedHeaderForGet.enum';

@Injectable({
  providedIn: 'root'
})
export class StandardModelService {

  slash: string = `/`;

  constructor(private http : HttpClient,
              private authentication: AuthenticationService) { }

  save(formData: any, model: string): Observable<any>{
    return this.http.post(
      `${environment.OverthinkServer}` + this.slash + `${model}`, 
      formData, 
      {
        headers: this.getHeaders()}
      );
  }

  update(formData: CustomObject, model: string): Observable<any>{
    return this.http.post(
      `${environment.OverthinkServer}` + this.slash + `${model}` + `/update`, 
      formData,
      {
        headers: this.getHeaders()}
      );
  }

  get(id: number, model: string): Observable<any>{
    if(Object.values(ModelDontNeedHeaderForGet).includes(model as ModelDontNeedHeaderForGet)){
      return this.http.get(
        `${environment.OverthinkServer}` + this.slash + `${model}`,
        {
          params : this.getSingleParam("id", id)
        }
        );
    }
    return this.http.get(
      `${environment.OverthinkServer}` + this.slash +  `${model}`,
      { 
        headers: this.getHeaders(),
        params : this.getSingleParam("id", id)
      }
      );
  }

  getWithCondition(formData: any, model: string): Observable<any>{
    if(Object.values(ModelDontNeedHeaderForGet).includes(model as ModelDontNeedHeaderForGet)){
      return this.http.put(
        `${environment.OverthinkServer}` + this.slash + `${model}` + `/conditions`,
        formData
        );
    }
    return this.http.put(
      `${environment.OverthinkServer}` + this.slash + `${model}` + `/conditions`,
      formData,
      {
        headers: this.getHeaders()
      }
      );
  }

  voided(formData: VoidedStandardModel, model: string){
    return this.http.post(
      `${environment.OverthinkServer}` + this.slash + `${model}`,
      formData,
      {headers: this.getHeaders()}
    )
  }

  getHeaders(){
    return new HttpHeaders().set('Authorization', this.authentication.getAuthenticatedUser().token);
  }

  getSingleParam(paramName: string, value: any){
    return new HttpParams().set(paramName, value);
  }

  getParamForConditionGet(value: ConditionPaging){
    return new HttpParams().set("conditionPaging", JSON.stringify(value));
  }
}
