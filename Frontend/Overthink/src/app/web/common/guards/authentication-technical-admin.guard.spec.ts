import { TestBed } from '@angular/core/testing';

import { AuthenticationTechnicalAdminGuard } from './authentication-technical-admin.guard';

describe('AuthenticationTechnicalAdminGuard', () => {
  let guard: AuthenticationTechnicalAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthenticationTechnicalAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
