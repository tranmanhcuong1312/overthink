import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { Router } from '@angular/router';
import { User } from '../models/sub-models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationSuperAdminGuard implements CanActivate {

  constructor(private authentication: AuthenticationService,
              private router: Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.authentication.getAuthenticatedUser() != null){
        let user: User = this.authentication.getAuthenticatedUser().user;
        if(user.role.name == "super_admin"){
          return true;
        }
      }
    return false;
  }
  
}
