import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationChildGuard implements CanActivate {

  constructor(private authentication: AuthenticationService){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.authentication.getAuthenticatedUser() == null){
      
      return false;
    }
      return true;
  }
  
}
