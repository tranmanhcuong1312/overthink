import { TestBed } from '@angular/core/testing';

import { AuthenticationSuperAdminGuard } from './authentication-super-admin.guard';

describe('AuthenticationSuperAdminGuard', () => {
  let guard: AuthenticationSuperAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthenticationSuperAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
