import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { Router } from '@angular/router'
import { User } from '../models/sub-models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationAdminGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService,
              private router: Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.authenticationService.getAuthenticatedUser() != null){
        let user: User = this.authenticationService.getAuthenticatedUser().user;
        if(user.role.name == "super_admin" || user.role.name == "technical_admin"){
          return true;
        }
      }
      this.router.navigate(['']);
      return false;
  }
  
}
