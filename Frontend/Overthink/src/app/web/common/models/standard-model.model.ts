import { User } from "./sub-models/user.model";

export class StandardModel<T>{
    public id: number;
    public creator: User;
    public dateCreated: Date;
    public changedBy: User;
    public dateChanged: Date;
    oldVersion: T;
    newVersion: T;
    voided: boolean;
    voidedBy: User;
    voidedDate: Date;
    voidedCourse: string;

    constructor(params: any){
        this.id = params.id || 0;
        this.creator = params.creator|| null;
        this.dateCreated = params.dateCreated || null;
        this.changedBy = params.changedBy || null;
        this.dateChanged = params.dateChanged || null;
        this.oldVersion = params.oldVersion || null;
        this.newVersion = params.newVersion || null;
        this.voided = params.voided || null;
        this.voidedBy = params.voidedBy || null;
        this.voidedDate = params.voidedDate || null;
        this.voidedCourse = params.voidedCourse || null;
    }
}