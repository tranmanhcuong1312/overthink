import { ConditionKey } from "./condition-key.enum";

export class Condition{
    field: string;
    conditionKey: ConditionKey;
    value: any;

    constructor(field: string, conditionKey: ConditionKey, value: any){
        this.field = field || "";
        this.conditionKey = conditionKey || null;
        this.value = value || null;
    }
}