import { Condition } from "./condition.model";
import { CustomPageRequest } from "./custom-page-request.model";

export class ConditionPaging{
    conditions: Condition[];
    customPageRequest: CustomPageRequest;
    includeVoided: boolean;

    constructor(conditions: Condition[], customPageRequest: CustomPageRequest, includeVoided: boolean) {
        this.conditions = conditions || null;
        this.customPageRequest = customPageRequest || null;
        this.includeVoided = includeVoided || false;
    }
}