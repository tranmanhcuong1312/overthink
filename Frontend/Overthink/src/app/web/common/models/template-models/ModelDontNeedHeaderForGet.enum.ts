export enum ModelDontNeedHeaderForGet{
    Job = 'job',
    JobField = 'job_field',
    Offer = 'offer',
    Course = 'course',
    Order = 'order',
    Event = 'event',
    Request = 'request'
}