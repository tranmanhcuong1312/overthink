import { User } from "../sub-models/user.model";

export class UserInforAuthentication{
    user: User;
    token: string;

    constructor(params: any) {
        this.user = params.user || null;
        this.token = params.token || null;
    }
}