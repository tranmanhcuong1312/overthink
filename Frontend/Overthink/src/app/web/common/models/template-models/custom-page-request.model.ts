import { Direction } from "./direction.enum";

export class CustomPageRequest{
    page: number;
    size: number;
    direction: Direction;
    sortBy: string;

    constructor(page: number, size: number, direction: Direction, sortBy: string) {
        this.page = page || 0;
        this.size = size || 0;
        this.direction = direction || Direction.ASC;
        this.sortBy = sortBy || "";
    }
}