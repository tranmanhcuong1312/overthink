import { User } from "./sub-models/user.model";

export class VoidedStandardModel{
    id: number;
    voided:boolean;
    voidedCause: string;

    constructor(id: number, voided: boolean, voidedCause: string){
        this.id = id || 0;
        this.voided = voided || false;
        this.voidedCause = voidedCause || "";
    }
}