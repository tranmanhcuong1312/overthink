import { StandardModel } from "../standard-model.model";
import { OfferPackage } from "./offer-package.model";
import { Offer } from "./offer.model";
import { Order } from "./order.model";

export class OrderOffer extends StandardModel<OrderOffer>{
    order: Order;
    offer: Offer;
    offerPackage: OfferPackage;
    
    constructor(params: any) {
        super(params);
        this.order = params.order || null;
        this.offer = params.offer || null;
        this.offerPackage = params.offerPackage || null;
    }
}