import { StandardModel } from "../standard-model.model";
import { ActivityType } from "./activity-type.model";
import { UserProfile } from "./user-profile.model";

export class UserActivity extends StandardModel<UserActivity>{
    userProfile: UserProfile;
    activityType: ActivityType;
    content: string;

    constructor(params: any){
        super(params);
        this.userProfile = params.userProfile || null;
        this.activityType = params.activityType || null;
        this.content = params.content || null;
    }
}