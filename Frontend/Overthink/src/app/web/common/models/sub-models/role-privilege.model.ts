import { StandardModel } from "../standard-model.model";
import { Privilege } from "./privilege.model";
import { Role } from "./role.model";

export class RolePrivilege extends StandardModel<RolePrivilege>{
    role: Role;
    privilege: Privilege

    constructor(params: any){
        super(params);
        this.role = params.role || null;
        this.privilege = params.privilege || null;
    }
}