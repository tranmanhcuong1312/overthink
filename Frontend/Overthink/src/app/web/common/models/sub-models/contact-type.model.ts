import { StandardModel } from "../standard-model.model";
import { Datatype } from "./datatype.model";

export class ContactType extends StandardModel<ContactType>{
    name: string;
    datatype: Datatype;

    constructor(params: any){
        super(params);
        this.name = params.name;
        this.datatype = params.datatype;
    }
}