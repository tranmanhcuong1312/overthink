import { StandardModel } from "../standard-model.model";
import { JobField } from "./job-field.model";

export class Job extends StandardModel<Job>{
    jobField: JobField;
    name: string;
    description: string;

    constructor(params: any){
        super(params);
        this.jobField = params.jobField || null;
        this.name = params.name || null;
        this.description = params.description || null;
    }
}