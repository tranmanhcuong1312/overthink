import { StandardModel } from "../standard-model.model";

export class TaskType extends StandardModel<TaskType>{

    name: string;
    description: string;

    constructor(params: any) {
        super(params);
        this.name = params.name || "";
        this.description = params.description || "";
    }
}