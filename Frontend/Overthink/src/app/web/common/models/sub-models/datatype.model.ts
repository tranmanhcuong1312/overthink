import { StandardModel } from "../standard-model.model";

export class Datatype extends StandardModel<Datatype>{
    name: string;
    description: string;

    constructor(params: any){
        super(params);
        this.name = params.name;
        this.description = params.description;
    }
}