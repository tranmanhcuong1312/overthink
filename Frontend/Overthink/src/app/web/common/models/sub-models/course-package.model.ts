import { StandardModel } from "../standard-model.model";
import { Course } from "./course.model";
import { Package } from "./package.model";

export class CoursePackage extends StandardModel<CoursePackage>{
    course: Course;
    aPackage: Package;
    content: string;
    price: number;
    time: Date;
    order: number;

    constructor(params: any){
        super(params);
        this.course = params.course || null;
        this.aPackage = params.aPackage || null;
        this.content = params.content || null;
        this.price = params.price || 0;
        this.time = params.time || null;
        this.order = params.order || 0;
    }
}