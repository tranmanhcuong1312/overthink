import { StandardModel } from "../standard-model.model";

export class Event extends StandardModel<Event>{
    name: string;
    content: string;
    startDate: Date;
    endDate: Date;

    constructor(params: any){
        super(params);
        this.name = params.name || null;
        this.content = params.content || null;
        this.startDate = params.startDate || null;
        this.endDate = params.endDate || null;
    }
}