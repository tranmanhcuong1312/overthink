import { StandardModel } from "../standard-model.model";
import { OrderCourse } from "./order-course.model";
import { OrderOffer } from "./order-offer.model";
import { Request } from "./request.model";
import { User } from "./user.model";

export class Order extends StandardModel<Order>{
    buyer: User;
    seller: User;
    request: Request;
    state: string;
    deadline: Date;

    constructor(params: any){
        super(params);
        this.buyer = params.buyer || null;
        this.seller = params.seller || null;
        this.request = params.request || null;
        this.state = params.state || null;
        this.deadline = params.deadline || null;
    }
}