import { StandardModel } from "../standard-model.model";
import { CoursePackage } from "./course-package.model";
import { Course } from "./course.model";
import { Order } from "./order.model";

export class OrderCourse extends StandardModel<OrderCourse>{
    order: Order;
    course: Course;
    coursePackage: CoursePackage;

    constructor(params: any) {
        super(params);
        this.order = params.order || null;
        this.course = params.course || null;
        this.coursePackage = params.coursePackage || null;
    }
}