import { StandardModel } from "../standard-model.model"
import { Job } from "./job.model";
import { ProductLevel } from "./product-level.model";
import { User } from "./user.model";

export class Offer extends StandardModel<Offer>{
    active: boolean;
    name: string;
    content: string;
    job: Job;
    productLevel: ProductLevel;
    price: number;
    orderCount: number;
    evaluationRate: number;

    constructor(params: any){
        super(params);
        this.active = params.active || false;
        this.orderCount = params.orderCount || 0;
        this.price = params.price ||0;
        this.productLevel = params.productLevel || null;
        this.job = params.job || null;
        this.name = params.name || null;
        this.content = params.content || null;
        this.evaluationRate = params.evaluationRate || 0;
    }
}