import { StandardModel } from "../standard-model.model";
import { JobField } from "./job-field.model";

export class PersonJob extends StandardModel<PersonJob>{
    jobField: JobField;
    name: string;
    startDate: Date;
    endDate: Date;
    level: string;
    company: string;
    description: string;

    constructor(params: any){
        super(params);
        this.jobField = params.JobField || null;
        this.name = params.name || null;
        this.startDate = params.startDate || null;
        this.endDate = params.endDate || null;
        this.level = params.level || null;
        this.company = params.company || null;
        this.description = params.description || null;
    }
}