import { StandardModel } from "../standard-model.model";
import { Revenue } from "./revenue.model";
import { UserProfile } from "./user-profile.model";

export class UserRevenue extends StandardModel<UserRevenue>{
    totalIncome: number;
    incomePerMonth: number;
    userProfile: UserProfile;

    constructor(params: any) {
        super(params);
        this.totalIncome = params.totalIncome || 0;
        this.incomePerMonth = params.incomePerMonth || 0;
        this.userProfile = params.userProfile || null;
    }
}