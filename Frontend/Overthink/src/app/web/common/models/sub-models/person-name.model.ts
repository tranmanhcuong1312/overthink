import { StandardModel } from "../standard-model.model";

export class PersonName extends StandardModel<PersonName>{
    givenName: string;
    middleName: string;
    familyName1: string;
    familyName2: string;

    constructor(params: any){
        super(params);
        this.givenName = params.givenName || null;
        this.middleName = params.middleName || null;
        this.familyName1 = params.familyName1 || null;
        this.familyName2 = params.familyName2 || null;
    }
}