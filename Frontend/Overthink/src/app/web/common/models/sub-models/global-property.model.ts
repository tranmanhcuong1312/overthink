import { StandardModel } from "../standard-model.model";
import { Datatype } from "./datatype.model";

export class GlobalProperty extends StandardModel<GlobalProperty>{
    name: string;
    datatype: Datatype;
    value: string;
    description: string;

    constructor(params: any){
        super(params);
        this.name = params.name || null;
        this.datatype = params.datatype || null;
        this.value = params.value || null;
        this.description = params.description || null;
    }
}