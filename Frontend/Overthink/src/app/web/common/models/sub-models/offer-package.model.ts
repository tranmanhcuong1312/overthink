import { StandardModel } from "../standard-model.model";
import { Offer } from "./offer.model";
import { Package } from "./package.model";

export class OfferPackage extends StandardModel<OfferPackage>{
    offer: Offer;
    aPackage: Package;
    name: string;
    content: string;
    price: number;
    deliveryTime: Date;
    order: number;

    constructor(params: any){
        super(params);
        this.offer = params.offer || null;
        this.aPackage = params.aPackage || null;
        this.name = params.name || null;
        this.content = params.content || null;
        this.price = params.price || 0;
        this.deliveryTime = params.deliveryTime || null;
        this.order = params.order || 0;
    }
}