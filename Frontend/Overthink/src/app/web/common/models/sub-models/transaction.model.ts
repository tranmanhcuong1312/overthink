import { StandardModel } from "../standard-model.model";
import { TransactionType } from "./transaction-type.model";
import { User } from "./user.model";

export class Transaction extends StandardModel<Transaction>{
    sender: User;
    receiver: User;
    transactionType: TransactionType;
    value: number;
    content: string;
    description: string;
    isPayed: string;

    constructor(params: any) {
        super(params);
        this.sender = params.sender || null;
        this.receiver = params.receiver || null;
        this.transactionType = params.transactionType || null;
        this.value = params.value || null;
        this.content = params.content || null;
        this.description = params.desciption || null;
        this.isPayed = params.isPayed || false;
    }
}