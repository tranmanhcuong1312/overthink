import { StandardModel } from "../standard-model.model";
import { Job } from "./job.model";
import { User } from "./user.model";

export class Course extends StandardModel<Course>{
    active: boolean;
    name: string;
    content: string;
    job: Job;
    price: number;
    lessons: number;
    evaluationRate: number;

    constructor(params: any){
        super(params);
        this.active = params.active || false;
        this.name = params.name || null;
        this.content = params.content || null;
        this.job = params.job || null;
        this.price = params.price ||0;
        this.lessons = params.lessons || 0;
        this.evaluationRate = params.evaluationRate || 0; 
    }    
}