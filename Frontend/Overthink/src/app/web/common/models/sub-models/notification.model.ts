import { StandardModel } from "../standard-model.model";
import { NotificationType } from "./notification-type.model";
import { User } from "./user.model";

export class Notification extends StandardModel<Notification>{
    notificationType: NotificationType;
    receiver: User;
    content: string;
    isRead: boolean;

    constructor(params: any){
        super(params);
        this.notificationType = params.notificationType || null;
        this.receiver = params.receiver || null;
        this.content = params.content || null;
        this.isRead = params.isRead || false;
    }
}