import { StandardModel } from "../standard-model.model";
import { ContactType } from "./contact-type.model";
import { UserProfile } from "./user-profile.model";

export class UserContact extends StandardModel<UserContact>{
    userProfile: UserProfile;
    contactType: ContactType;
    value: string;

    constructor(params: any){
        super(params);
        this.userProfile = params.userProfile || null;
        this.contactType = params.contactType || null;
        this.value = params.value || null;
    }
}