import { StandardModel } from "../standard-model.model";
import { TaskType } from "./task-type.model";

export class Task extends StandardModel<Task>{
    taskType: TaskType;
    name: string;
    content: string;
    priority: string;

    constructor(params: any) {
        super(params);
        this.taskType = params.taskType || null;
        this.name = params.name || "";
        this.content = params.contetnt || "";
        this.priority = params.priority || "";
    }
}