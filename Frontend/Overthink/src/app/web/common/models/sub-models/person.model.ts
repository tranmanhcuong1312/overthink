import { StandardModel } from "../standard-model.model";
import { PersonAddress } from "./person-address.model";
import { PersonContact } from "./person-contact.model";
import { PersonJob } from "./person-job.model";
import { PersonName } from "./person-name.model";
import { User } from "./user.model";

export class Person extends StandardModel<Person>{
    gender: string;
    birthday: Date;
    death: boolean;
    deathDate: Date;
    deathCause: string;
    users: User[];
    personNames: PersonName[];
    personAddress: PersonAddress[];
    personJobs: PersonJob[];
    personContacts: PersonContact[];

    constructor(params: any) {
        super(params);
        this.gender = params.gender || null;
        this.birthday = params.birthday || null;
        this.death = params.death || false;
        this.deathDate = params.deathDate || null;
        this.deathCause = params.deathCause || null;
        this.users = params.users || null;
        this.personNames = params.personNames || null;
        this.personAddress = params.personAddress || null;
        this.personJobs = params.personJobs || null;
        this.personContacts = params.personContacts || null;
    }
}