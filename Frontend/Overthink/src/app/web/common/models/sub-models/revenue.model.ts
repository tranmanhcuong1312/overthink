import { StandardModel } from "../standard-model.model";
import { UserRevenue } from "./user-revenue.model";

export class Revenue extends StandardModel<Revenue>{
    userRevenue: UserRevenue;
    month: Date;
    income: number;

    constructor(params: any) {
        super(params);
        this.userRevenue = params.userRevenue || null;
        this.month = params.month || null;
        this.income = params.income || 0;
    }
}