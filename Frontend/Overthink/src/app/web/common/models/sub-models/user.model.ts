import { StandardModel } from "../standard-model.model";
import { Role } from "./role.model";

export class User extends StandardModel<User>{
    registeredEmail: string;
    username: string;
    secretQuestion: string;
    role: Role;

    constructor(params: any){
        super(params);
        this.registeredEmail = params.registeredEmail || null;
        this.username = params.username || null;
        this.secretQuestion = params.secretQuestion || null;
        this.role = params.role || null;
    }
}