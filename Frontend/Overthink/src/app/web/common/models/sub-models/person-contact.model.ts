import { StandardModel } from "../standard-model.model";
import { ContactType } from "./contact-type.model";

export class PersonContact extends StandardModel<PersonContact>{
    contactType: ContactType;
    description: string;
    value: string;

    constructor(params: any){
        super(params);
        this.contactType = params.contactType || null;
        this.description = params.description || null;
        this.value = params.value || null;
    }
}