import { StandardModel } from "../standard-model.model";
import { User } from "./user.model";

export class UserProfile extends StandardModel<UserProfile>{
    user: User;
    name: string;
    profileLink: string;

    constructor(params: any) {
        super(params);
        this.user = params.user || null;
        this.name = params.name || null;
        this.profileLink = params.profileLink || null;
    }
}