import { StandardModel } from "../standard-model.model";

export class PersonAddress extends StandardModel<PersonAddress>{
    address: string;
    cityVillage: string;
    stateProvince: string;
    country: string;
    postalCode: string;

    constructor(params: any){
        super(params);
        this.address = params.address || null;
        this.cityVillage = params.cityVillage || null;
        this.stateProvince = params.stateProvince || null;
        this.country = params.country || null;
        this.postalCode = params.postalCode || null;
    }
}