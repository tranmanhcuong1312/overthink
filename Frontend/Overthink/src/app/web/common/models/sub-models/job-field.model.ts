import { StandardModel } from "../standard-model.model";
import { Job } from "./job.model";

export class JobField extends StandardModel<JobField>{
    name: string;
    description: string;
    jobs: Job[];

    constructor(params: any){
        super(params);
        this.name = params.name || null;
        this.description = params.description || null;
        this.jobs = params.jobs || null;
    }
}