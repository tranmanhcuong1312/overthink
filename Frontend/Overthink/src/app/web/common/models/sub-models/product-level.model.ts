import { StandardModel } from "../standard-model.model";

export class ProductLevel extends StandardModel<ProductLevel>{
    name: string;
    orderCondition: number;
    rateCondition: number;
    description: string;

    constructor(params: any){
        super(params);
        this.name = params.name || null;
        this.orderCondition = params.orderCondition || 0;
        this.rateCondition = params.rateCondition || 0;
        this.description = params.description || null;
    }
}