import { StandardModel } from "../standard-model.model";
import { Job } from "./job.model";
import { User } from "./user.model";

export class Request extends StandardModel<Request>{
    active: boolean;
    user: User;
    job: Job;
    name: string;
    description: string;
    lowestPrice: number;
    highestprice: number;
    expectTime: Date;

    constructor(params: any){
        super(params);
        this.active = params.active || false;
        this.user = params.user || null;
        this.job = params.job || null;
        this.name = params.name || null;
        this.description = params.description || null;
        this.lowestPrice = params.lowestPrice || 0;
        this.highestprice = params.highestprice || 0;
        this.expectTime = params.expectTime || null;
    }
}