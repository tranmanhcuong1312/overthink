import { StandardModel } from "../standard-model.model";

export class TransactionType extends StandardModel<TransactionType>{
    name: string;
    description: string;

    constructor(params: any){
        super(params);
        this.name = params.name || null;
        this.description = params.description || null;
    }
}