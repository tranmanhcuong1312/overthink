import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { throwError, Observable } from "rxjs";
import { AuthenticationService } from "../services/authentication.service";
import { catchError } from 'rxjs/internal/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private authenticationService: AuthenticationService) {
        
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
               
            }
        });
        return next.handle(req)
        .pipe(
            catchError((err: HttpErrorResponse) => {
                if (err.status === 401) {
                    this.authenticationService.logout();
                }
                return throwError(err);
            }),
        )
    }

    

}