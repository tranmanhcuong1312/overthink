import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSystemPropertiesComponent } from './web-system-properties.component';

describe('WebSystemPropertiesComponent', () => {
  let component: WebSystemPropertiesComponent;
  let fixture: ComponentFixture<WebSystemPropertiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebSystemPropertiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSystemPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
