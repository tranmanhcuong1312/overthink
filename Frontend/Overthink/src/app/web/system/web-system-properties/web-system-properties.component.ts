import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { ConditionPaging } from '../../common/models/template-models/condition-paging.model';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { ActivityType } from '../../common/models/sub-models/activity-type.model';
import { ContactType } from '../../common/models/sub-models/contact-type.model';
import { Datatype } from '../../common/models/sub-models/datatype.model';
import { TaskType } from '../../common/models/sub-models/task-type.model';
import { TransactionType } from '../../common/models/sub-models/transaction-type.model';
import { NotificationType } from '../../common/models/sub-models/notification-type.model';

@Component({
  selector: 'app-web-system-properties',
  templateUrl: './web-system-properties.component.html',
  styleUrls: ['./web-system-properties.component.css']
})
export class WebSystemPropertiesComponent implements OnInit {

  activityTypes: ActivityType[] | null | undefined;
  contactTypes: ContactType[] | null | undefined;
  datatypes: Datatype[] | null | undefined;
  taskTypes: TaskType[] | null | undefined;
  transactionTypes: TransactionType[] | null | undefined;
  notificationTypes: NotificationType[] | null | undefined;

  constructor(private standard: StandardModelService,
              private condition: ConditionPagingService) { }

  ngOnInit(): void {
    this.getActivityTypes();
    this.getContactTypes();
    this.getDatatypes();
    this.getTaskTypes();
    this.getTransactionTypes();
    this.getNotificationTypes();
  }

  getConditionPaging(){
    return this.condition.getConditionPagingWithListCondition(
      [], new CustomPageRequest(0, 0, Direction.ASC, "id"), true 
    );
  }

  getActivityTypes(){
      this.standard.getWithCondition(this.getConditionPaging(), "activity_type").subscribe(
        data => {
          this.activityTypes = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  getContactTypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "contact_type").subscribe(
        data => {
          this.contactTypes = data;
        },
        err => {
          console.log(err);
        }
    )
  }

  getDatatypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "datatype").subscribe(
        data => {
          this.datatypes = data;
        },
        err => {
          console.log(err);
        }
    )
  }

  getTaskTypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "task_type").subscribe(
        data => {
          this.taskTypes = data;
        },
        err => {
          console.log(err);
        }
    )
  }

  getTransactionTypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "transaction_type").subscribe(
        data => {
          this.transactionTypes = data;
        },
        err => {
          console.log(err);
        }
    )
  }

  getNotificationTypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "notification_type").subscribe(
        data => {
          this.notificationTypes = data;
        },
        err => {
          console.log(err);
        }
    )
  }
}
