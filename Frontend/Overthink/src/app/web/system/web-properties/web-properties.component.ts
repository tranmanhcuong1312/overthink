import { Component, OnInit } from '@angular/core';
import { StandardModelService} from 'src/app/web/common/services/standard-model.service';
import { GlobalProperty } from '../../common/models/sub-models/global-property.model';
import { JobField } from '../../common/models/sub-models/job-field.model';
import { Package } from '../../common/models/sub-models/package.model';
import { ProductLevel } from '../../common/models/sub-models/product-level.model';
import { ConditionPaging } from '../../common/models/template-models/condition-paging.model';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { ConditionPagingService } from '../../common/services/condition-paging.service';

@Component({
  selector: 'app-web-properties',
  templateUrl: './web-properties.component.html',
  styleUrls: ['./web-properties.component.css']
})
export class WebPropertiesComponent implements OnInit {

  globalProperties: GlobalProperty[] | null | undefined;
  jobFields: JobField[] | null | undefined;
  packages: Package[] | null | undefined;
  productLevels: ProductLevel[] | null | undefined;

  constructor(private standard: StandardModelService,
              private condition: ConditionPagingService) { }

  ngOnInit(): void {
    this.getGlobalProperties();
    this.getJobFields();
    this.getPackages();
    this.getProductLevels();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), true 
      );
  }

  getGlobalProperties(){
      this.standard.getWithCondition(this.getConditionPaging(), "global_property").subscribe(
        data => {
          this.globalProperties = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  getJobFields(){     
    this.standard.getWithCondition(this.getConditionPaging(), "job_field").subscribe(
        data => {
          this.jobFields = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  getPackages(){
      this.standard.getWithCondition(this.getConditionPaging(), "package").subscribe(
        data => {
          this.packages = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  getProductLevels(){
      this.standard.getWithCondition(this.getConditionPaging(), "product_level").subscribe(
        data => {
          this.productLevels = data;
        },
        err => {
          console.log(err);
        }
      )
  }
}
