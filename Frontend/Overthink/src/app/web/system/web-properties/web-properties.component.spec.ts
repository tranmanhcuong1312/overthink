import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebPropertiesComponent } from './web-properties.component';

describe('WebPropertiesComponent', () => {
  let component: WebPropertiesComponent;
  let fixture: ComponentFixture<WebPropertiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebPropertiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
