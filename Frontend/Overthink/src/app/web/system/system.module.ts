import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SystemRoutingModule } from './system-routing.module';
import { TaskComponent } from './task/task.component';
import { EventComponent } from './event/event.component';
import { NotificationComponent } from './notification/notification.component';
import { WebPropertiesComponent } from './web-properties/web-properties.component';
import { WebSystemPropertiesComponent } from './web-system-properties/web-system-properties.component';
import { RolePrivilegeComponent } from './role-privilege/role-privilege.component';


@NgModule({
  declarations: [TaskComponent, EventComponent, NotificationComponent, WebPropertiesComponent, WebSystemPropertiesComponent, RolePrivilegeComponent],
  imports: [
    CommonModule,
    SystemRoutingModule,
    ReactiveFormsModule
  ]
})
export class SystemModule { }
