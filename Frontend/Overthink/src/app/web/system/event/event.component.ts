import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { AuthenticationService } from 'src/app/web/common/services/authentication.service';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { Event } from '../../common/models/sub-models/event.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VoidedStandardModel } from '../../common/models/voided-standard-model.model';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  events: Event[] = [];
  eventForm: FormGroup;
  eventChange!: Event;

  
  constructor(private standard: StandardModelService,
              private condition: ConditionPagingService,
              private authentication: AuthenticationService,
              private builder: FormBuilder) {
    this.eventForm = this.builder.group({
      name: ['', Validators.required],
      content: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required] 
    });

    this.eventChange
  }

  ngOnInit(): void {
    this.getEvents();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getEvents(){
    this.standard.getWithCondition(this.getConditionPaging(), "event").subscribe(
      data => {
        this.events = data;
      },
      err => {
        console.log(err);
      }
    )
  }

  createEvent(){
      this.standard.save(this.eventForm.value, "event").subscribe(
        data => {
          window.location.reload();
        },
        err => {
          console.log(err);
        }
      )
  }

  openEditForm(event: Event){
    this.eventForm.setValue({
      name: event.name,
      content: event.content,
      startDate: event.startDate,
      endDate: event.endDate
    })
    this.eventChange = event;
  }

  updateEvent(){
    let map = new Map<String, any>(); 
    if(this.eventForm.value != null){
      Object.keys(this.eventForm.controls).forEach(key => {
        map.set(key, this.eventForm.controls[key].value);
      });
      map.set("id", this.eventChange.id);
      const object = Object.fromEntries(map);
      this.standard.update(object , "event").subscribe(
        data => {
          window.location.reload();
        },
        error =>{
          console.log(error);
        }
      )
    }else{
      throw new Error('Form need value');
    }
  }

  voidedEvent(event: Event){
    let voided= new VoidedStandardModel(event.id, true, '');
    this.standard.voided(voided, 'event').subscribe(
      data => {
        window.location.reload()
      },
      error => {
        console.log(error)        
      }
    )
  }
}
