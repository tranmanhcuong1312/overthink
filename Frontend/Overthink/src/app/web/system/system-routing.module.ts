import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventComponent } from './event/event.component';
import { NotificationComponent } from './notification/notification.component';
import { RolePrivilegeComponent } from './role-privilege/role-privilege.component';
import { SystemComponent } from './system.component';
import { TaskComponent } from './task/task.component';
import { WebPropertiesComponent } from './web-properties/web-properties.component';
import { WebSystemPropertiesComponent } from './web-system-properties/web-system-properties.component';

const routes: Routes = [
  {
    path: '',
    component: SystemComponent, 
    children: [
      {
        path: 'event',
        component: EventComponent
      },
      {
        path: 'notification',
        component: NotificationComponent
      },
      {
        path: 'task',
        component: TaskComponent
      },
      {
        path: 'role-privilege',
        component: RolePrivilegeComponent
      },
      {
        path: 'properties',
        component: WebSystemPropertiesComponent
      },
      {
        path: '',
        component: WebPropertiesComponent
      },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
