import { Component, OnInit } from '@angular/core';
import { StandardModelService } from 'src/app/web/common/services/standard-model.service';
import { ConditionPagingService } from 'src/app/web/common/services/condition-paging.service';
import { CustomPageRequest } from '../../common/models/template-models/custom-page-request.model';
import { Direction } from '../../common/models/template-models/direction.enum';
import { Task } from '../../common/models/sub-models/task.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaskType } from '../../common/models/sub-models/task-type.model';
import { VoidedStandardModel } from '../../common/models/voided-standard-model.model';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  tasks: Task[] = [];
  taskTypes: TaskType[] = [];
  taskForm: FormGroup;
  taskChange!: Task;

  constructor(private standard: StandardModelService,
              private condition: ConditionPagingService,
              private builder: FormBuilder) {
    this.taskForm = this.builder.group({
      taskType: ['', Validators.required],
      name: ['', Validators.required],
      content: ['', Validators.required],
      priority: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getTasks();
    this.getTaskTypes();
  }

  getConditionPaging(){
    return  this.condition.getConditionPagingWithListCondition(
        [], new CustomPageRequest(0, 0, Direction.ASC, "id"), false 
      );
  }

  getTasks(){
    this.standard.getWithCondition(this.getConditionPaging(), "task").subscribe(
      data => {
        this.tasks = data
      },
      error => {
        console.log(error);
      }
    )
  }

  getTaskTypes(){
    this.standard.getWithCondition(this.getConditionPaging(), "task_type").subscribe(
      data => {
        this.taskTypes = data;
      },
      error => {
        console.log(error);
      }
    )
  }

  createTask(){
    let map = new Map<String, any>();
    Object.keys(this.taskForm.controls).forEach(key => {
      map.set(key, this.taskForm.controls[key].value);
    });
    let id: number = map.get("taskType");
    this.taskTypes.forEach(taskType =>{
      if(taskType.id == id){
        map.set("taskType", taskType)
      }
    })
    const object = Object.fromEntries(map);
    this.standard.save( object, "task").subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    )
  }

  openEditForm(task: Task){
    this.taskForm.setValue({
      taskType: task.taskType.name,
      name: task.name,
      content: task.content,
      priority: task.priority
    });
    this.taskChange = task;
  }

  updateTask(){
    let map = new Map<String, any>(); 
    if(this.taskForm.value != null){
      Object.keys(this.taskForm.controls).forEach(key => {
        map.set(key, this.taskForm.controls[key].value);
      });
      map.set("id", this.taskChange.id);
      const object = Object.fromEntries(map);
      this.standard.update(object , "task").subscribe(
        data => {
          window.location.reload();
        },
        error =>{
          console.log(error);
        }
      )
    }else{
      throw new Error('Form need value');
    }
  }

  voidedTask(task: Task){
    let voided= new VoidedStandardModel(task.id, true, '');
    this.standard.voided(voided, 'task').subscribe(
      data => {
        window.location.reload()
      },
      error => {
        console.log(error)        
      }
    )
  }
}
